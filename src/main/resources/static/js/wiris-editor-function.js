function getData() {
	var value ="";
	getLaTeX(editor.getMathML(), function (content) {
		var boundChar = "$";
		value = boundChar.concat(content).concat(boundChar);
    });
	return value;
}

function myFunction() {
	document.getElementById("demo").innerHTML = editor.getMathML();
	getLaTeX(editor.getMathML(), function(content) {
		document.getElementById('source-latex').innerHTML = content;
	});
}

function wrs_urlencode(clearString) {
	var output = '';
	var x = 0;
	clearString = clearString.toString();
	var regex = /(^[a-zA-Z0-9_.]*)/;

	var clearString_length = ((typeof clearString.length) == 'function') ? clearString
			.length()
			: clearString.length;

	while (x < clearString_length) {
		var match = regex.exec(clearString.substr(x));
		if (match != null && match.length > 1 && match[1] != '') {
			output += match[1];
			x += match[1].length;
		} else {
			var charCode = clearString.charCodeAt(x);
			var hexVal = charCode.toString(16);
			output += '%' + (hexVal.length < 2 ? '0' : '')
					+ hexVal.toUpperCase();
			++x;
		}
	}

	return output;
}

function getLaTeX(mathml, callback) {
	var js_path = "http://www.wiris.net/demo/editor";
	var req = new XMLHttpRequest();
	req.open("POST", js_path + "/mathml2latex", false);
	req.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	var params = "mml=" + encodeURIComponent(mathml);
	//req.setRequestHeader("Content-length", params.length);
	//	req.setRequestHeader("Connection", "close");

	req.onreadystatechange = function() {
		if (req.readyState == 4) {
			if (req.status != 200) {
				callback("Error generating LaTeX.");
			} else {
				callback(req.responseText);
			}
		}
	}
	req.send(params);
}