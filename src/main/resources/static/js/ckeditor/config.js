/**
 * @license Copyright (c) 2003-2017, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here.
	// For complete reference see:
	// http://docs.ckeditor.com/#!/api/CKEDITOR.config

	// The toolbar groups arrangement, optimized for two toolbar rows.
	config.extraPlugins = 'wiris';

	/**
	 	sonle : config for removing encode special character for math,
	 			example : [> -> gt;], [];
	  */

    config.entities = false;
    config.basicEntities = false;
    config.entities_greek = false;
    config.entities_latin = false;
    config.entities_additional = '';
    config.htmlEncodeOutput = false;

    /**
     */

	config.toolbarGroups = [
		//{ name: 'editing',     groups: [ 'find', 'selection', 'spellchecker' ] },
		{ name: 'tools' },
		//{ name: 'document',	   groups: [ 'mode', 'document', 'doctools' ] },
		//{ name: 'basicstyles', groups: [ 'basicstyles' ] },
		//{ name: 'paragraph',   groups: [ 'list', 'indent', 'align'] }
		//{ name: 'paragraph',   groups: [ 'list'] }
	];

	// Remove some buttons provided by the standard plugins, which are
	// not needed in the Standard(s) toolbar.

    config.removeButtons = 'Underline,Strike,Subscript,Superscript,Anchor,Styles,Specialchar';

	// Set the most common block elements.
	config.format_tags = 'h1;h2;h3;pre';

	// Simplify the dialog windows.
	config.removeDialogTabs = 'image:advanced;link:advanced';
	
};
