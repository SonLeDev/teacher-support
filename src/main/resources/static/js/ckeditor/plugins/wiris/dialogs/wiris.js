CKEDITOR.dialog.add( 'wirisDialog', function( editor ) {
    return {
        title: 'WIRIS editor',
        minWidth: 800,
        minHeight: 350,
        contents: [
            {
                id: 'tab-basic',
                label: 'Basic Settings',
                elements: [
                    {
                    	type: 'html',
                        html: '<div id="myDiv">' +
                              '<iframe id="'+editor.id+'_'+editor.name +'" ' +
                                        ' style="width:100% !important;height: 330px;" ' +
                                        ' src="http://'+window.location.host+'/admin/wiris/3">' +
                              '</iframe></div>'
                    }
                   
                ]
            }
            
        ],
        onOk: function() {
        	var content = document.getElementById(editor.id+'_'+editor.name).contentWindow.getData();
        	var imgHtml = CKEDITOR.dom.element.createFromHtml("<math>"+ content +"</math>");
			editor.insertElement(imgHtml);
        }
    };
});