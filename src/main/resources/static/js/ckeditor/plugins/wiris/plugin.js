CKEDITOR.plugins.add( 'wiris', {
    icons: 'wiris',
    init: function( editor ) {
    	editor.addCommand( 'wiris', new CKEDITOR.dialogCommand( 'wirisDialog' ) );
    	editor.ui.addButton( 'wiris', {
    	    label: 'Wiris',
    	    command: 'wiris',
    	    toolbar: 'tools'
    	});
    	
    	CKEDITOR.dialog.add( 'wirisDialog', this.path + 'dialogs/wiris.js' );
    }
});
