angular.module('edukaApp.service.module', [])
    .factory('helperService',function ($http,$q) {
        var promiseGet = function (urlGet) {
            return $http.get(urlGet)
                .then(function (response) {
                    return{
                        data:response.data.data
                    }
                });
        }

        /**
         *  SpringMVC using RequestBody to catch form
         *  dataJson : is object js
         *  post without custom header
         *  default Json format
         */
        var promiseSimplePost = function (urlPost,dataJson) {
            return $http.post(urlPost,dataJson)
                .then(function (response) {
                    return{
                        data:response.data.data
                    }
                });
        }
        /**
         *  SpringMVC using RequestParam to catch
         *  dataJson : is object js
         *  post header content-type urlencoded for VietNamese
         */
        var promisePost = function (urlPost,dataJson) {
            var defer = $q.defer();
            var postReq = {
                method: 'POST',
                url: urlPost,
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
                },
                data: $.param(dataJson)
            }

            $http(postReq)
                .then(function successCallback(response){
                        defer.resolve(response.data);
                    }
                    ,function errorCallback(response){
                        defer.reject('Oops... something went wrong');
                    });
            return defer.promise;
        }

        /**
         *  var  formData = new FormData();
                 formData.append('imgFile', $scope.imagePositon5);
                 formData.append('quizObjJson',angular.toJson(quizObjTemp,true));
         * @param urlPost
         * @param formData
         * @returns {promise|{then, fail}}
         */
        var promisePostFormData = function (urlPost,formData) {
            var defer = $q.defer();
            var configHeader = {
                transformRequest: angular.identity,
                headers: {'Content-Type': undefined}
            }
            $http.post(urlPost,formData,configHeader)
                .then(function(response) {
                    defer.resolve(response.data);
            })
            return defer.promise;
        }
        
        return{
            promiseGet:promiseGet
            ,promisePost:promisePost
            ,promiseSimplePost:promiseSimplePost
            ,promisePostFormData:promisePostFormData
        }
    })
    .factory('commonUtilsService',function () {

        var awsBucketEduka = "https://s3-ap-southeast-1.amazonaws.com/edukaio";


        var isEmpty = function (temp) {
            if(temp === undefined || temp == null || temp == ''){
                return true;
            }
            return false;
        }
        var isNotEmpty = function (temp) {
            if(temp != undefined && temp != null && temp != ''){
                return true;
            }
            return false;
        }
        
        var isObjectEmpty = function (obj) {
            if(obj == undefined || obj == null){
                return true;
            }
            return false;
        }

        var isObjectNotEmpty = function (obj) {
            return !isObjectEmpty(obj);
        }
        
        return{
            isEmpty:isEmpty
            ,isNotEmpty:isNotEmpty
            ,isObjectEmpty:isObjectEmpty
            ,isObjectNotEmpty:isObjectNotEmpty
            ,awsBucketEduka:awsBucketEduka
        }
    })
    .factory('adminService',function($state,$cookieStore,$rootScope
        ,$http,broadcastService,$q,helperService){
        var getSubjectName = function(subjectId){
            var subjectName = "";
            switch(subjectId){
                case 1 : subjectName = "Toán Học - Lớp 12"; break;
                case 2 : subjectName = "Vật Lý - Lớp 12"; break;
                case 3 : subjectName = "Hoá Học - Lớp 12"; break;
                case 4 : subjectName = "Tiếng Anh - Lớp 12"; break;
                case 5 : subjectName = "Sinh Học"; break;
                case 6 : subjectName = "Địa Lý"; break;
                case 7 : subjectName = "Lịch Sử"; break;
                default: subjectName = "";
            }
            return subjectName;
        }
        var isLogin = function () {
            if($cookieStore.get('eduka') === undefined){
                // don't have user login infor, comback to login form
                $state.go('login');
                return false;
            }
            return true;
        }
        var getUserAccount = function () {
            return $cookieStore.get('eduka');
        }
        var getUserLogin = function () {
            return $cookieStore.get('eduka');
        }
        var getChapterUnits = function(subjectId){
            return helperService.promiseGet("/quiz/chaptersGroupBySubjectId?subjectId="+subjectId);
        }
        
        var getRouters = function (userId) {
            return helperService.promiseGet("/permission/router/user/"+userId);
        }
        return {
            getSubjectName : getSubjectName,
            isLogin : isLogin,
            getUserAccount : getUserAccount
            ,getUserLogin : getUserLogin
            ,getChapterUnits:getChapterUnits
            ,getRouters:getRouters
        }

    })

.service('shareService', function () {
     var self = this;

     self.sharedVariables = { };
     self.getSharedVariables = getSharedVariables;
     self.setVariable = setVariable;

     //function declarations
     function getSharedVariables() {
         return self.sharedVariables;
     }
     function setVariable(paramName,value) {
         self.sharedVariables[paramName] = value;
     }
 })

.service('apiService', function(){
     var self = this;

     this.saveAndUploadFormData = function(url, formData) {

          var deferred = $q.defer();

          $http.post(url, formData, {
              transformRequest: angular.identity,
              headers: {'Content-Type': undefined}
          }).then(function(response) {
              deferred.resolve(response.data.data);
          })
          .catch(function(response) {
              console.error('Gists error', response.status, response.data);
          })
          .finally(function() {
              console.log("finally finished gists");
          });

          return deferred.promise;
     }
})
;



