/**
 * Created by sonle on 8/8/17.
 */
// var edukaApp = angular.module('edukaApp',[])
angular.module('edukaApp.directives.basic.module',[])
    .directive('helloWorld', function() {
        return {
            restrict: 'AE',
            replace: 'true',
            template: '<h3>Hello World!!</h3>'
        };
    })
    .directive('selectSearchAutoComplete',function () {
          return{
              restrict:'AE',
              replace:'true',
              templateUrl:'/eduka/directives/tmpl/select-search-auto-complele.html'
          }
    })

;