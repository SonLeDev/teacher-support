/***************************************
 * Managing all broadcast from $rootScope
 */
angular.module('edukaApp.broadcast.module', [])
    .factory('broadcastService',function($rootScope){
        var nameEvent = {
            getChapterUnitEvent : 'getChapterUnit'
            ,saveChapterAndUploadEvent : 'saveChapterAndUpload'
            ,saveQuiz : 'saveQuiz'
            ,getQuizList:'getQuizList'
        }

        var emitSaveChapterAndUploadEvent = function (data) {
            $rootScope.$emit(this.nameEvent.saveChapterAndUpload,data);
        }

        return {
            nameEvent:nameEvent
            ,emitSaveChapterAndUploadEvent : emitSaveChapterAndUploadEvent
        }
    })
;
