/**
 * Created by sonle on 9/7/17.
 */
angular.module('eduka.unit.chapter.module', [])
    .factory('unitChapterService',function($http,$q,helperService){

        var getUnitSelectedList = function (subjectId) {
            return helperService.promiseGet("/unit/selected/list/"+subjectId);
        }
        var getUnitNames = function (subjectId) {
            return helperService.promiseGet("/unit/names/"+subjectId);
        }

        var getUnitChapters = function(subjectId){
            return helperService.promiseGet("/quiz/chaptersGroupBySubjectId?subjectId="+subjectId);
        }

        var getUnitList = function (subjectId) {
            return helperService.promiseGet("/unit/chapter/unitList/"+subjectId)
        }

        return {
            getUnitChapters:getUnitChapters,
            getUnitList:getUnitList,
            getUnitNames:getUnitNames,
            getUnitSelectedList:getUnitSelectedList

        }

    })