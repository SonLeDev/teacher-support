angular.module('edukaApp.chapter.module', [])
.controller('chapterListController', function QuizController(
    $scope,$http,$state,$stateParams
    ,$cookies,$cookieStore
    ,adminService,chapterService) {

    $scope.scopeName = "chapterListScope";
    // get contentScope and put va

    // checking exiting or not, but how to reset ????
    $scope.$parent.navigateDynamic = " > Danh sách chuyên đề";

    if(!adminService.isLogin()){
        return;
    }

    $scope.classId = 12;
    $scope.subjectId = $stateParams.subjectId;
    $scope.subjectName = adminService.getSubjectName($scope.subjectId);

    $scope.newObj = {};
    $scope.newObj.classId = $scope.classId;
    $scope.newObj.subjectId = $scope.subjectId;
    $scope.awsBucket = "https://s3-ap-southeast-1.amazonaws.com/edukaio";
    $scope.iconFile = null;
    $scope.iconSrc = null;
    $scope.notification = "";
    $scope.errorClass = "";
    $scope.chapterMap = {};

    $http({
        method : "POST"
        ,params:{'classId': $scope.classId,'subjectId':$scope.subjectId}
        ,url : "/chapter/list"
    }).then(function mySuccess(response) {
        var chapterArr = response.data.data;
        for (var i in  chapterArr) {
            var chp = chapterArr[i];
            $scope.chapterMap[chp.id] = chp;
        }
    }, function myError(response) {
        console.log(response);
    });

    $scope.goDetailUnit = function(chapter){
        $state.go('home.content.unitListState',{chapterId:chapter.id});
    };

    $scope.goEditChapter = function (chapter) {
        $scope.newObj = chapter;
        setTimeout(function() {
            var element = document.getElementById('name');
            if(element)
                element.focus();
        });
    }

    $scope.persist = function () {

        if(!$scope.validForm()){ return;}
        var formData = new FormData();
        formData.append('iconFile', $scope.iconFile);
        formData.append('chapter',angular.toJson($scope.newObj,true));
        chapterService.saveChapterAndUpload(formData);

        $scope.reset();
    };
    $scope.reset = function () {
        $scope.newObj = {};
        $scope.iconFile = null;
        $scope.iconSrc = null;
    }
    $scope.validForm = function () {
        $scope.errorClass = "";
        if($scope.newObj.chpName === undefined || $scope.newObj.chpName == ''){
            $scope.errorClass = "parsley-error";
            return false;
        }
        if($scope.newObj.chpOrder === undefined || $scope.newObj.chpOrder == ''){
            $scope.errorClass = "parsley-error";
            return false;
        }

        return true;
    }
    /////////////////////////////////////////////
    //handling onchange upload file and preview image
    //////////////////////////////

    $scope.onchangeFileUpload = function(event,indx){
        var files = event.target.files; //FileList object
        $scope.fileIndx = indx;
        var file = files[indx];
        var reader = new FileReader();
        reader.onload = $scope.onloadPreviewIcon;
        reader.readAsDataURL(file);
        $scope.iconFile = file;
    }
    $scope.onloadPreviewIcon = function(e){
        $scope.$apply(function() {
            if ($scope.fileIndx =="0"){
                $scope.iconSrc = e.target.result;
            }
        });
    }

    /////////////////////////////////////////////
    //ending handle onchange upload file and preview image
    //////////////////////////////
})

.controller('unitController', function QuizController($scope,$http,$state,$stateParams
        ,$cookies,$cookieStore
        ,chapterService,adminService) {

    if(!adminService.isLogin()){
        return;
    }

    $scope.classId = 12;
    $scope.iconFile = null;
    $scope.iconSrc = null;

    if($stateParams.chapterId!=undefined && $stateParams.chapterId!=null){
        sessionStorage.chapterId = $stateParams.chapterId;
    }
    $scope.chapterId = sessionStorage.chapterId;

    $scope.newObj = {};
    $scope.newObj.subjectId = sessionStorage.subjectId;
    $scope.newObj.chapterId = $scope.chapterId;

    $http({
        method : "GET"
        ,url : "/chapter/"+$scope.chapterId+"/units"
    }).then(function mySuccess(response) {
        $scope.unitList = response.data.data;
    }, function myError(response) {
        console.log(response);
    });

    $scope.goEditUnit = function (unit) {
        $scope.newObj = unit;
        setTimeout(function() {
            var element = document.getElementById('name');
            if(element)
                element.focus();
        });
    }
    $scope.persist = function () {
        if(!$scope.validForm()){ return;}
        var formData = new FormData();
        formData.append('iconFile', $scope.iconFile);
        formData.append('unit',angular.toJson($scope.newObj,true));
        chapterService.saveUnitAndUpload(formData);

        $scope.reset();
    };
    $scope.reset = function () {
        $scope.newObj = {};
        $scope.iconFile = null;
        $scope.iconSrc = null;
    }
    $scope.validForm = function () {
        $scope.errorClass = "";
        if($scope.newObj.unitName === undefined || $scope.newObj.unitName == ''){
            $scope.errorClass = "parsley-error";
            return false;
        }
        if($scope.newObj.unitCode === undefined || $scope.newObj.unitCode == ''){
            $scope.errorClass = "parsley-error";
            return false;
        }
        if($scope.newObj.unitNo === undefined || $scope.newObj.unitOrder == ''){
            $scope.errorClass = "parsley-error";
            return false;
        }

        return true;
    }

    /////////////////////////////////////////////
    //handling onchange upload file and preview image
    //////////////////////////////

    $scope.onchangeFileUpload = function(event,indx){
        var files = event.target.files; //FileList object
        $scope.fileIndx = indx;
        var file = files[indx];
        var reader = new FileReader();
        reader.onload = $scope.onloadPreviewIcon;
        reader.readAsDataURL(file);
        $scope.iconFile = file;
    }
    $scope.onloadPreviewIcon = function(e){
        $scope.$apply(function() {
            if ($scope.fileIndx =="0"){
                $scope.iconSrc = e.target.result;
            }
        });
    }

    /////////////////////////////////////////////
    //ending handle onchange upload file and preview image
    //////////////////////////////
});
