angular.module('chapter.service.module', [])
    .factory('chapterService', function($http,broadcastService) {

        var saveChapterAndUpload = function(formData){

            $http.post("/chapter/saveChapterAndUpload", formData, {
                transformRequest: angular.identity,
                headers: {'Content-Type': undefined}
            }).then(function(response) {
                console.log(response.data);
                broadcastService.emitSaveChapterAndUploadEvent(response.data.data);
            })
            .catch(function(response) {
                console.error('Gists error', response.status, response.data);
            })
            .finally(function() {
                console.log("finally finished gists");
            });
        };
        var saveUnitAndUpload = function(formData){

            $http.post("/chapter/saveUnitAndUpload", formData, {
                transformRequest: angular.identity,
                headers: {'Content-Type': undefined}
            }).then(function(response) {
                console.log(response.data);
            })
                .catch(function(response) {
                    console.error('Gists error', response.status, response.data);
                })
                .finally(function() {
                    console.log("finally finished gists");
                });
        };
        return {
            saveChapterAndUpload : saveChapterAndUpload,
            saveUnitAndUpload : saveUnitAndUpload
        }
    })

;