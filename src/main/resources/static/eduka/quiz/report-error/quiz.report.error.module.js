/**
 * Created by sonle on 11/28/17.
 */

angular.module('edukaApp.quiz.report.error.module', [])
    .controller('quizReportErrorController', function ($scope,$http,$state,$stateParams
                ,quizService,adminService) {

        $scope.subjectId = $stateParams.subjectId;
        $scope.quizList = [];

        var paramJson = {};

        paramJson.subjectId = $scope.subjectId;
        paramJson.userId = adminService.getUserLogin().id;
        paramJson.reportQuizError = true;

        $scope.loadingToggle = true;
        quizService.filterQuizs(paramJson).then(function (resData) {
            $scope.loadingToggle = false;
            filterSuccessUpdateData(resData);
        },function () {
            $scope.loadingToggle = false;
            alert("Không thể tìm kiếm, có lỗi server.");
        });

        function filterSuccessUpdateData(resData) {
            $scope.quizList = [];

            if(resData.data!=null && resData.data.length > 0){
                $scope.quizList = resData.data;
            }else{
                $scope.quizList = [];
            }
            $scope.timeChangeQuizViewListSize = new Date();

        }
    });
