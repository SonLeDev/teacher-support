angular.module('edukaApp.quiz.service.module', [])
    .factory('quizService', function($http,broadcastService,helperService,commonUtilsService) {

        var getQuiz = function (quizId) {
            return helperService.promiseGet("/quiz/"+quizId);
        };

        var filterQuizs = function (paramJson) {
            return helperService.promiseSimplePost("/quiz/list/filter",paramJson);
        }
        var getQuizListByUserIdAndSubjectId = function (userId,subjectId) {
            return helperService.promiseGet("/quiz/list/subject/"+subjectId+"/user/" + userId);
        };

        var getQuizListByTestId= function (testId) {
            return helperService.promiseGet("/quiz/list/test/" + testId);
        }
        var getQuizAll = function () {
            return helperService.promiseGet("/quiz/all");
        };

        var getQuizListByExamId = function (examId) {
            return helperService.promiseGet("/quiz/list/exam/" + examId);
        }
        var getQuizListByUpdateDate = function (date) {
            return helperService.promisePost("/quiz/list/by/updateDate",{"updateDate":date});
        }

        /**
         * Using for post quizObj without upload Image
         * @param quizObj
         */
        var postSaveQuiz = function(quizObj){
            return helperService.promisePost("/quiz/save",quizObj);
        }

        /**
         * Using for post quizObj which need to upload image
         * @param formData
         */
        var postSaveQuizFormData = function(formData){
            return helperService.promisePostFormData("/quiz/saveQuizAndUpload",formData);
        }
        var reportQuiz = function(quizId){
            return helperService.promiseGet("/quiz/report/"+quizId);
        }
        var delQuiz = function (quizId) {
            return helperService.promisePost("/quiz/delete",{"quizId":quizId});
        }
        
        var publishQuiz = function (quizId) {
            return helperService.promisePost("/quiz/publish",{"quizId":quizId});
        }

        var validateBeforeEditQuiz = function (quizObj) {
            var valid=true;
            debugger;
            if((commonUtilsService.isEmpty(quizObj.quizId)&& commonUtilsService.isEmpty(quizObj.id))
                || commonUtilsService.isEmpty(quizObj.subjectId)
                || commonUtilsService.isEmpty(quizObj.userId)
            ){
                valid = false;
            }
            return valid;
        }
        var validCkEditorField =function (quizObj) {
            var valid=true;
            if(commonUtilsService.isEmpty(quizObj.question)
                || commonUtilsService.isEmpty(quizObj.answerA)
                || commonUtilsService.isEmpty(quizObj.answerB)
                || commonUtilsService.isEmpty(quizObj.answerC)
                || commonUtilsService.isEmpty(quizObj.answerD)
            ){
                valid = false;
            }
            return valid;
        }
        var validateQuiz = function (quizObj,type) {
            /**
             id,quizId : required when update
             question : required
             answerA : required
             answerB : required
             answerC : required
             answerD : required
             correctAnswer : required
             chkAnswer : required
             level : required
             unitId : required
             userId : required - system fill
             subjectId : required - system fill

             noOfQuest : required when save for exam,testing
             examId : required for create/update examination - system fill
             testId : required for create/update testing type - system fill

              */

            var valid=true;
            if(commonUtilsService.isEmpty(quizObj.unitId)
                || commonUtilsService.isEmpty(quizObj.question)
                || commonUtilsService.isEmpty(quizObj.answerA)
                || commonUtilsService.isEmpty(quizObj.answerB)
                || commonUtilsService.isEmpty(quizObj.answerC)
                || commonUtilsService.isEmpty(quizObj.answerD)
                // || commonUtilsService.isEmpty(quizObj.chkAnswer)
                || commonUtilsService.isEmpty(quizObj.correctAnswer)
                || commonUtilsService.isEmpty(quizObj.level)
                || commonUtilsService.isEmpty(quizObj.userId)
                || commonUtilsService.isEmpty(quizObj.subjectId)

            ){
                valid = false;
            }

            if(type!=undefined && type=='EXAMINATION'){
                if(commonUtilsService.isEmpty(quizObj.examId)
                    || (commonUtilsService.isEmpty(quizObj.noOfQuest)
                        && commonUtilsService.isEmpty(quizObj.noOfQuiz))){
                    valid = false;
                }
            }else if(type!=undefined && type=='EXAMINATION_EDIT_QUIZ'){
                // don't valid noOfQuest when edit
                if(commonUtilsService.isEmpty(quizObj.examId)){
                    valid = false;
                }
            }
            else if(type!=undefined && type=='TESTING'){
                if(commonUtilsService.isEmpty(quizObj.testId)
                    || (commonUtilsService.isEmpty(quizObj.noOfQuest)
                        && commonUtilsService.isEmpty(quizObj.noOfQuiz))){
                    valid = false;
                }
            }
            else if(type!=undefined && type=='TESTING_EDIT_QUIZ'){
                if(commonUtilsService.isEmpty(quizObj.testId)){
                    valid = false;
                }
            }
            else if(type!=undefined && type=='updating'){
                if(commonUtilsService.isEmpty(quizObj.id)
                    && commonUtilsService.isEmpty(quizObj.quizId)){
                    // updating case require at least quizObj.id or quizObj.quizId not empty
                    valid = false;
                }
            }
            return valid;
        }

        return {
            publishQuiz:publishQuiz
            ,delQuiz:delQuiz
            ,reportQuiz:reportQuiz
            ,getQuizListByUserIdAndSubjectId : getQuizListByUserIdAndSubjectId
            ,getQuiz : getQuiz
            ,getQuizAll:getQuizAll
            ,getQuizListByExamId:getQuizListByExamId
            ,postSaveQuiz:postSaveQuiz
            ,postSaveQuizFormData:postSaveQuizFormData
            ,getQuizListByTestId:getQuizListByTestId
            ,getQuizListByUpdateDate:getQuizListByUpdateDate
            ,filterQuizs:filterQuizs
            ,validateQuiz:validateQuiz
            ,validCkEditorField:validCkEditorField
            ,validateBeforeEditQuiz:validateBeforeEditQuiz
        }
    })