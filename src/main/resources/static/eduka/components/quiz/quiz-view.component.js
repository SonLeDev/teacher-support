angular.module('edukaApp.component.quiz.view.module',[])
    .component("quizViewComponent",{
        templateUrl: "/eduka/components/quiz/tmpl-quiz-view.html",
        controller: quizViewController,
        bindings: {
            quizId:'<'
            ,quiz:'<'
        }
    })
    .component("quizTicketsComponent",{
        templateUrl: "/eduka/components/quiz/tmpl-quiz-tickets.html",
        controller: quizTicketsController,
        bindings: {
            subjectId:'<',
            quizList:'<'
            ,quiz:'<'
            ,onChangeList:'<'
        }
    })
;
/**
 * quizTicketsController which control 2 views
 * - the tickets quiz
 *      - Mathjax show math for a list of latex
 * - interaction with input quiz form for editing
 *      - $ctrl.quizObj will pass to <input-quiz-component>
 *          and then receive back to updating at onDoUpdateQuiz()
*       -
 * @param quizService
 */
function quizTicketsController(quizService,commonUtilsService) {
    var $ctrl = this;
    $ctrl.isEdit = false;
    $ctrl.loadingToggle = false;

    $ctrl.showPopupConfirm = false;
    $ctrl.confirmMessage = "";
    $ctrl.typeConfirm = "";
    $ctrl.quizId = null;

    $ctrl.quizObj = null;
    $ctrl.indexQuizList = {};

    $ctrl.listSize = 0;
    $ctrl.pageSize = 10;
    $ctrl.fromIndex=0;
    $ctrl.toIndex= 0;


    $ctrl.$onInit = function () {
        $ctrl.isEdit = false;
        MathJax.Hub.Config({
            tex2jax: {
                inlineMath: [["$","$"],["\\(","\\)"],["\\[","\\]"]]
            }
        });
        PreviewQuizMath.callback = MathJax.Callback(["createPreview",PreviewQuizMath]);
        PreviewQuizMath.callback.autoReset = true;  // make sure it can run more than once
        PreviewQuizMath.initDivView(document.getElementById("mathPreview")
            ,document.getElementById("mathBuffer"));
    }

    // $onChanges base on param binding changing
    $ctrl.$onChanges = function (changesObj) {
        $ctrl.indexQuizList = {};
        $ctrl.isEdit = false;
        $ctrl.showPopupConfirm = false;
        $ctrl.loadingToggle = true;
        // extend more map<id,quiz> for managing update quiz
        if($ctrl.quizList != null && $ctrl.quizList.length >0){

            $ctrl.listSize = $ctrl.quizList.length;
            $ctrl.fromIndex=0;
            $ctrl.toIndex= $ctrl.fromIndex + $ctrl.pageSize;
            if($ctrl.listSize < $ctrl.pageSize){
                $ctrl.toIndex= $ctrl.listSize;
            }
            
            $ctrl.extractQuizPage();

        }else{
            $ctrl.listSize = 0;
            $ctrl.fromIndex=0;
            $ctrl.toIndex=0;
            $ctrl.indexQuizList = {};
            setTimeout(function(){ PreviewQuizMath.update()}, 1);
        }

    }

    $ctrl.extractQuizPage = function () {
        $ctrl.indexQuizList = {};
        for(var idex = $ctrl.fromIndex;idex< $ctrl.toIndex;idex++){
            var quiz = $ctrl.quizList[idex];
            var quizId = quiz.id;
            if(quizId==undefined){
                quizId = quiz.quizId;
            }
            $ctrl.indexQuizList[quizId]=quiz;
        }
        setTimeout(function(){ PreviewQuizMath.update()}, 1);

    }
    $ctrl.onPrePage = function () {
        $ctrl.loadingToggle = true;
        $ctrl.fromIndex=$ctrl.fromIndex - $ctrl.pageSize;
        if($ctrl.fromIndex < 0){
            $ctrl.fromIndex = 0;
            $ctrl.loadingToggle = false;
            return;
        }
        $ctrl.toIndex= $ctrl.fromIndex + $ctrl.pageSize;
        if($ctrl.listSize < $ctrl.pageSize){
            $ctrl.toIndex= $ctrl.listSize;
        }
        $ctrl.extractQuizPage();
    }
    
    $ctrl.onNextPage = function () {
        $ctrl.loadingToggle = true;
        $ctrl.fromIndex=$ctrl.fromIndex + $ctrl.pageSize;
        if($ctrl.fromIndex >= $ctrl.listSize){
            $ctrl.fromIndex = $ctrl.fromIndex - $ctrl.pageSize;
            $ctrl.loadingToggle = false;
            return;
        }
        $ctrl.toIndex= $ctrl.fromIndex + $ctrl.pageSize;
        if($ctrl.listSize < $ctrl.toIndex){
            $ctrl.toIndex= $ctrl.listSize;
        }
        $ctrl.extractQuizPage();

    }
    
    $ctrl.$postLink = function () {
    }

    $ctrl.turnOffLoading = function () {
        $ctrl.loadingToggle = false;
    }
    /**
     * just show <input-quiz-component> with param : $ctrl.quizObj
     * @param quizId
     */
    $ctrl.onProcessReportQuiz = function (quizId) {

        console.log("report quizId :"+quizId);
        $ctrl.loadingToggle = true;
        $ctrl.quizId = quizId;
        quizService.reportQuiz(quizId).then(function (dataRes) {
            if(dataRes.data!= undefined && dataRes.data!=null ){
                $ctrl.removeQuizFromQuizList();
                $ctrl.extractQuizPage();
                $ctrl.quizId = 0;
                $ctrl.loadingToggle = false;
                $ctrl.showPopupSuccess = true;
                $ctrl.successMessage = "Báo lỗi câu hỏi thành công ! Cảm ơn sự đóng góp của bạn.";

            }else{
                $ctrl.loadingToggle = false;
                $ctrl.showPopupSuccess = false;
                $ctrl.successMessage="";
                console.log("can not report quizId :"+quizId);
            }
        })

    }

    $ctrl.doSuccessFormOk = function () {
        $ctrl.showPopupSuccess = false;
        $ctrl.successMessage="";
    }

    $ctrl.onOpenEditQuizForm = function (quizId) {

        console.log("edit quizId :"+quizId);
        quizService.getQuiz(quizId).then(function (dataRes) {
            if(dataRes.data!= undefined && dataRes.data!=null ){
                $ctrl.isEdit = true;
                $ctrl.quizObj = dataRes.data;
            }else{
                console.log("can not edit quizId :"+quizId);
            }
        })
    }

    $ctrl.onOpenConfirmDeleteQuizForm = function (quizId) {
        
        $ctrl.quizId = quizId;
        $ctrl.showPopupConfirm = true;
        $ctrl.confirmMessage = "Bạn muốn xoá câu hỏi : " + quizId;
        $ctrl.typeConfirm = "deleteQuizType";

    }

    $ctrl.onOpenConfirmPublishQuizForm = function (quizId) {
        
        $ctrl.quizId = quizId;
        $ctrl.showPopupConfirm = true;
        $ctrl.confirmMessage = "Bạn muốn xuất bản mã câu hỏi : " + quizId;
        $ctrl.typeConfirm = "publishQuizType";

    }

    $ctrl.doOKConfirm = function (typeConfirm) {

        $ctrl.showPopupConfirm = false;
        $ctrl.confirmMessage = "";
        $ctrl.typeConfirm = "";
        $ctrl.loadingToggle = true;
        if($ctrl.quizId == undefined || $ctrl.quizId == null){
            $ctrl.loadingToggle = false;
            alert("Khong the xử lý cau hoi " + $ctrl.quizId);
            return;
        }

        if(typeConfirm=="publishQuizType"){
            quizService.publishQuiz($ctrl.quizId).then(function () {

                $ctrl.removeQuizFromQuizList();
                $ctrl.extractQuizPage();
                $ctrl.quizId = 0;

            });
        }else if(typeConfirm=="deleteQuizType"){
            quizService.delQuiz($ctrl.quizId).then(function () {
                $ctrl.removeQuizFromQuizList();
                $ctrl.extractQuizPage();
                $ctrl.quizId = 0;
            })
        }else{
            $ctrl.loadingToggle = false;
        }

    }
    
    $ctrl.removeQuizFromQuizList = function () {
        for(var idex = $ctrl.fromIndex;idex< $ctrl.toIndex;idex++){
            var quizObjTmp = $ctrl.quizList[idex];
            if(quizObjTmp.quizId == $ctrl.quizId){
                $ctrl.quizList.splice(idex, 1);
                $ctrl.listSize = $ctrl.quizList.length;
                if($ctrl.toIndex>$ctrl.listSize){
                    $ctrl.toIndex= $ctrl.listSize;
                }
                break;
            }
        }
    }
    
    $ctrl.doCancel = function () {
        $ctrl.showPopupConfirm = false;
        $ctrl.confirmMessage = "";
    }

    $ctrl.onDoCancelEdit = function () {
        $ctrl.isEdit = false;
        $ctrl.quizObj = {};
    }

    $ctrl.onDoUpdateQuiz = function () {
        var questFile = $ctrl.questFile;
        $ctrl.quizObj.subjectId = $ctrl.subjectId;

        if(quizService.validateQuiz($ctrl.quizObj,'updating')){
            $ctrl.loadingToggle=true;

            $ctrl.quizObj.chkAnswerA = false;
            $ctrl.quizObj.chkAnswerB = false;
            $ctrl.quizObj.chkAnswerC = false;
            $ctrl.quizObj.chkAnswerD = false;
            if(questFile!=undefined || questFile!=null){
                /**
                 * Case upload image, using post FormData
                 */
                var quizObjTemp = $ctrl.quizObj;
                var formData = new FormData();
                formData.append('imgQuestFile', $ctrl.questFile);
                formData.append('quizObjJson',angular.toJson(quizObjTemp,true));
                quizService.postSaveQuizFormData(formData)
                    .then(function (resData) {
                        onSaveQuizSuccessful(resData);
                    },function (error) {
                        onSaveQuizFailure(error);
                    });
            }else{
                quizService.postSaveQuiz($ctrl.quizObj)
                    .then(function (resData) {
                        onSaveQuizSuccessful(resData);
                    },function (error) {
                        onSaveQuizFailure(error);
                    })
            }

        }else{
            // show required field
            alert("Bạn chưa nhập đủ thông tin cần thiết.");
        }
    }
    function onSaveQuizSuccessful(resData) {
        // window.scrollTo(0, 50);
        // console.log("save successful : " + JSON.stringify(data.data));
        $ctrl.loadingToggle= false;

        $ctrl.successMessage = 'Lưu dữ thành công, mã câu hỏi là : ' + $ctrl.quizObj.id;
        $ctrl.showPopupSuccess= true;

        $ctrl.isEdit = false;
        $ctrl.quizObj = resData.data;

        /*
         caused by Response data is quiz-Entity
         */
        if(commonUtilsService.isObjectEmpty($ctrl.quizObj.unitId)
            && commonUtilsService.isObjectNotEmpty($ctrl.quizObj.eduUnit)){
            $ctrl.quizObj.unitId =$ctrl.quizObj.eduUnit.id;
            $ctrl.quizObj.unitName =$ctrl.quizObj.eduUnit.unitName;
        }
        // update back to the quiz which inside the list
        // may be find the correct quizId and then replace it in $ctrl.quizList

        var quizId = $ctrl.quizObj.id != undefined?$ctrl.quizObj.id:$ctrl.quizObj.quizId
        $ctrl.quizObj.quizId = quizId;
        $ctrl.quizObj.id = quizId;
        $ctrl.indexQuizList[quizId]=$ctrl.quizObj;
        // call mathjax update view show math
        setTimeout(function(){ PreviewQuizMath.update()}, 1);

    }
    function onSaveQuizFailure(error) {
        $ctrl.loadingToggle=false;
        console.log("save falure : " + JSON.stringify(error));
        $ctrl.isEdit = false;
        alert('Lưu dữ liệu thất bại !!! Hãy liên hệ với admin để khắc phục lỗi.');
    }

    $ctrl.levelText = function (level) {
        if(level==1){
            return "Nhận biết";
        }else if(level==2){
            return "Thông dụng";
        }else if(level==3){
            return 'Vận dụng';
        }else if(level==4){
            return 'Vận dụng cao';
        }
        return ""
    }

}

function quizViewController(quizService,commonUtilsService) {
    var $ctrl = this;
    $ctrl.questImg = "";
    $ctrl.isEdit = true;
    $ctrl.$onInit = function () {
        $ctrl.isEdit = true;
        $ctrl.bucket =commonUtilsService.awsBucketEduka;
        $ctrl.questImg = "";
        MathJax.Hub.Config({
            tex2jax: {
                inlineMath: [["$","$"],["\\(","\\)"],["\\[","\\]"]]
            }
        });
        PreviewQuizMath.callback = MathJax.Callback(["createPreview",PreviewQuizMath]);
        PreviewQuizMath.callback.autoReset = true;  // make sure it can run more than once
        PreviewQuizMath.initDivView(document.getElementById("mathPreview")
            ,document.getElementById("mathBuffer"));


        if($ctrl.quiz === undefined || $ctrl.quiz == null){
            if($ctrl.quizId != undefined && $ctrl.quizId !=null){
                quizService.getQuiz( $ctrl.quizId)
                    .then(function (data) {
                        $ctrl.quiz = data.data;

                        if(commonUtilsService.isObjectEmpty($ctrl.quiz )){return;}
                        preSetQuiz();
                        setTimeout(function(){ PreviewQuizMath.update()}, 1);
                    });
            }
        }else{
            if(commonUtilsService.isObjectEmpty($ctrl.quiz )){return;}

            preSetQuiz();
            setTimeout(function(){ PreviewQuizMath.update()}, 0.5);
        }

    };

    $ctrl.$onChanges = function (changesObj) {
        if(commonUtilsService.isObjectNotEmpty(changesObj.quiz)){
            if(commonUtilsService.isObjectEmpty($ctrl.quiz )){return;}
            $ctrl.quiz = changesObj.quiz.currentValue;

            preSetQuiz();

            setTimeout(function(){ PreviewQuizMath.update()}, 1);
        }

    }

    var preSetQuiz = function () {
        /**
         * quiz.id and quiz.quizId are using inter-changeable.
         */
        $ctrl.questImg = "";
        $ctrl.showQuestImg = false;
        if(commonUtilsService.isObjectEmpty($ctrl.quiz.quizId)
            && commonUtilsService.isObjectNotEmpty($ctrl.quiz.id)){
            $ctrl.quiz.quizId = $ctrl.quiz.id;
        }
        if(commonUtilsService.isNotEmpty($ctrl.quiz.questImg)){
            $ctrl.showQuestImg = true;
            $ctrl.questImg = $ctrl.bucket +$ctrl.quiz.questImg;
        }
    }

};



////////////////////////////////////////////////////////
// Outside the angularjs
// Object preview Mathjax
///////////////////////////////////////////////////////////


var PreviewQuizMath = {

    delay: 150,        // delay after keystroke before updating
    preview: null,     // filled in by Init below
    buffer: null,      // filled in by Init below
    timeout: null,     // store setTimout id
    mjRunning: false,  // true when MathJax is processing
    mjPending: false,  // true when a typeset has been queued
    oldtext: null,     // used to check if an update is needed


    initDivView: function (divIdPreview, divIdBuffer) {
        this.preview 	= divIdPreview;
        this.buffer 	= divIdBuffer;
    },
    swapBuffers: function () {
        // using swap prevew and buffer form for each other
        var buffer = this.preview, preview = this.buffer;
        this.buffer = buffer; this.preview = preview;
        buffer.style.visibility = "hidden"; buffer.style.position = "absolute";
        preview.style.position = ""; preview.style.visibility = "";


        $('.btn-edit-quiz').click(function(){
            var quizId = $(this).attr("quiz-id");
            angular.element($("#quiz-multi-view")).scope().$ctrl.onOpenEditQuizForm(quizId);
            angular.element($("#quiz-multi-view")).scope().$apply();

        });

        $('.btn-report-quiz').click(function(){
            var quizId = $(this).attr("quiz-id");
            angular.element($("#quiz-multi-view")).scope().$ctrl.onProcessReportQuiz(quizId);
            angular.element($("#quiz-multi-view")).scope().$apply();
        });

        $('.btn-delete-quiz').click(function(){
            var quizId = $(this).attr("quiz-id");
            angular.element($("#quiz-multi-view")).scope().$ctrl.onOpenConfirmDeleteQuizForm(quizId);
            angular.element($("#quiz-multi-view")).scope().$apply();

        });

        $('.btn-publish-quiz').click(function(){
            var quizId = $(this).attr("quiz-id");
            angular.element($("#quiz-multi-view")).scope().$ctrl.onOpenConfirmPublishQuizForm(quizId);
            angular.element($("#quiz-multi-view")).scope().$apply();

        });

        // after show math done, turn off loading spinner
        if(angular.element($("#quiz-multi-view")).scope()!=undefined){
            angular.element($("#quiz-multi-view")).scope().$ctrl.turnOffLoading();
            angular.element($("#quiz-multi-view")).scope().$apply();
        }

    },
    update: function () {
        console.log("PreviewQuizMath.update ... ");
        if (this.timeout) {clearTimeout(this.timeout)}
        this.timeout = setTimeout(this.callback,this.delay);
    },
    createPreview: function () {
        console.log("Mathjax - create view");
        PreviewQuizMath.timeout = null;
        if (this.mjPending) return;
        // extract data latex which want to show the math
        var text = document.getElementById("contentQuizDiv").innerHTML;

        if (text === this.oldtext){
            if(angular.element($("#quiz-multi-view")).scope()!=undefined){
                angular.element($("#quiz-multi-view")).scope().$ctrl.turnOffLoading();
                angular.element($("#quiz-multi-view")).scope().$apply();
            }
            return;
        }
        if (this.mjRunning) {
            this.mjPending = true;
            MathJax.Hub.Queue(["createPreview",this]);
        } else {
            this.buffer.innerHTML = this.oldtext = text;
            this.mjRunning = true;
            MathJax.Hub.Queue(
                ["Typeset",MathJax.Hub,this.buffer],
                ["callbackPreviewDone",this]
            );
        }
    },

    callbackPreviewDone: function () {
        this.mjRunning = this.mjPending = false;
        this.swapBuffers();
    }
};

/////////////////////////////////////////////////////////////////////////////////////
////// Mathjax preview
/////////////////////////////////////////////////////////////////////////////////////