/**
 * Created by sonle on 9/24/17.
 */
angular.module('edukaApp.component.create.quiz.module',[])
    .component("createQuizFlatComponent",{
        templateUrl: "/eduka/components/create-quiz/tmpl-create-quiz-flat.html",
        controller: createQuizFlatController,
        bindings: {
            subjectId:'<',
            userId:'<'
        }
    });


function createQuizFlatController() {
    var $ctrl = this;

    $ctrl.$onInit = function (){
        console.log($ctrl.subjectId);
    }

    $ctrl.$onChanges = function(changesObj){
    }
}


