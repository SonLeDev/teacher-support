angular.module('edukaApp.component.action.bar.module',[])
    .component("actionBar",{
        templateUrl: "/eduka/components/action-bar/tmpl-action-bar.html",
        controller: actionBarController,
        bindings: {
            onSave:'&'
            ,onDelete: '&' // binding out to the function's parent form
            ,onUpdate: '&'
            ,onCancel:'&'
        }
    });

function actionBarController(){
    var ctrl = this;

    ctrl.delete = function() {
        ctrl.onDelete();
    };

    ctrl.update = function() {
        ctrl.onUpdate();
    };
}