/**
 * Created by sonle on 9/24/17.
 */
angular.module('edukaApp.component.general.form.module',[])
    .component("loadingSpinnerComponent",{
        templateUrl: "/eduka/components/general-form/tmpl-loading-form.html",
        controller: loadingSpinnerController,
        bindings: { // flow binding to template first, then template down to controller
            toggle:'<'      // catch onChange 'showPopup' to show this form
        }
    });


function loadingSpinnerController() {
    var $ctrl = this;

    $ctrl.$onInit = function (){ $ctrl.toggle = false;}

    $ctrl.$onChanges = function(changesObj){
        if(changesObj.toggle.currentValue==true){
        }else{
        }
    }
}