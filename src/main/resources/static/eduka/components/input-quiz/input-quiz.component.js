/**
 * This component base on the first version of inputting quiz
 */
angular.module('edukaApp.component.input.quiz.module',[])
    .component("inputQuizComponent",{
        templateUrl: "/eduka/components/input-quiz/tmpl-input-quiz.html",
        controller: inputQuizController,
        bindings: {
            subjectId:'<',
            userId:'<', // input required
            quizId:'<', // input option
            quizObj:'<',
            resetQuiz:'<',       // signature for reset empty CKEditor, when edit quiz, resetQuiz = false
            timeOnChangeQuiz:'<' // signature for change quizObj
        }
    })
;
function inputQuizController($state,adminService,quizService) {
    var $ctrl = this;
    console.log("input-quiz.component");
    $ctrl.awsBucket = "https://s3-ap-southeast-1.amazonaws.com/edukaio";
    $ctrl.isNextQuiz = false;

    initCKEditor();
    initMathJax();

    $ctrl.$onInit = function () {
        console.log("$onInit");
        $ctrl.subjectName = adminService.getSubjectName($ctrl.subjectId);
    }

    $ctrl.$onChanges = function(changesObj) {
        console.log("$onChanges input-quiz.component");
        if(!angular.equals({},$ctrl.quizObj) && !angular.equals(null,$ctrl.quizObj)){
            debugger;
            $ctrl.questImgUrl = null;
            $ctrl.imgQuest = null;
            if($ctrl.quizObj.questImg!=undefined
                && $ctrl.quizObj.questImg!=null
                && $ctrl.quizObj.questImg!=''){
                $ctrl.questImgUrl = $ctrl.awsBucket + $ctrl.quizObj.questImg;
            }
            if(quizService.validCkEditorField($ctrl.quizObj)){
                bindingQuizObjToCkEditor($ctrl.quizObj);
            }

        }
        else if(changesObj.quizId!=undefined && changesObj.quizId!=null
                && changesObj.quizId.currentValue!=undefined){
            quizService.getQuiz($ctrl.quizId).then(function (data) {
                $ctrl.quizObj = data.data;
                $ctrl.quizObj['chkAnswer'] = $ctrl.quizObj['correctAnswer'];
                console.log("$onChanges::bindingQuizObjToCkEditor");
                setTimeout(function(){
                    bindingQuizObjToCkEditor($ctrl.quizObj)}, 100);
            });
        }
        if($ctrl.resetQuiz!=undefined && $ctrl.resetQuiz!=null
            &&$ctrl.resetQuiz){
            $ctrl.questImgUrl = null;
            $ctrl.imgQuest = null;
            resetEmptyCkEditor();
        }

    }
    $ctrl.$postLink = function () {
        console.log("$postLink");
    }

    $ctrl.removeImgQuest = function () {
        $ctrl.imgQuest = null;
        $ctrl.quizObj.imgQuestFile = null;
        $ctrl.questImgUrl = null;
        $ctrl.quizObj.questImg = null; // signature for remove image

    }

    function resetEmptyCkEditor() {
        for ( instance in CKEDITOR.instances ){
            CKEDITOR.instances[instance].updateElement();
            CKEDITOR.instances[instance].setData('');
        }
    }
    function bindingQuizObjToCkEditor(quizObj){
        if (typeof quizObj != 'undefined') {
            if (typeof quizObj.question != 'undefined' && quizObj.question != '' && quizObj.question != null) {
                CKEDITOR.instances.questionTxtArea.setData(quizObj.question);
            }

            if (typeof quizObj.answerA != 'undefined' && quizObj.answerA != '' && quizObj.answerA != null) {
                CKEDITOR.instances.answerATxtArea.setData(quizObj.answerA);
            }

            if (typeof quizObj.answerB != 'undefined' && quizObj.answerB != '' && quizObj.answerB != null) {
                CKEDITOR.instances.answerBTxtArea.setData(quizObj.answerB);
            }

            if (typeof quizObj.answerC != 'undefined' && quizObj.answerC != '' && quizObj.answerC != null) {
                CKEDITOR.instances.answerCTxtArea.setData(quizObj.answerC);
            }

            if (typeof quizObj.answerD != 'undefined' && quizObj.answerD != '' && quizObj.answerD != null) {
                CKEDITOR.instances.answerDTxtArea.setData(quizObj.answerD);
            }

            if (typeof quizObj.detailAnswer != 'undefined' && quizObj.detailAnswer != '' && quizObj.detailAnswer != null) {
                CKEDITOR.instances.detailAnswerTxtArea.setData(quizObj.detailAnswer);
            }
            PreviewInputMathjax.update("");
        }
    }

    function initCKEditor() {

        console.log("initCKEditor");

        if(CKEDITOR.instances.questionTxtArea){ CKEDITOR.instances.questionTxtArea.destroy();}
        if(CKEDITOR.instances.answerATxtArea){ CKEDITOR.instances.answerATxtArea.destroy();}
        if(CKEDITOR.instances.answerBTxtArea){ CKEDITOR.instances.answerBTxtArea.destroy();}
        if(CKEDITOR.instances.answerCTxtArea){ CKEDITOR.instances.answerCTxtArea.destroy();}
        if(CKEDITOR.instances.answerDTxtArea){ CKEDITOR.instances.answerDTxtArea.destroy();}
        if(CKEDITOR.instances.detailAnswerTxtArea){ CKEDITOR.instances.detailAnswerTxtArea.destroy();}


        var editorData;
        CKEDITOR.replace( 'questionTxtArea').on( 'change', function( evt ) {
            PreviewInputMathjax.update('question');
            editorData = CKEDITOR.instances.questionTxtArea.getData()
                .replace(/<math>/g , "").replace(/<\/math>/g , "")
                .replace(/<p>/g , "").replace(/<\/p>/g , "")
            ;
            if($ctrl.quizObj != undefined && $ctrl.quizObj!=null){
                $ctrl.quizObj.question = editorData;
            }

        });
        CKEDITOR.replace( 'answerATxtArea').on( 'change', function( evt ) {
            PreviewInputMathjax.update('answerA');
            editorData = CKEDITOR.instances.answerATxtArea.getData()
                .replace(/<math>/g , "").replace(/<\/math>/g , "")
                .replace(/<p>/g , "").replace(/<\/p>/g , "");
            if($ctrl.quizObj != undefined && $ctrl.quizObj!=null){

                $ctrl.quizObj.answerA = editorData;
            }
        });
        CKEDITOR.replace( 'answerBTxtArea').on( 'change', function( evt ) {
            PreviewInputMathjax.update('answerB');
            editorData = CKEDITOR.instances.answerBTxtArea.getData()
                .replace(/<math>/g , "").replace(/<\/math>/g , "")
                .replace(/<p>/g , "").replace(/<\/p>/g , "")
            ;
            if($ctrl.quizObj != undefined && $ctrl.quizObj!=null){

                $ctrl.quizObj.answerB = editorData;
            }
        });
        CKEDITOR.replace( 'answerCTxtArea').on( 'change', function( evt ) {
            PreviewInputMathjax.update('answerC');
            editorData = CKEDITOR.instances.answerCTxtArea.getData()
                .replace(/<math>/g , "").replace(/<\/math>/g , "")
                .replace(/<p>/g , "").replace(/<\/p>/g , "")
            ;
            if($ctrl.quizObj != undefined && $ctrl.quizObj!=null){

                $ctrl.quizObj.answerC = editorData;
            }
        });
        CKEDITOR.replace( 'answerDTxtArea').on( 'change', function( evt ) {
            PreviewInputMathjax.update('answerD');
            editorData = CKEDITOR.instances.answerDTxtArea.getData()
                .replace(/<math>/g , "").replace(/<\/math>/g , "")
                .replace(/<p>/g , "").replace(/<\/p>/g , "")
            ;
            if($ctrl.quizObj != undefined && $ctrl.quizObj!=null){

                $ctrl.quizObj.answerD = editorData;
            }
        });
        CKEDITOR.replace( 'detailAnswerTxtArea').on( 'change', function( evt ) {
            PreviewInputMathjax.update('answerDetail');
            editorData = CKEDITOR.instances.detailAnswerTxtArea.getData()
                .replace(/<math>/g , "").replace(/<\/math>/g , "")
                .replace(/<p>/g , "").replace(/<\/p>/g , "")
            ;
            if($ctrl.quizObj != undefined && $ctrl.quizObj!=null){

                $ctrl.quizObj.detailAnswer = editorData;
            }
        });



    }

};


function initMathJax() {
    MathJax.Hub.Config({
        tex2jax: {
            inlineMath: [["$","$"],["\\(","\\)"],["\\[","\\]"]]
        }
    });

    PreviewInputMathjax.callback = MathJax.Callback(["createPreview",PreviewInputMathjax]);
    PreviewInputMathjax.callback.autoReset = true;  // make sure it can run more than once

    PreviewInputMathjax.initQuest(document.getElementById("questionMathPreview")
        ,document.getElementById("questionMathBuffer"));
    PreviewInputMathjax.initAnswerA(document.getElementById("answerAPreview")
        ,document.getElementById("answerABuffer"));
    PreviewInputMathjax.initAnswerB(document.getElementById("answerBPreview")
        ,document.getElementById("answerBBuffer"));
    PreviewInputMathjax.initAnswerC(document.getElementById("answerCPreview")
        ,document.getElementById("answerCBuffer"));
    PreviewInputMathjax.initAnswerD(document.getElementById("answerDPreview")
        ,document.getElementById("answerDBuffer"));
    PreviewInputMathjax.initDetailAnswer(document.getElementById("detailAnswerPreview")
        ,document.getElementById("detailAnswerBuffer"));
}


////////////////////////////////////////////////////////
// Outside the angularjs
// Object preview Mathjax
///////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////
////// Mathjax preview
/////////////////////////////////////////////////////////////////////////////////////

var PreviewInputMathjax = {

    delay: 150,        // delay after keystroke before updating
    preview: null,     // filled in by Init below
    buffer: null,      // filled in by Init below
    timeout: null,     // store setTimout id
    mjRunning: false,  // true when MathJax is processing
    mjPending: false,  // true when a typeset has been queued
    text: null,         // used to check if an update is needed
    oldText: null,     // used to check if an update is needed
    fieldChanged:'',

    previewContent:{},
    questContent:{text :"", oldText:"", preview:null,buffer:null},
    ansAContent: {text :"", oldText:"", preview:null,buffer:null},
    ansBContent: {text :"", oldText:"", preview:null,buffer:null},
    ansCContent: {text :"", oldText:"", preview:null,buffer:null},
    ansDContent: {text :"", oldText:"", preview:null,buffer:null},
    detailAnswerContent: {text :"", oldText:"", preview:null,buffer:null},

    initQuest: function (divPreview,divBuffer) {
        this.questContent.preview 	= divPreview;
        this.questContent.buffer 	= divBuffer;
    },
    initAnswerA: function (divPreview,divBuffer) {
        this.ansAContent.preview 	= divPreview;
        this.ansAContent.buffer 	= divBuffer;
    },
    initAnswerB: function (divPreview,divBuffer) {
        this.ansBContent.preview 	= divPreview;
        this.ansBContent.buffer 	= divBuffer;
    },
    initAnswerC: function (divPreview,divBuffer) {
        this.ansCContent.preview 	= divPreview;
        this.ansCContent.buffer 	= divBuffer;
    },
    initAnswerD: function (divPreview,divBuffer) {
        this.ansDContent.preview 	= divPreview;
        this.ansDContent.buffer 	= divBuffer;
    },
    initDetailAnswer: function (divPreview,divBuffer) {
        this.detailAnswerContent.preview 	= divPreview;
        this.detailAnswerContent.buffer 	= divBuffer;
    },

    update: function (fieldChanged) {
        this.fieldChanged = fieldChanged;
        console.log("PreviewInputMathjax.Update ... ");
        if (this.timeout) {clearTimeout(this.timeout)}
        this.timeout = setTimeout(this.callback,this.delay);
    },
    createPreview: function () {
        console.log("PreviewInputMathjax.createPreview ... ");

        PreviewInputMathjax.timeout = null;
        if (this.mjPending) return;

        var hasChanged = false;
        if(CKEDITOR.instances.questionTxtArea!=undefined){
            hasChanged = this.extractPreviewData(CKEDITOR.instances.questionTxtArea,this.questContent);
        }
        if(!hasChanged && CKEDITOR.instances.answerATxtArea!=undefined){
            hasChanged = this.extractPreviewData(CKEDITOR.instances.answerATxtArea,this.ansAContent);
        }
        if(!hasChanged && CKEDITOR.instances.answerATxtArea!=undefined){
            hasChanged = this.extractPreviewData(CKEDITOR.instances.answerBTxtArea,this.ansBContent);
        }
        if(!hasChanged && CKEDITOR.instances.answerATxtArea!=undefined){
            hasChanged = this.extractPreviewData(CKEDITOR.instances.answerCTxtArea,this.ansCContent);
        }
        if(!hasChanged && CKEDITOR.instances.answerATxtArea!=undefined){
            hasChanged = this.extractPreviewData(CKEDITOR.instances.answerDTxtArea,this.ansDContent);
        }

        if(!hasChanged && CKEDITOR.instances.detailAnswerTxtArea!=undefined){
            hasChanged = this.extractPreviewData(CKEDITOR.instances.detailAnswerTxtArea,this.detailAnswerContent);
        }

        if(!hasChanged) return; // don't request MathJax when don't have any changes.

        if (this.mjRunning) {
            this.mjPending = true;
            MathJax.Hub.Queue(["createPreview",this]);
        } else {
            this.buffer.innerHTML = this.previewContent.oldText = this.text;
            this.mjRunning = true;
            MathJax.Hub.Queue(["Typeset",MathJax.Hub,this.buffer],
                ["previewDone",this]);
        }
    },
    previewDone: function () {
        this.mjRunning = this.mjPending = false;
        this.swapBuffers();
    },
    swapBuffers: function () {
        // using swap prevew and buffer form for each other
        var buffer = this.preview, preview = this.buffer;
        this.buffer = buffer; this.preview = preview;
        buffer.style.visibility = "hidden"; buffer.style.position = "absolute";
        preview.style.position = ""; preview.style.visibility = "";
        // update until hasChanged = false;
        this.update("");

    },

    extractPreviewData:function(ckInstance,contentObj){
        console.log("PreviewInputMathjax.extractPreviewData ... ");

        var text = ckInstance.getData();
        contentObj['text'] = text.replace(/<math>/g , "").replace(/<\/math>/g , "");
        if (contentObj['text'] != contentObj['oldText']){
            console.log("diff from " + contentObj);
            this.text = contentObj['text'];
            this.oldText = contentObj['oldText'];
            this.preview 	= contentObj['preview'];
            this.buffer 	= contentObj['buffer'];
            this.previewContent = contentObj;
            return true;
        };
        return false;
    }

};
