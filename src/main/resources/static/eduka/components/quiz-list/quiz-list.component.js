angular.module('edukaApp.component.quiz.list.module',[])
    .component("quizListComponent",{
        templateUrl: "/eduka/components/quiz-list/tmpl-quiz-list.html",
        controller: quizListController,
        bindings: {
             userId:'<'
            ,subjectId:'<'
            ,quizList:'<'
            ,onChangeQuizList:'<' // trigger onChange quizList content
            ,isResetPaging:'<'
            ,onEditRow:'&'
            ,onDeleteRow:'&'
        }
    })
;
function quizListController(quizService) {
    var $ctrl = this;

    $ctrl.$onInit = function () {
        $ctrl.isResetPaging = true;
        if($ctrl.userId != undefined && $ctrl.userId!=0){
            $ctrl.userId = $ctrl.userId || 'NA';
            quizService.getQuizListByUserIdAndSubjectId($ctrl.userId,$ctrl.subjectId)
                .then(function (data) {
                    updateTable(data.data);
                });
        }else if($ctrl.quizList != undefined){
            updateTable($ctrl.quizList);
        }
    };

    $ctrl.$onChanges = function(changes) {
        if($ctrl.quizList!=undefined){
            // console.log($ctrl.quizList.length);
            updateTable();
        }
    }

    $ctrl.editRow = function (quizId) {
        $ctrl.onEditRow({quizId:quizId});
    }
    $ctrl.deleteRow = function (quizId) {
        $ctrl.onDeleteRow({quizId:quizId});
    }

    function updateTable() {

        $ctrl.rowPerPage = 10; // hard-code
        ///////////////////////
        // handling paging
        ////////////////////////////
        if($ctrl.isResetPaging!= undefined && $ctrl.isResetPaging == true){
            $ctrl.currPage = 1;
            $ctrl.numOfPage = 0;
            $ctrl.numPagingBar = [];
        }
        if($ctrl.quizList.length==$ctrl.rowPerPage
            && $ctrl.currPage!=undefined && $ctrl.currPage!=null && $ctrl.currPage>1){
            $ctrl.currPage--;
        }

        pagingBar();

        $ctrl.onPaging($ctrl.currPage);
        ////////////////////////////////////////
        // Ending paging
        //////////////////////
    }

    function pagingBar(){

        $ctrl.numOfPage = Math.ceil($ctrl.quizList.length/$ctrl.rowPerPage);
        $ctrl.numPagingBar=[];
        for(var i = 1 ; i <= $ctrl.numOfPage ; i ++){
            $ctrl.numPagingBar.push(i);
        }
    }
    $ctrl.onPaging =function (page) {
        if(page <=0 || page > $ctrl.numOfPage) return;
        $ctrl.quizPaging = []; // reset page content
        $ctrl.currPage = page;
        var toIdex = page*$ctrl.rowPerPage;
        var fromIdex = toIdex - $ctrl.rowPerPage;
        for(;fromIdex<toIdex && fromIdex < $ctrl.quizList.length ;fromIdex++){
            var quiz = $ctrl.quizList[fromIdex];
            quiz.idex = fromIdex+1;
            $ctrl.quizPaging.push(quiz);
        }
    }
    $ctrl.onPagingPrevious = function () {
        if($ctrl.currPage <=1) return;
        $ctrl.currPage --;
        $ctrl.onPaging($ctrl.currPage);
    }
    $ctrl.onPagingNext = function () {
        if($ctrl.currPage >= $ctrl.numOfPage) return;
        $ctrl.currPage ++;
        $ctrl.onPaging($ctrl.currPage);
    }

}

