/**
 * Created by sonle on 9/24/17.
 */
angular.module('edukaApp.component.input.math.module',[])
    .component("inputMathVComponent",{
        templateUrl: "/eduka/components/input-math/tmpl-input-math-vertical.html",
        controller: inputMathController,
        bindings: {
            id:"@",
            latexData:'='
        }
    })
    .component("inputMathHComponent",{
        templateUrl: "/eduka/components/input-math/tmpl-input-math-horizontal.html",
        controller: inputMathController,
        bindings: {
            id:"@",
            latexData:'='
        }
    });


function inputMathController() {
    var $ctrl = this;
    this.$postLink = function () {

    }

    $ctrl.$onInit = function (){
        $ctrl.id = "LatexTxtArea";
        InputMathEditor.init($ctrl.id);

        MathJax.Hub.Config({
            tex2jax: {
                inlineMath: [["$","$"],["\\(","\\)"],["\\[","\\]"]]
            }
        });
        $ctrl.id;
        PreviewInputMath.callback = MathJax.Callback(["CreatePreview",PreviewInputMath]);
        PreviewInputMath.callback.autoReset = true;  // make sure it can run more than once

        PreviewInputMath.InitQuest(document.getElementById("InputMathPreview")
            ,document.getElementById("InputMathBuffer"));
        PreviewInputMath.Update();

    }

    $ctrl.$onChanges = function(changesObj){
    }


}

var InputMathEditor = {
    init : function(id){
        //CKEDITOR.config.toolbar_MA= ['list'];
        debugger;
        if(CKEDITOR.instances.LatexTxtArea){ CKEDITOR.instances.LatexTxtArea.destroy();}

        var idTextArea = 'LatexTxtArea';
        CKEDITOR.replace(idTextArea,
            {removeButtons: 'Bold,Italic,Underline,Strike,Subscript,Superscript,Anchor,Styles,Specialchar'}
        ).on( 'change', function( evt ) {
            PreviewInputMath.Update();
        });
    },
    ShowDataForEdit:function(latexData){
        if (typeof latexData != 'undefined') {
                CKEDITOR.instances.LatexTxtArea.setData(latexData);
                document.getElementById("QuestionMathPreview").innerHTML = latexData;
        }
    },
}

var PreviewInputMath = {

    delay: 150,        // delay after keystroke before updating
    preview: null,     // filled in by Init below
    buffer: null,      // filled in by Init below
    timeout: null,     // store setTimout id
    mjRunning: false,  // true when MathJax is processing
    mjPending: false,  // true when a typeset has been queued
    text: null,         // used to check if an update is needed
    oldText: null,     // used to check if an update is needed

    previewContent:{},
    questContent:{text :"", oldText:"", preview:null,buffer:null},

    InitQuest: function (divPreview,divBuffer) {
        this.questContent.preview 	= divPreview;
        this.questContent.buffer 	= divBuffer;
    },

    SwapBuffers: function () {
        // console.log("PreviewMath.SwapBuffers ... ");

        // using swap prevew and buffer form for each other
        var buffer = this.preview, preview = this.buffer;
        this.buffer = buffer; this.preview = preview;
        buffer.style.visibility = "hidden"; buffer.style.position = "absolute";
        preview.style.position = ""; preview.style.visibility = "";
    },
    Update: function () {
        // console.log("PreviewMath.Update ... ");
        if (this.timeout) {clearTimeout(this.timeout)}
        this.timeout = setTimeout(this.callback,this.delay);
    },
    CreatePreview: function () {
        // console.log("PreviewMath.CreatePreview ... ");

        PreviewMath.timeout = null;
        if (this.mjPending) return;

        var hasChanged = false;
        // extract data from ckEdit, then send to API Mathjax, finally show the math response
        if(CKEDITOR.instances.LatexTxtArea!=undefined){
            hasChanged = this.ExtractPreviewData(CKEDITOR.instances.LatexTxtArea,this.questContent);
        }

        if(!hasChanged) return; // don't request MathJax when don't have any changes.

        if (this.mjRunning) {
            this.mjPending = true;
            MathJax.Hub.Queue(["CreatePreview",this]);
        } else {
            this.buffer.innerHTML = this.previewContent.oldText = this.text;
            this.mjRunning = true;
            MathJax.Hub.Queue(["Typeset",MathJax.Hub,this.buffer],
                ["PreviewDone",this]);
        }
    },

    ExtractPreviewData:function(ckInstance,contentObj){
        var text = '';

        // text = CKEDITOR.instances.LatexTxtArea.getData();
        text = ckInstance.getData();
        text = text.replace(/<math>/g , "")
                    .replace(/<\/math>/g , "");
        contentObj['text'] = text;
        console.log("=== latex pattern : " + text);
        if (contentObj['text'] != contentObj['oldText']){
            console.log("diff from " + contentObj);
            this.text = contentObj['text'];
            this.oldText = contentObj['oldText'];
            this.preview 	= contentObj['preview'];
            this.buffer 	= contentObj['buffer'];
            this.previewContent = contentObj;
            return true;
        };
        return false;
    },

    PreviewDone: function () {
        console.log("PreviewMath.PreviewDone ... ");

        this.mjRunning = this.mjPending = false;
        this.SwapBuffers();
    }
};


