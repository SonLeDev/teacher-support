/**
 * Created by sonle on 9/22/17.
 */
angular.module('edukaApp.component.confirm.form.module',[])
    .component("confirmFormComponent",{
        templateUrl: "/eduka/components/confirm-form/tmpl-confirm-form.html",
        controller: confirmFormController,
        bindings: { // flow binding to template first, then template down to controller
            message:'<'         // content message
            ,doOk:'&'           // callback to parent form to continue the flow
            ,doCancel:'&'       // callback to parent form to update 'showPopup=false'
            ,showPopup:'<'      // catch onChange 'showPopup' to show this form

        }
    })


.component("successFormComponent",{
    templateUrl: "/eduka/components/confirm-form/tmpl-success-form.html",
    controller: successFormController,
    bindings: { // flow binding to template first, then template down to controller
        message:'<'         // content message
        ,doOk:'&'           // callback to parent form to continue the flow
        ,showPopup:'<'      // catch onChange 'showPopup' to show this form

    }
})
;
function confirmFormController() {
    var $ctrl = this;
    
    $ctrl.$onInit = function () {
        $ctrl.showPopup = false;
    }

    $ctrl.$onChanges = function(changesObj){
        if(changesObj.showPopup.currentValue==true){
            $ctrl.popupClass = 'is-visible';
        }else{
            $ctrl.popupClass = '';
        }
    }

    $ctrl.onClickOK = function () {
        $ctrl.doOk({msg:''});
    }
    $ctrl.onClickCancel = function () {
        $ctrl.hideForm = true;
        $ctrl.popupClass = '';
        $ctrl.doCancel();
    }

}
function successFormController() {
    var $ctrl = this;

    $ctrl.$onInit = function () {
        // $ctrl.popupClass = 'is-visible';

        $ctrl.showPopup = false;
    }

    $ctrl.$onChanges = function(changesObj){
        if(changesObj.showPopup.currentValue==true){
            $ctrl.popupClass = 'is-visible';
        }else{
            $ctrl.popupClass = '';
        }
    }

    $ctrl.onClickOK = function () {
        $ctrl.hideForm = true;
        $ctrl.popupClass = '';
        $ctrl.doOk({msg:''});
    }
}