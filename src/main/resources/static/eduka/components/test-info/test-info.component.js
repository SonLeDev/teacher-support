angular.module('edukaApp.component.test.info.module',[])
    .component("testInfoComponent",{
        templateUrl: "/eduka/components/test-info/tmpl-test-info.html",
        controller: testInfoController,
        bindings: {
            testId:'<',
            testObj:'<'
        }
    });

function testInfoController(testService){
    var $ctrl = this;
    $ctrl.showLoading = false;
    $ctrl.errorMsg = "";
    $ctrl.testObj=null;
    $ctrl.$onInit = function () {
        $ctrl.showLoading = false;
    }
    $ctrl.$onChanges = function () {
        $ctrl.errorMsg = "";
        $ctrl.testObj=null;
        if($ctrl.testId!=undefined && $ctrl.testId!=null){
            $ctrl.showLoading = true;
            testService.getTestFull($ctrl.testId)
                .then(function (resData) {
                    $ctrl.showLoading = false;
                    $ctrl.errorMsg = "";
                    debugger;
                    $ctrl.testObj = resData.data;
                },function (err) {
                    $ctrl.showLoading = false;
                    $ctrl.errorMsg = "Đã xảy ra lỗi kết nối : " + err;
                })
            ;
        }
    }
}