angular.module('edukaApp.component.exam.info.module',[])
    .component("examInfoComponent",{
        templateUrl: "/eduka/components/exam-info/tmpl-exam-info.html",
        controller: examInfoController,
        bindings: {
            examId:'<',
            examObj:'<'
        }
    });

function examInfoController(examService){
    var $ctrl = this;
    $ctrl.showLoading = false;
    $ctrl.errorMsg = "";
    $ctrl.examObj=null;
    $ctrl.$onInit = function () {
        $ctrl.showLoading = false;
    }
    $ctrl.$onChanges = function () {
        $ctrl.errorMsg = "";
        $ctrl.examObj=null;
        if($ctrl.examId!=undefined && $ctrl.examId!=null){
            $ctrl.showLoading = true;
            examService.getExamFull($ctrl.examId)
                .then(function (resData) {
                    $ctrl.showLoading = false;
                    $ctrl.errorMsg = "";
                    debugger;
                    $ctrl.examObj = resData.data;
                },function (err) {
                    $ctrl.showLoading = false;
                    $ctrl.errorMsg = "Đã xảy ra lỗi kết nối : " + err;
                })
            ;
        }
    }
}