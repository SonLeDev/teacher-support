angular.module('edukaApp.component.chapter.unit.module',[])
    .component("chapterUnitSelecterComponent",{
        templateUrl: "/eduka/components/chapter-unit/tmpl-chapter-unit.html",
        controller: chapterUnitController,
        bindings: {
            subjectId:'<' //
            ,onUnitId:'<' // catch onChange on unitId, onChange only happend on one-binding direction
            ,unitId:'=' //
        }
    })
;
function chapterUnitController(adminService,commonUtilsService) {
    var $ctrl = this;
    $ctrl.chapterOption = {};
    $ctrl.unitOption = {};
    $ctrl.chapterIdSelected = "0";
    $ctrl.unitId = $ctrl.unitId;

    function findChapterIdByUnitId(unitId) {
        var chapterId=0;
        $ctrl.chapterOption.some(function(chapter){
            if(chapterId!=0) return true;
            chapter.unitList.some(function (unit) {
                if(unit.unitId == unitId){
                    chapterId = chapter.chapterId;
                    return true;
                }
            })
        })
        return chapterId;
    }

    $ctrl.$onInit = function () {
        $ctrl.chapterIdSelected = "0";
        adminService.getChapterUnits($ctrl.subjectId)
            .then(function (data) {
                $ctrl.chapterOption = data.data;
                /*
                    - case edit, we already have unitId of quiz
                    - must to find appropriate chapter option
                 */
                if($ctrl.unitId!=undefined && $ctrl.unitId !=null){
                    $ctrl.unitIdSelected = ""+$ctrl.unitId;
                    $ctrl.chapterIdSelected = ""+findChapterIdByUnitId($ctrl.unitId);
                    $ctrl.updateUnitOption();
                }
        });
    };

    $ctrl.$onChanges = function(changesObj){
        if(commonUtilsService.isObjectNotEmpty(changesObj.onUnitId)
            && commonUtilsService.isObjectNotEmpty(changesObj.onUnitId.currentValue)
            && changesObj.onUnitId.currentValue !=changesObj.onUnitId.previousValue
        ){
            $ctrl.unitId = changesObj.onUnitId.currentValue;
            $ctrl.unitIdSelected = ""+$ctrl.unitId;
            $ctrl.chapterIdSelected = ""+findChapterIdByUnitId($ctrl.unitId);
            $ctrl.updateUnitOption();
        }else{
            $ctrl.unitOption = [];
            $ctrl.chapterIdSelected = 0;
        }

        // console.log("change input");
    }

    $ctrl.updateUnitOption = function () {
        $ctrl.chapterOption.some(function(item){
            if(item.chapterId== parseInt($ctrl.chapterIdSelected)){
                $ctrl.unitOption = item.unitList;
                return true;
            }
        })
    }
    $ctrl.updateUnitId = function () {
        $ctrl.unitId = $ctrl.unitIdSelected;
    }
    
}