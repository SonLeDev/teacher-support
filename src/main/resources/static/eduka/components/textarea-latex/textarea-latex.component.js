angular.module('edukaApp.component.textarea.latex.module',[])
    .component("textareaLatexComponent",{
        templateUrl: "/eduka/components/textarea-latex/tmpl-textarea-latex.html",
        controller: textareaLatexController,
        bindings: {
            textId:'@'
            ,textValue:'='
            ,placeHolder:'@'
        }
    })
;

function textareaLatexController() {
    var $ctrl = this;
    $ctrl.$onInit = function () {
        $ctrl.wirisClass = "hide_wiris";
        $ctrl.showWiris = false;
    }

    $ctrl.showWirisDialog = function () {
        $ctrl.wirisClass = "show_wiris";
        $ctrl.showWiris = true;
    }
    $ctrl.offWirisDialog = function () {
        $ctrl.showWiris = false;
        $ctrl.wirisClass = "hide_wiris";
        var formular = document.getElementById('iframe_'+$ctrl.textId).contentWindow.getData();
        if($ctrl.textValue === undefined || $ctrl.textValue == null) $ctrl.textValue = "";
        if(formular!=null && formular.trim() !=""){
            $ctrl.textValue = $ctrl.textValue
                .concat(" ")
                .concat("$")
                .concat(formular)
                .concat("$");
            console.log(formular);
        }
    }
    $ctrl.breakLine = function (e) {
        var breakSymbol = " \\n \n ";
        if($ctrl.textValue === undefined || $ctrl.textValue == null) $ctrl.textValue = "";
        $ctrl.textValue = $ctrl.textValue.concat(" ").concat(breakSymbol);
    }

}

