angular.module('edukaApp.home.menu.module', [])
    .controller('leftsideController',function ($scope,adminService) {

    $scope.user = $scope.$parent.user;
    $scope.activeMenuMath = '';
    $scope.showChildMenuMath = '';
    $scope.activeMenuPhysic = '';
    $scope.showChildMenuPhysic = '';

    $scope.activeMenuChemistry = '';
    $scope.showChildMenuChemistry = '';

    $scope.activeMenuEnglish = '';
    $scope.showChildMenuEnglish = '';


    $scope.activeMenuManage = '';
    $scope.showChildMenuManage = '';

    $scope.menuPractMath = "home.content.practQuizListState({subjectId:1})";

    debugger;
    if($scope.user.routers == undefined || $scope.user.routers == null){
        adminService.getRouters($scope.user.id).then(
            function (resData) {
                // cache menu to user
                $scope.user.routers = resData.data;
                $scope.routers = $scope.user.routers;            },
            function (errData) {
                // TODO : what should to do when empty or error router ???
            });
    }else {
        $scope.routers = $scope.user.routers;
    }


    $scope.onClickActiveMenu = function (router) {
        debugger;
        for(var idex in $scope.routers){
            $scope.routers[idex].activeGroup = "";
            $scope.routers[idex].activeChild = "";
        }
        router.activeGroup = "active";
        router.activeChild = "active-child";
    }
})
    .controller('topsideController',function ($scope) {
        $scope.user = $scope.$parent.user;
    })
;