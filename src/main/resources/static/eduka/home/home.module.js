angular.module('edukaApp.home.module', [])
.controller('homeController', function($scope
        ,$rootScope, $state
        ,$cookies,$cookieStore) {

    console.log("router to homeState");
    $state.go('home.content'); // loading all parts of home page

    $rootScope.loading =false;
    $rootScope.subjectId = 0;
    $rootScope.classId = 0;
    // user existed in warehouse after login successful
    $scope.permit = {
        menu:{
            math:false,
            physic:false,
            all:false
        }
    }

    if($scope.user === undefined){
        //try to get user in cookie
        ////////////////////////////////////
        // Very useful when refresh page
        ///////////////////////////////////
        $scope.user = $cookieStore.get('eduka');
    }

    if($scope.user === undefined || $scope.user == null){
        // don't see user in cookie and warehouse, that mean should logout
       $state.go('login');
    }else{
        $rootScope.subjectId = $scope.user.subjectId;
        $rootScope.classId = $scope.user.classId;
        if($scope.user.permit!=undefined && $scope.user.permit!=null){
            $scope.permit = {
                menu:{
                    math:$scope.user.permit.math,
                    physic:$scope.user.permit.physic,
                    all:$scope.user.permit.all
                }
            }
        }
    }
    $scope.logout = function () {
        $rootScope.subjectId = 0;
        $rootScope.classId = 0;
        $cookieStore.remove('eduka');
        $scope.permit = null;
        sessionStorage.clear();
        window.location.href = "/admin/";
    }

})
.controller('contentController',function ($scope,$state,adminService) {

    console.log("router to homeLayoutState : include ui-view: top-left-right");
    $scope.scopeName = "contentScope";

    $scope.unitId = 17;
    $scope.textA  = "";
    $scope.quizId  = 2763;
    $scope.subjectId  = 1;
    $scope.latexData  = "Cho bài toán như sau $ \\sqrt {2x} $";

    $scope.userLogin = adminService.getUserLogin();
    // Must to reset navigateDynamic
    $scope.navigateDynamic = "";
    // $state.go("home.content.dashboardState");
    $state.go("home.content.composePaperState");

    if($scope.userLogin.id == 2){

        $state.go("home.content.composePaperState");

        // $state.go("home.content.reviewQuizState",{subjectId: 1});
        // $state.go('home.content.managePublishingState');
        // $state.go('home.content.testListState',{subjectId:2});

        // $state.go('home.content.examQuizListState',{testId: 32,subjectId:2});
        // $state.go('home.content.examQuizListState',{examId: 325,subjectId:2});
        // $state.go('home.content.practQuizListState',{subjectId:2});

        // $state.go("home.content.dashboardState");

        // $state.go("home.content.examListState",{subjectId: 1});
    }

})

;