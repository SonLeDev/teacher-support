/**
 * Created by sonle on 9/6/17.
 */
angular.module('edukaApp.review.quiz.module', ['ngMaterial'])
    .config(function($mdDateLocaleProvider) {
        $mdDateLocaleProvider.formatDate = function (date) {
            return date ? moment(date).format('DD-MM-YYYY') : '';
        };

        $mdDateLocaleProvider.parseDate = function (dateString) {
            var m = moment(dateString, 'DD-MM-YYYY', true);
            return m.isValid() ? m.toDate() : new Date(NaN);
        }
    })
    .controller('reviewQuizController',function($scope,$stateParams
        ,examService,testService,unitChapterService,userAccountService,quizService
        ,commonUtilsService){

        $scope.subjectId = $stateParams.subjectId;

        $scope.quizList = [];
        $scope.quizObj={};
        $scope.examPapers = [];
        $scope.testPapers = [];
        $scope.unitChapters = [];
        $scope.userAccounts = [];

        $scope.selectedExamId;
        $scope.selectedTestId;
        $scope.selectedUnitId;
        $scope.selectedUserId;
        $scope.selectedDate;

        $scope.loadingToggle = false;

        $scope.quizIsPublished = 'false';

        examService.getBasicExamList($scope.subjectId).then(function (resData) {
            $scope.examPapers = resData.data;
            var startOption = {id:0,name:""}
            $scope.examPapers.unshift(startOption);

        });

        testService.getBasicTestList($scope.subjectId).then(function (resData) {
            $scope.testPapers = resData.data;
            var startOption = {id:0,name:""}
            $scope.testPapers.unshift(startOption);
        })

        unitChapterService.getUnitList($scope.subjectId).then(function (resData) {
            $scope.unitList = resData.data;
            var startOption = {id:0,unitName:""};
            $scope.unitList.unshift(startOption);
        })

        userAccountService.getBasicUserAccounts($scope.subjectId).then(function (resData) {
            $scope.userAccounts = resData.data;
            var startOption = {id:0,name:""}
            $scope.userAccounts.unshift(startOption);
        })

        $scope.onChangeSelectExam = function () {
            $scope.selectedTestId = null;
            $scope.selectedUnitId =null;
        }
        $scope.onChangeSelectTest = function () {
            $scope.selectedExamId = null;
            $scope.selectedUnitId =null;
        }
        $scope.onChangeSelectUnit = function () {
            $scope.selectedExamId = null;
            $scope.selectedTestId = null;
        }

        $scope.onClickFilter = function () {
            $scope.loadingToggle = true;
            $scope.quizList = [];
            var paramJson = {};

            paramJson.published = $scope.quizIsPublished;

            if(commonUtilsService.isObjectNotEmpty($scope.selectedExamId)){
                paramJson.examId = $scope.selectedExamId;
            }else if(commonUtilsService.isObjectNotEmpty($scope.selectedTestId)){
                paramJson.testId = $scope.selectedTestId;
            }
            if(commonUtilsService.isObjectNotEmpty($scope.selectedUnitId)){
                paramJson.unitId = $scope.selectedUnitId;
            }
            if(commonUtilsService.isObjectNotEmpty($scope.selectedUserId)){
                paramJson.userId = $scope.selectedUserId;
            }
            if(commonUtilsService.isObjectNotEmpty($scope.selectedDate)){
                paramJson.updateDate = $scope.selectedDate;
            }

            quizService.filterQuizs(paramJson).then(function (resData) {
                $scope.loadingToggle = false;
                filterSuccessUpdateData(resData);
            },function () {
                $scope.loadingToggle = false;
                alert("Không thể tìm kiếm, có lỗi server.");
            });
        }

        function filterSuccessUpdateData(resData) {
            $scope.quizList = [];

            if(resData.data!=null && resData.data.length > 0){
                $scope.quizList = resData.data;
            }else{
                $scope.quizList = [];
            }
            $scope.timeChangeQuizViewListSize = new Date();

        }

    })
