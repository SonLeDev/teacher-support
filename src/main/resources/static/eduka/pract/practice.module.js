angular.module('edukaApp.practice.module', [])
.factory('practiceService',function (helperService) {

    var changePublishQuiz = function (examId) {
        // return helperService.promisePost("/exam/publish",{"examId":examId});
    }
    return {
        changePublishQuiz:changePublishQuiz
    }
})
.controller('practQuizListController',function ($scope, $state, $stateParams
                        ,adminService,practiceService,quizService) {

    $scope.scopeName = "practQuizListScope";
    // get contentScope and put va

    // checking exiting or not, but how to reset ????
    $scope.$parent.navigateDynamic = " > Câu hỏi ôn tập";

    $scope.paperType == $stateParams.paperType;
    $scope.paperId == $stateParams.paperId;

    $scope.subjectId = $stateParams.subjectId;
    $scope.classId = adminService.getUserLogin().classId;
    $scope.quizList = [];
    $scope.userId = adminService.getUserLogin().id;

    console.log(adminService.getUserLogin());

    $scope.isInputQuiz = false;
    $scope.isNextInputQuiz = false;
    $scope.quizObj ={};
    $scope.resetQuiz =false;
    $scope.timeOnChangeQuiz = new Date();
    $scope.isResetPaging = true;

    $scope.deleteQuizId = 0;
    $scope.showPopupConfirm=false;
    $scope.messageConfirm="";
    $scope.typeConfirm="DELETING_QUIZ";
    $scope.deleteQuizId = 0;
    $scope.isEditState=false;

    updateTableQuizList();

    $scope.createNewQuiz = function () {
        $scope.timeOnChangeQuiz = new Date();
        $scope.quizObj ={};
        $scope.resetQuiz =true;
        $scope.isInputQuiz = true;
    }

    $scope.onEditRow = function (quizId) {
        quizService.getQuiz(quizId)
            .then(function (resData) {
                if(resData.data != undefined && resData.data!=null){
                    doEditQuiz(resData.data);
                }else{
                    alert("Không tìm thấy mã câu hỏi : " + quizId);
                }

            },function (err) {
                alert("Không thể chỉnh sửa câu hỏi : " + quizId);
            });
    }
    $scope.onDeleteQuiz = function (quizId) {
        $scope.typeConfirm="DELETING_QUIZ";
        $scope.showPopupConfirm=true;
        $scope.messageConfirm="Bạn muốn xoá câu hỏi : " + quizId;
        $scope.deleteQuizId = quizId;
    }

    $scope.onCancelInputQuiz = function () {
        $scope.isInputQuiz = false;
        $scope.isEditState=false;
        // $scope.resetQuiz =true;
        if($scope.isNextInputQuiz){
            updateTableQuizList();
            $scope.isNextInputQuiz = false;
        }
    }
    
    $scope.onSaveQuizAndExit = function () {
        $scope.isNextInputQuiz = false;
        doSaveQuiz();
    }

    $scope.onSaveQuizAndNext = function () {
        $scope.isNextInputQuiz = true;
        doSaveQuiz();
    }

    $scope.onConfirmOk = function (typeConfirm) {
        if(typeConfirm=="DELETING_QUIZ"){
            $scope.loadingToggle = true;
            quizService.delQuiz($scope.deleteQuizId)
                .then(function (resData) {
                    doDeletedQuizSuccessful();
                },function (err) {
                    alert("Không thể xoá câu hỏi : " + $scope.deleteQuizId);
                    $scope.deleteQuizId = 0;
                })
        }
        $scope.showPopupConfirm=false;
        $scope.messageConfirm="";
    }

    $scope.onConfirmCancel = function () {
        $scope.showPopupConfirm=false;
        $scope.messageConfirm="";
        $scope.deleteQuizId = 0;
    }


    function updateTableQuizList() {
        $scope.loadingToggle = true;
        quizService.getQuizListByUserIdAndSubjectId($scope.userId,$scope.subjectId)
            .then(function (resData) {
                $scope.quizList = resData.data;
                $scope.isResetPaging = false;
                $scope.onChangeQuizList = new Date();
                $scope.loadingToggle = false;
            });
    }

    function doEditQuiz(quizObj) {
        $scope.timeOnChangeQuiz = new Date();
        $scope.resetQuiz =false;
        $scope.quizObj = quizObj;
        var quizId = ($scope.quizObj.quizId != undefined
                        && $scope.quizObj.quizId != null)? $scope.quizObj.quizId:$scope.quizObj.id;

        // validate before allow edit quiz
        if(quizService.validateBeforeEditQuiz($scope.quizObj)){
            $scope.isInputQuiz = true;
            $scope.isEditState=true;
        }else{
            alert("Câu hỏi có vấn đề, không thể sửa : " + quizId);
        }
    }

    function doSaveQuiz() {
        $scope.loadingToggle = true;
        if($scope.quizObj.quizId == undefined || $scope.quizObj.quizId == null){
            // dong't update userId in the case update quiz
            $scope.quizObj.userId = adminService.getUserLogin().id;
        }
        $scope.quizObj.subjectId = $scope.subjectId;
        $scope.quizObj.noOfQuest = 0;
        $scope.quizObj.isPract = true;

        if(quizService.validateQuiz($scope.quizObj,'practise')){

            if(($scope.quizObj.imgQuestFile!=undefined && $scope.quizObj.imgQuestFile!=null)
                || $scope.quizObj.imgAnswerDetailFile!=undefined && $scope.quizObj.imgAnswerDetailFile!=null){
                /**
                 * Case upload image, using post FormData
                 */
                var quizObjTemp = $scope.quizObj;
                var formData = new FormData();
                formData.append('imgQuestFile', $scope.quizObj.imgQuestFile);
                formData.append('imgAnswerDetailFile', $scope.quizObj.imgAnswerDetailFile);
                formData.append('quizObjJson',angular.toJson(quizObjTemp,true));
                quizService.postSaveQuizFormData(formData)
                    .then(function (resData) {
                        onSaveQuizSuccessful(resData);
                    },function (error) {
                        onSaveQuizFailure(error);
                    });
            }else{
                quizService.postSaveQuiz($scope.quizObj)
                    .then(function (resData) {
                        onSaveQuizSuccessful(resData);
                    },function (error) {
                        onSaveQuizFailure(error);
                    })
            }

        }else{
            $scope.loadingToggle = false;
            alert("Bạn chưa nhập đủ thông tin");
        }
    }
    function onSaveQuizFailure(error) {
        $scope.loadingToggle = false;
        alert("Đã xảy ra lỗi, lưu câu hỏi thất bại");
    }
    function onSaveQuizSuccessful(resData) {
        $scope.loadingToggle = false;
        var quizId = 0;
        if(resData.data!=undefined && resData.data!=null){
            quizId = resData.data.id;
            if(quizId==undefined || quizId == null){
                quizId =resData.data.quizId;
            }
        }
        // show confirm message : save ok with quizId
        $scope.showPopupSuccess = true;
        $scope.successMessage="Lưu thành công, mã câu hỏi : " + quizId;
    }

    $scope.doSuccessFormOk = function () {
        if($scope.isNextInputQuiz){
            // keeping unitId
            var unitIdTmp = $scope.quizObj.unitId;
            $scope.quizObj = {};
            $scope.quizObj.unitId = unitIdTmp;
            $scope.isInputQuiz = true;
        }else{
            updateTableQuizList();
            $scope.quizObj=null;
            $scope.isInputQuiz = false;

        }
        $scope.isEditState=false;
        $scope.showPopupSuccess = false;
        $scope.successMessage="";
    }

    function doDeletedQuizSuccessful() {
        $scope.deleteQuizId = 0;
        updateTableQuizList();
    }

})





;