angular.module('edukaApp.exam.module', [])
.factory('examService',function (helperService) {
    var getExam = function (examId) {
        return helperService.promiseGet("/exam/"+examId);
    }
    var getExamFull = function (examId) {
        return helperService.promiseGet("/exam/"+examId+"/full");
    }
    var getBasicExamList = function (subjectId) {
        return helperService.promiseGet("/subject/"+subjectId+"/exam/basic/list");
    }
    var getExamList = function (subjectId) {
        return helperService.promiseGet("/subject/"+subjectId+"/exam/list");
    }
    var getExamListByUser = function (paramJson) {
        return helperService.promiseSimplePost("/exam/list/by/user",paramJson);
    }
    var saveExam = function (examJson) {
        return helperService.promisePost("/exam/save",examJson);
    }
    var changePublishExam = function (examId) {
        return helperService.promisePost("/exam/publish",{"examId":examId});
    }
    var getMaxNoOfQuiz = function (examId) {
        return helperService.promiseGet("/exam/"+examId+"/maxNoOfQuiz");
    }
    var deleteExam = function (examId) {
        return helperService.promisePost("/exam/delete",{"examId":examId})
    }
    return {
        getExam:getExam
        ,getExamFull:getExamFull
        ,getExamList: getExamList
        ,getBasicExamList:getBasicExamList
        ,saveExam:saveExam
        ,getExamListByUser:getExamListByUser
        ,changePublishExam:changePublishExam
        ,getMaxNoOfQuiz:getMaxNoOfQuiz
        ,deleteExam:deleteExam
    }
})
.controller('examListController', function($scope,$rootScope,$state,$stateParams
                ,examService,commonUtilsService,adminService,quizService) {

    $scope.scopeName = "examListScope";
    // get contentScope and put va

    // checking exiting or not, but how to reset ????
    $scope.$parent.navigateDynamic = " > Đề thi tốt nghiệp";
    $scope.exam = {};
    // init default input exam paper for class 12
    // $scope.exam.classId = $rootScope.classId;
    // $scope.exam.subjectId = $rootScope.subjectId;

    $scope.subjectId = $stateParams.subjectId;
    $scope.classId = adminService.getUserLogin().classId;

    $scope.examList = [];
    $scope.author = adminService.getUserLogin().id;
    $scope.showPanel = false;

    $scope.edukaPanel={};
    $scope.edukaPanel.title = "Tạo đề thi";

    $scope.panel = {};
    $scope.panel.errMsg = "";

    examService.getExamListByUser(
        {"classId":""+$scope.classId,"subjectId":$scope.subjectId,"author":$scope.author})
        .then(function (data) {
        $scope.examList = data.data;
    });

    $scope.openNewExamPanel = function () {
        $scope.exam = {};
        $scope.exam.numOfQuiz = 50;
        $scope.exam.timeExpire = 90;
        $scope.showPanel = true;
    }

    $scope.onSaveAndExit = function () {
        var userLogin = adminService.getUserLogin()
        $scope.exam.author = userLogin.id;
        $scope.exam.subjectId = $scope.subjectId;
        $scope.exam.classId = $scope.classId;
        debugger;
        if(validateNewExam()){
            examService.saveExam($scope.exam).then(function (resData) {
                debugger;
                if(resData.data.examId == undefined || resData.data.examId == null){
                    resData.data.examId = resData.data.id;
                }
                var isAdded = false;
                // case for edit, need to replace the old
                for(var i = 0; i < $scope.examList.length ; i++) {
                    if(resData.data.examId == $scope.examList[i].examId){
                        $scope.examList[i] = resData.data;
                        isAdded = true;
                        break;
                    }
                }
                if(!isAdded){
                    $scope.examList.push(resData.data);
                }
                closeNewExamPanel();
            },function (err) {
                alert("Không thể lưu bài thi, liên hệ admin để khắc phục lỗi : " + err);
            });
        }
    }

    $scope.onPublish = function (examId) {
        debugger;
        examService.changePublishExam(examId)
            .then(function (resData) {
                console.log("...");
            })
    }
    
    $scope.onEditExam = function (examId) {
        debugger;
        // $scope.exam = {};
        $scope.exam={};
        for(var i = 0; i < $scope.examList.length ; i++){
            if(examId == $scope.examList[i].examId){
                $scope.exam = $scope.examList[i];

                if($scope.exam.timeExpire!=undefined && $scope.exam.timeExpire!=null){
                    $scope.exam.timeExpire =  parseInt($scope.exam.timeExpire);
                }else{
                    $scope.exam.timeExpire = 0;
                }
                break;
            }
        }
        $scope.showPanel = true;
    }
    $scope.onDeleteExam = function (examId) {
        examService.deleteExam(examId).then(function (resData) {
            for(var i = 0; i < $scope.examList.length ; i++){
                if(examId == $scope.examList[i].examId){
                    $scope.examList.splice(i,1);
                    break;
                }
            }

        })
    }
    $scope.onSaveAndNext = function () {
        $scope.exam = {};
    }

    $scope.onExit = function () {
        closeNewExamPanel();
    }

    function closeNewExamPanel() {
        $scope.showPanel = false;
    }
    function validateNewExam() {
        var valid = true;
        $scope.panel.errMsg = "";

        /*if(commonUtilsService.isEmpty($scope.exam.codeExam)){
            $scope.panel.errMsg = "Xin hãy nhập Mã đề";
            valid =false;
        }else */
        if(commonUtilsService.isEmpty($scope.exam.examName)){
            $scope.panel.errMsg = "Xin hãy nhập Tên đề";
            valid =false;
        }else if(commonUtilsService.isEmpty($scope.exam.numOfQuiz)
                    || $scope.exam.numOfQuiz <= 0){
            $scope.panel.errMsg = "Số câu hỏi phải lớn hơn 0";
            valid =false;
        }else if(commonUtilsService.isEmpty($scope.exam.timeExpire)
                || $scope.exam.timeExpire <= 0){
            $scope.panel.errMsg = "Thời gian làm bài phải lớn hơn 0 phút";
            valid =false;
        }
        return valid;

    }


})
.controller('examQuizListController',function ($scope, $state, $stateParams
    ,quizService,examService,testService,adminService) {

    $scope.examId = $stateParams.examId;
    $scope.testId = $stateParams.testId;

    $scope.isExaminationType = false;
    $scope.isTestingType = false;
    $scope.titleForm ="";
    $scope.namePaper ="";

    debugger;
    if($scope.examId!=undefined && $scope.examId!=null && $scope.examId!=0){
        $scope.isExaminationType = true;
        $scope.titleForm = "Đề Thi Tốt Nghiệp : "
        examService.getExam($scope.examId).then(function (resData) {
            if(resData.data!=undefined && resData.data!=null){
                $scope.namePaper=resData.data.examName + " ["+resData.data.id+"]";
            }
        })
    }else if($scope.testId != undefined || $scope.testId!=null && $scope.testId!=0){
        $scope.isTestingType = true;
        $scope.titleForm = "Ôn Tập Kiểm Tra : "
        testService.getTestPaper($scope.testId).then(function (resData) {
            if(resData.data!=undefined && resData.data!=null){
                $scope.namePaper=resData.data.name + " ["+resData.data.id+"]";
            }
        })
    }

    $scope.subjectId = $stateParams.subjectId;
    $scope.userId = adminService.getUserLogin().id;

    $scope.noOfQuest = 1;

    $scope.quizList = [];
    $scope.onChangeQuizList = new Date();
    $scope.isResetPaging = true;

    $scope.isInputQuiz = false;
    $scope.quizObj=null;
    $scope.resetQuiz =false;
    $scope.isNextInputQuiz = false;

    $scope.isEditState=false;

    $scope.loadingToggle = false;

    $scope.showPopupSuccess = false;
    $scope.successMessage;

    $scope.typeConfirm="DELETING_QUIZ";
    $scope.showPopupConfirm=false;
    $scope.messageConfirm="";
    $scope.deleteQuizId = 0;

    /*if($scope.examId===undefined
     || $scope.examId == null
     || $scope.examId == 0){
     backToExamQuizList();
     }*/

    if($scope.isTestingType){
        testService.getMaxNoOfQuiz($scope.testId)
            .then(function (data) {
                $scope.noOfQuest = 1;
                if(data.data!=undefined && data.data!=null){
                    $scope.noOfQuest = data.data;
                    $scope.noOfQuest++;
                }
            });
    }

    if($scope.isExaminationType){
        examService.getMaxNoOfQuiz($scope.examId)
            .then(function (data) {
                $scope.noOfQuest = 1;
                if(data.data!=undefined && data.data!=null){
                    $scope.noOfQuest = data.data;
                    $scope.noOfQuest++;
                }
            });
    }
    
    

    updateTableQuizList();

    $scope.onClickNamePaper = function () {
        if($scope.isExaminationType){
            $state.go("home.content.examListState",{subjectId: $scope.subjectId});

        }
        if($scope.isTestingType){
            $state.go('home.content.testListState',{subjectId:$scope.subjectId});
        }
    }
    
    $scope.onCreateQuiz = function () {
        $scope.isInputQuiz = true;
        $scope.quizObj={};
        $scope.resetQuiz =true;
    }
    $scope.onCancelCreateQuiz = function () {
        $scope.isInputQuiz = false;
        $scope.isEditState=false;
        if($scope.isNextInputQuiz){
            updateTableQuizList();
            $scope.isNextInputQuiz = false;
        }

    }
    $scope.onSaveQuizAndExit= function () {
        $scope.isNextInputQuiz = false;
        doSaveQuiz();
    }
    $scope.onSaveQuizAndNext =function () {
        // keep state
        $scope.isNextInputQuiz = true;
        doSaveQuiz();
    }

    $scope.onEditQuizRow = function (quizId) {
        quizService.getQuiz(quizId).then(function (resData) {
            if(resData.data != undefined && resData.data!=null){
                doEditQuiz(resData.data);
            }else{
                alert("Không tìm thấy mã câu hỏi : " + quizId);
            }
        },function (err) {
            alert("Không thể chỉnh sửa câu hỏi : " + quizId);
        })

    }
    $scope.onDeleteQuizRow = function (quizId) {
        // TODO : don't allow deleting quiz from examId, it lead to confuse about the numOfQuest
        /*$scope.typeConfirm="DELETING_QUIZ";
        $scope.showPopupConfirm=true;
        $scope.messageConfirm="Bạn muốn xoá câu hỏi : " + quizId;
        $scope.deleteQuizId = quizId;*/
    }

    $scope.onConfirmOk = function (typeConfirm) {
        if(typeConfirm=="DELETING_QUIZ"){
            $scope.loadingToggle = true;
            quizService.delQuiz($scope.deleteQuizId)
                .then(function (resData) {
                    doDeletedQuizSuccessful();
                },function (err) {
                    alert("Không thể xoá câu hỏi : " + $scope.deleteQuizId);
                    $scope.deleteQuizId = 0;
                })
        }
        $scope.showPopupConfirm=false;
        $scope.messageConfirm="";
    }

    function doDeletedQuizSuccessful() {
        $scope.deleteQuizId = 0;
        updateTableQuizList();
    }

    function doEditQuiz(resQuizObj) {
        // $scope.timeOnChangeQuiz = new Date();
        $scope.resetQuiz =false;
        $scope.quizObj = resQuizObj;
        var quizId = ($scope.quizObj.quizId != undefined
        && $scope.quizObj.quizId != null)? $scope.quizObj.quizId:$scope.quizObj.id;

        // validate before allow edit quiz
        if(quizService.validateBeforeEditQuiz($scope.quizObj)){
            $scope.isInputQuiz = true;
            $scope.isEditState=true;
        }else{
            alert("Câu hỏi có vấn đề, không thể sửa : " + quizId);
        }
    }

    function doSaveQuiz() {
        debugger;

        $scope.loadingToggle = true;

        $scope.quizObj.userId = $scope.userId;
        $scope.quizObj.subjectId = $scope.subjectId;
        $scope.quizObj.examId = $scope.examId;
        $scope.quizObj.testId = $scope.testId;

        var validationType = 'EXAMINATION';
        if($scope.isEditState){
            validationType = 'EXAMINATION_EDIT_QUIZ';
        }

        if($scope.examId==undefined || $scope.examId==null){
            validationType = 'TESTING';
            if($scope.isEditState){
                validationType = 'TESTING_EDIT_QUIZ';
            }
        }

        if(!$scope.isEditState){
            // adding noOfQuest only when create new quiz for exam/testing
            $scope.quizObj.noOfQuest = $scope.noOfQuest;
        }

        if(quizService.validateQuiz($scope.quizObj,validationType)){

            if(($scope.quizObj.imgQuestFile!=undefined && $scope.quizObj.imgQuestFile!=null)
                || $scope.quizObj.imgAnswerDetailFile!=undefined && $scope.quizObj.imgAnswerDetailFile!=null){
                /**
                 * Case upload image, using post FormData
                 */
                var quizObjTemp = $scope.quizObj;
                var formData = new FormData();
                formData.append('imgQuestFile', $scope.quizObj.imgQuestFile);
                formData.append('imgAnswerDetailFile', $scope.quizObj.imgAnswerDetailFile);
                formData.append('quizObjJson',angular.toJson(quizObjTemp,true));
                quizService.postSaveQuizFormData(formData)
                    .then(function (resData) {
                        onSaveQuizSuccessful(resData);
                    },function (error) {
                        onSaveQuizFailure(error);
                    });
            }else{
                quizService.postSaveQuiz($scope.quizObj)
                    .then(function (resData) {
                        onSaveQuizSuccessful(resData);
                    },function (error) {
                        onSaveQuizFailure(error);
                    })
            }

        }else{
            $scope.loadingToggle = false;
            alert("Bạn chưa nhập đủ thông tin");
        }
    }
    function onSaveQuizSuccessful(resData) {
        var quizId = resData.data.id!=undefined?resData.data.id:resData.data.quizId;

        $scope.onChangeQuizList = new Date();
        $scope.loadingToggle = false;

        // show confirm message : save ok with quizId
        $scope.showPopupSuccess = true;
        $scope.successMessage="Lưu thành công, mã câu hỏi : " + quizId;
    }
    
    function onSaveQuizFailure(error) {
        $scope.loadingToggle = false;
        alert("Lỗi server, liên hệ admin để khắc phục.");

    }

    $scope.doSuccessFormOk = function () {
        $scope.noOfQuest++;

        if($scope.isNextInputQuiz){
            debugger;
            // keeping unitId
            var unitIdTmp = $scope.quizObj.unitId;
            $scope.quizObj = {};
            $scope.quizObj.unitId = unitIdTmp;
            $scope.isInputQuiz = true;
        }else{
            updateTableQuizList();
            $scope.quizObj=null;
            $scope.isInputQuiz = false;

        }
        $scope.isEditState=false;
        $scope.showPopupSuccess = false;
        $scope.successMessage="";

    }
    function updateTableQuizList() {
        $scope.loadingToggle = true;
        if($scope.isTestingType){
            quizService.getQuizListByTestId($scope.testId).then(function (data) {
                $scope.onChangeQuizList = new Date();
                $scope.quizList=data.data;
                $scope.isResetPaging = false;
                $scope.loadingToggle = false;
            });
        }else if($scope.isExaminationType){
            quizService.getQuizListByExamId($scope.examId).then(function (data) {
                $scope.onChangeQuizList = new Date();
                $scope.quizList=data.data;
                $scope.isResetPaging = false;
                $scope.loadingToggle = false;
            });
        }
    }
})


;