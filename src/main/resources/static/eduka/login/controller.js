angular.module('login.module', [])
.controller('loginController',function QuizController($scope,$rootScope,$http
    ,$state,$stateParams,$cookies,$cookieStore) {

    $scope.login = function () {
        console.log('--login()');
        $rootScope.loading = true;
        $http({
            method : "POST"
            ,url : "/admin/user/checkLogin"
            ,params:{'userName':$scope.userName,'password':$scope.password }
        }).then(function mySuccess(response) {
            $rootScope.loading = false;
            if (typeof response.data.data == 'undefined' || response.data.data ==  null
                || response.data.data.id==null) {
                console.log('--login() unsuccessful !!! ');
                $scope.error = "Thông tin đăng nhập không chính ";
            } else {
                console.log('--login() successful. ');

                $scope.error = "";
                /*
                    TODO : which storage should to save data on client side ???
                    - noted about the case refresh page

                 */
                debugger;
                $cookieStore.put('eduka', response.data.data);

                window.location.href = "/admin/";

                // TODO : refactor using $location("/admin/");
                // $location.url("/admin/")
            }
        }, function myError(response) {
            console.log(response);
        });
    }
    
})

;