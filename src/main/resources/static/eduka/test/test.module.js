angular.module('edukaApp.test.module', [])
.factory('testService',function (helperService) {
    var getTestFull = function (testId) {
        return helperService.promiseGet("/test/"+testId+"/full");
    }
    var getTestPaper = function (testId) {
        return helperService.promiseGet("/test/"+testId);
    }
    var getBasicTestList = function (subjectId) {
        return helperService.promiseGet("/subject/"+subjectId+"/test/basic/list");
    }

    var getTestList = function (subjectId) {
        return helperService.promiseGet("/subject/"+subjectId+"/test/list");
    }

    var getTestListByUser = function (paramJson) {
        return helperService.promiseSimplePost("/test/list/by/user",paramJson);
    }
    var saveTest = function (examJson) {
        return helperService.promisePost("/test/save",examJson);
    }
    var changePublishTest = function (testId) {
        return helperService.promisePost("/test/publish",{"testId":testId});
    }
    var getMaxNoOfQuiz = function (testId) {
        return helperService.promiseGet("/test/"+testId+"/maxNoOfQuiz");
    }
    var deleteTest = function (testId) {
        return helperService.promisePost("/test/delete",{"testId":testId})
    }
    return {
        getTestFull : getTestFull
        ,getTestPaper : getTestPaper
        ,getBasicTestList: getBasicTestList
        ,getTestList: getTestList
        ,saveTest:saveTest
        ,getTestListByUser:getTestListByUser
        ,changePublishTest:changePublishTest
        ,getMaxNoOfQuiz:getMaxNoOfQuiz
        ,deleteTest:deleteTest
    }
})
.controller('testListController', function($scope,$rootScope,$state,$stateParams
                ,testService,commonUtilsService,adminService) {

    $scope.scopeName = "testListScope";
    // get contentScope and put va

    // checking exiting or not, but how to reset ????
    $scope.$parent.navigateDynamic = " > Ôn tập kiểm tra";

    $scope.authorId = adminService.getUserLogin().id;
    $scope.subjectId = $stateParams.subjectId;

    $scope.test = {};
    // init default input exam paper for class 12
    // $scope.exam.classId = $rootScope.classId;
    // $scope.exam.subjectId = $rootScope.subjectId;

    $scope.classId = 12; // handling findClassIdBySubjectId
    $scope.testList = [];

    $scope.showPanel = false;

    $scope.edukaPanel={};
    $scope.edukaPanel.title = "Tạo bài kiểm tra";

    $scope.panel = {};
    $scope.panel.errMsg = "";


    testService.getTestListByUser(
        {"classId":""+$scope.classId,"subjectId":$scope.subjectId,"author":$scope.authorId})
        .then(function (resData) {
            if(resData.data!=undefined && resData.data!=null){
                $scope.testList = resData.data;
            }
    });

    $scope.openNewPanel = function () {
        $scope.test = {};
        $scope.test.numOfQuiz = 0;
        $scope.test.timeExpire = 0;
        $scope.showPanel = true;
    }

    $scope.onSaveAndExit = function () {
        var userLogin = adminService.getUserLogin()
        $scope.test.authorId = userLogin.id;
        $scope.test.subjectId = $scope.subjectId;
        $scope.test.classId = $scope.classId;
        debugger;
        if(validateNewTest()){
            testService.saveTest($scope.test).then(function (resData) {

                if(resData.data!=undefined && resData.data !=null){
                    var isAdded = false;
                    // case for edit, need to replace the old
                    for(var i = 0; i < $scope.testList.length ; i++) {
                        if(resData.data.id == $scope.testList[i].id){
                            $scope.testList[i] = resData.data;
                            isAdded = true;
                            break;
                        }
                    }
                    if(!isAdded){
                        $scope.testList.push(resData.data);
                    }
                }
                closeNewPanel();
            },function (err) {
                alert("Không thể tạo bài kiểm tra, liên hệ admin : " + err);
            });
        }
    }

    $scope.onPublish = function (testId) {
        testService.changePublishTest(testId)
            .then(function (resData) {
                console.log("...");
            })
    }

    $scope.onEditTest = function (testId) {
        $scope.test = {};
        for(var i = 0; i < $scope.testList.length ; i++){
            if(testId == $scope.testList[i].id){
                $scope.test = $scope.testList[i];
                break;
            }
        }
        $scope.showPanel = true;
    }
    
    $scope.onDeleteTest = function (testId) {
        testService.deleteTest(testId).then(function (resData) {
            for(var i = 0; i < $scope.testList.length ; i++){
                if(testId == $scope.testList[i].id){
                    $scope.testList.splice(i,1);
                    break;
                }
            }
        })
    }
    
    $scope.onSaveAndNext = function () {
        $scope.exam = {};
    }

    $scope.onExit = function () {
        closeNewPanel();
    }

    function closeNewPanel() {
        $scope.showPanel = false;
    }
    function validateNewTest() {
        var valid = true;
        $scope.panel.errMsg = "";

        /*if(commonUtilsService.isEmpty($scope.test.code)){
            $scope.panel.errMsg = "Xin hãy nhập Mã KTra";
            valid =false;
        }else */
        if(commonUtilsService.isEmpty($scope.test.name)){
            $scope.panel.errMsg = "Xin hãy nhập Tên KTra";
            valid =false;
        }else if(commonUtilsService.isEmpty($scope.test.numOfQuiz)
                    || $scope.test.numOfQuiz <= 0){
            $scope.panel.errMsg = "Số câu hỏi phải lớn hơn 0";
            valid =false;
        }else if(commonUtilsService.isEmpty($scope.test.timeExpire)
                || $scope.test.timeExpire <= 0){
            $scope.panel.errMsg = "Thời gian làm bài phải lớn hơn 0 phút";
            valid =false;
        }
        return valid;

    }

    $scope.onDetailTestQuiz = function (testId) {
        // go to practice.module with testType[testId]
        $state.go('home.content.examQuizListState',{testId: testId,subjectId:$scope.subjectId});

    }


})

;