/**
 * Created by sonle on 9/7/17.
 */
angular.module('eduka.user.account.module', [])
    .factory('userAccountService',function($http,$q,helperService){

        var getBasicUserAccounts = function(subjectId){
            return helperService.promiseGet("/subject/"+subjectId+"/user/account/basic/list");
        }
        return {
            getBasicUserAccounts:getBasicUserAccounts
        }

    })