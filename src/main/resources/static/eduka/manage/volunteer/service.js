angular.module('admin.report.service.module',[])
.factory('reportUserService', function($http) {

    var reportUserInputQuiz = function () {
        return $http.get("/admin/report/userInputQuiz")
            .then(function (response) {
                return{
                    data:response.data.data
                }
            });
    };

    return{
        reportUserInputQuiz:reportUserInputQuiz,
    }
});