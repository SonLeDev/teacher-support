angular.module('admin.report.module', [])
.controller('reportUserInputQuizController',function ($scope,reportUserService) {
    $scope.status = reportUserService.serviceData;

    reportUserService.reportUserInputQuiz()
            .then(function (data) {
                $scope.reportUserList = data.data;
        });
})

.controller('userDetailQuizController',function ($scope, $state, $stateParams
                ,reportQuizService) {
    $scope.userId = $stateParams.userId;
})

;