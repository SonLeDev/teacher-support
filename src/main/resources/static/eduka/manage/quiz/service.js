/**
 * Created by sonle on 8/6/17.
 */
angular.module('eduka.manage.quiz.service.module',[])
    .factory('reportQuizService',function ($http) {

        var showAllQuiz = function () {
            return $http.get("/admin/manage/quiz")
                .then(function (response) {
                    return{
                        data:response.data.data
                    }
                });
        };

        return{
            showAllQuiz:showAllQuiz
        }
    });
