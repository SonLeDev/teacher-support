angular.module('eduka.manage.quiz.controller.module',[])
    .controller('reportQuizController',function ($scope,reportQuizService) {
        reportQuizService.showAllQuiz()
            .then(function (data) {
                $scope.quizList = data.data;
            });
    });
