/**
 * Created by sonle on 9/6/17.
 */
angular.module('edukaApp.compose.paper.module', ['ngMaterial'])
    .controller('composePaperController',function($scope, $element,$stateParams
        ,adminService,examService,testService,unitChapterService,userAccountService,quizService){

        $scope.formStep = [1,0,0,0,0];
        $scope.crrIndexStep = 0;

        $scope.modeSelectedQuiz = 'random';
        $scope.selectedClassId = 12;
        $scope.selectedSubjectId = 4;
        $scope.selectedPaperTypeId = 1;

        $scope.unitList = [];
        $scope.quizList = [];

        $scope.paperObj={};
        $scope.paperObj.name='ĐỀ KIỂM TRA ĐỊNH KỲ LẦN 1';
        $scope.paperObj.description='Năm học 2017-2018';
        $scope.paperObj.duration=0;

        $scope.paperObj.selectedClassId = $scope.selectedClassId;
        $scope.paperObj.subjectId = $scope.selectedSubjectId;
        $scope.paperObj.selectedPaperTypeId = $scope.selectedPaperTypeId;

        $scope.paperObj.numOfQuiz = 20;

        $scope.selectedUnitIds = [];
        $scope.selectedQuizIds = [];
        $scope.quizListPreview = [];


        $scope.getSubjectName = function(){
            return adminService.getSubjectName($scope.selectedSubjectId);
        }

        $scope.subjectName = $scope.getSubjectName();

        $scope.$watchGroup(['selectedSubjectId'], function () {
            $scope.selectedUnitIds = [];
            $scope.unitList = [];

            $scope.selectedQuizIds = [];
            $scope.quizList = [];

            $scope.quizListPreview = [];
        });

        $scope.$watchGroup(['paperObj.numOfQuiz','modeSelectedQuiz'], function () {
            $scope.selectedQuizIds = [];
            $scope.quizList = [];
            $scope.quizListPreview = [];
        });

        $scope.$watchCollection('selectedUnitIds', function () {
            $scope.selectedQuizIds = [];
            $scope.quizList = [];
            $scope.quizListPreview = [];
        });

        $scope.$watchCollection('selectedQuizIds', function () {
            $scope.quizListPreview = [];
        });

        $scope.getUnitList = function () {
            $scope.unitList=[];
            $scope.quizList=[];
            unitChapterService.getUnitSelectedList($scope.selectedSubjectId).
            then(function (resData) {
                $scope.unitList = resData.data;
            });
        }

        $scope.selectUnit = function (unitObj) {
            console.log(unitObj);
            // extract unitId that selected
            if(unitObj.wanted){
                $scope.selectedUnitIds.push(unitObj.unitId);
            }else{
                var index = $scope.selectedUnitIds.indexOf(unitObj.unitId);
                if (index > -1) {
                    $scope.selectedUnitIds.splice(index, 1);
                }
            }
        }

        $scope.selectQuiz = function (quizObj) {
            if(quizObj.wanted){
                $scope.selectedQuizIds.push(quizObj.quizId);
            }else{
                var index = $scope.selectedQuizIds.indexOf(quizObj.quizId);
                if (index > -1) {
                    $scope.selectedQuizIds.splice(index, 1);
                }
            }
        }

        $scope.onFilterQuizs = function () {
            var paramJson = {};

            paramJson.selectedUnitIds = $scope.selectedUnitIds;

            $scope.selectedQuizIds = [];
            $scope.quizList = [];
            $scope.quizListPreview = [];
            quizService.filterQuizs(paramJson).then(function (resData) {
                $scope.quizList = resData.data;
            },function () {
                alert("Không thể tìm kiếm, có lỗi server.");
            });
        }
        
        $scope.onRandomFilterQuizs = function () {
            var paramJson = {};

            paramJson.selectedUnitIds = $scope.selectedUnitIds;
            paramJson.randomQuiz = true;
            paramJson.limitedRow = $scope.paperObj.numOfQuiz;

            $scope.selectedQuizIds = [];
            $scope.quizList = [];
            $scope.quizListPreview = [];
            quizService.filterQuizs(paramJson).then(function (resData) {
                $scope.quizList = resData.data;
                for(var idex in $scope.quizList){
                    if($scope.quizList[idex].wanted){
                        $scope.selectedQuizIds.push($scope.quizList[idex].quizId);
                    }
                }
            },function () {
                alert("Không thể tìm kiếm, có lỗi server.");
            });
        }
        
        $scope.previewPaper = function () {
            var paramJson = {};
            paramJson.selectedQuizIds = $scope.selectedQuizIds;
            $scope.quizListPreview = [];
            quizService.filterQuizs(paramJson).then(function (resData) {
                $scope.quizListPreview = resData.data;
            },function () {
                alert("Không thể tìm kiếm, có lỗi server.");
            });
        }

        $scope.onClickNext = function () {
            $scope.crrIndexStep ++ ;
            for(var idx in $scope.formStep){
                $scope.formStep[idx] = 0;
                if(idx == $scope.crrIndexStep){
                    $scope.formStep[idx] = 1;
                }
            }
            $scope.actionOnStep();
        }
        $scope.onClickBack = function () {
            $scope.crrIndexStep -- ;
            for(var idx in $scope.formStep){
                $scope.formStep[idx] = 0;
                if(idx == $scope.crrIndexStep){
                    $scope.formStep[idx] = 1;
                }
            }
            $scope.actionOnStep();
        }

        $scope.actionOnStep = function () {
            console.log("crrIndexStep : " + $scope.crrIndexStep);
            if($scope.crrIndexStep == 1){
                // validate
            }
            if($scope.crrIndexStep == 2){
                // $scope.selectedQuizIds = [];
                // $scope.quizList = [];
                if($scope.selectedUnitIds.length == 0){
                    $scope.getUnitList();
                }
            }
            if($scope.crrIndexStep == 3){
                if($scope.selectedUnitIds.length > 0 && $scope.quizList.length == 0){
                    if($scope.modeSelectedQuiz == 'random'){
                        $scope.onRandomFilterQuizs();
                    }else{
                        $scope.onFilterQuizs();
                    }
                }
            }
            if($scope.crrIndexStep == 4){
                $scope.previewPaper();
            }
        }

    })
