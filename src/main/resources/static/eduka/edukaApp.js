/**********************************
 * **************************************************************
  This is the entry to AngularJS
    - Register dependencies
    - Applying UI-Router
 ***************************************************************
 * ************************************/

var depenModules = [
    'ui.router'
    ,'ngCookies'
    ,'ngMaterial'
    // ,'edukaApp.component.action.bar.module'
    ,'edukaApp.directive.common.form.module'
    ,'edukaApp.component.spinner.loading.module'
    ,'edukaApp.component.quiz.list.module'
    ,'edukaApp.component.quiz.view.module'
    ,'edukaApp.component.chapter.unit.module'
    ,'edukaApp.component.textarea.latex.module'
    ,'edukaApp.component.confirm.form.module'
    ,'edukaApp.component.general.form.module'
    ,'edukaApp.component.input.math.module'
    ,'edukaApp.component.create.quiz.module'
    ,'edukaApp.component.input.quiz.module'
    ,'edukaApp.component.publish.statistic.module'
    ,'edukaApp.component.exam.info.module'
    ,'edukaApp.component.test.info.module'


    ,'edukaApp.manage.publishing.module'

    ,'edukaApp.service.module'
    ,'edukaApp.home.module'
    ,'edukaApp.home.menu.module'
    ,'edukaApp.home.dashboard.module'

    ,'edukaApp.test.module'
    ,'edukaApp.exam.module'
    ,'edukaApp.practice.module'
    ,'edukaApp.review.quiz.module'

    ,'edukaApp.quiz.service.module'
    ,'edukaApp.quiz.view.module'
    ,'edukaApp.quiz.report.error.module'

    ,'edukaApp.compose.paper.module'

    ,'edukaApp.chapter.module'

    ,'eduka.unit.chapter.module'
    ,'eduka.user.account.module'

    ,'login.module'


    ,'edukaApp.broadcast.module'
    ,'chapter.service.module'

    ,'admin.report.module'
    ,'admin.report.service.module'
    ,'eduka.manage.quiz.controller.module'
    ,'eduka.manage.quiz.service.module'


    ,'ngCkeditor'];

/*
    Using to inter-page redirect to home page
*/
var redirectState = {
    url:""
    ,templateUrl:'/eduka/redirect.html'
    ,controller:'redirectController'
}
var loginState = {
    url: "/login"
    ,templateUrl: '/eduka/login/login.html'
    ,params: { objParam: null } // null is the default value
    ,controller: 'loginController'
}

/*view-ui*/
var homeState = {
    url:"/home"
    ,templateUrl:'/eduka/home/home.html'
    ,controller:'homeController'
}
/*
    view-ui
        |_ top   : topside.html
        |__left  : sidebar.html
        |__right : content.html
*/
var homeLayoutState ={
    url:"/"
    ,views:{
        'top':{
            templateUrl:'/eduka/home/menu/topside.html'
            ,controller:'topsideController'
        }
        ,'left':{
            templateUrl:'/eduka/home/menu/leftside.html'
            ,controller:'leftsideController'
        }
        ,'right':{
            templateUrl:'/eduka/home/content.html'
            ,controller:'contentController'
        }
    }
}

/*
 view-ui
     |__left : sidebar
     |__right : content
        |__volunteerState
 */

var dashboardState = {
    url:"dashboard"
    ,views:{
        'content':{
            templateUrl:'/eduka/home/dashboard/tmpl_dashboard.html'
            ,controller: 'dashboardController'
        }
    }
}

var volunteerState ={
    url: "volunteer"
    ,views:{
        'content':{
            templateUrl:'/eduka/manage/volunteer/tmpl_dash_board.html'
            ,controller: 'reportUserInputQuizController'
        }
    }
}


var practQuizListState = {
    url:"practice/quiz/list?subjectId:paperType:paperId"
    ,views:{
        'content':{
            templateUrl: '/eduka/pract/tmpl-quiz-list.html'
            ,controller: 'practQuizListController'
        }
    }
}

var examListState = {
    url: "exam/list?subjectId"
    ,views:{
        'content':{
            templateUrl: '/eduka/exam/tmpl-exam-list.html'
            ,controller: 'examListController'
        }
    }
}

var examQuizListState = {
    url:"exam/quiz/list?examId:testId:subjectId"
    ,views:{
        'content':{
            templateUrl: '/eduka/exam/tmpl-exam-quiz-list.html'
            ,controller: 'examQuizListController'
        }
    }
}


var testListState={
    url: "test/list?subjectId"
    ,views:{
        'content':{
            templateUrl: '/eduka/test/tmpl-test-list.html'
            ,controller: 'testListController'
        }
    }
}
var testQuizListState={
    url: "test/quiz/list?subjectId:testId"
    ,views:{
        'content':{
            templateUrl: '/eduka/test/tmpl-test-quiz-list.html'
            ,controller: 'testQuizListController'
        }
    }
}

var reviewQuizState={
    url:"review/quiz?subjectId"
    ,views:{
        'content':{
            templateUrl: '/eduka/review/quiz/tmpl-review-quiz.html'
            ,controller: 'reviewQuizController'
        }
    }
}

var managePublishingState={
    url: "manage/publishing"
    ,views: {
        'content': {
            templateUrl:'/eduka/manage/publishing/tmpl_publishing.html'
            ,controller: 'publishingController'
        }
    }
}


var detailQuiz={
    url: "quiz?quizId"
    ,views:{
        'content':{
            templateUrl: '/eduka/quiz/view/tmpl-quiz-view.html' // The component's name
            ,controller:'quizViewController'
        }
    }
}

var quizReportErrorState={
    url:"quiz/report?subjectId"
    ,views:{
        'content':{
            templateUrl: '/eduka/quiz/report-error/tmpl-quiz-report-error.html'
            ,controller:'quizReportErrorController'
        }
    }
}

var chapterListState={
    url: "/chapter/list?subjectId"
    ,views:{
        'content':{
            templateUrl: '/eduka/chapter/list.html'
            ,controller: 'chapterListController'
        }
    }

}

var unitListState={
    url: "/chapter/unit/list?chapterId"
    ,views: {
        'content': {
            templateUrl: '/eduka/chapter/unit.list.html'
            ,controller: 'unitController'
        }
    }
}

var volunteerQuizState ={
    url: "/volunteer/quiz?userId"
    ,views: {
        'content': {
            templateUrl:'/eduka/manage/volunteer/tmpl_volunteer_quiz.html'
            ,controller: 'userDetailQuizController'
        }
    }
}

var composePaperState ={
    url: "compose/paper"
    ,views: {
        'content': {
            templateUrl:'/eduka/compose-paper/tmpl-compose-paper.html'
            ,controller: 'composePaperController'
        }
    }
}

var manageQuizState={
    url: "/manage/quiz"
    ,templateUrl:'/eduka/manage/quiz/tmpl_dash_board.html'
    ,controller:'reportQuizController'
}


/**
 * Firstly, using mul-view to layout : menu, header, footer and content
 * Secondly, using nest-view to show forms inside ui-content
 * Thirdly, using
 *
 * Layout combine with url
 * entry : /admin/  ->  $urlRouterProvider.otherwise('/') : all unknown link
 *                  -> redirectState:redirectState
 * redirectState -> redirectController :|__login?false -> login:loginState
 *                                      |__login?true -> home:homeState
 *  homeState-> homeController: { - initData
 *                              { -> home.content:homeLayoutState
 *  homeLayoutState (home.content)
 *     -> show  { - top  : banner   : show user-profile, notification, processing,...
 *              { - left : menu     : control menu redirect base on user-permission
 *              { - right : content : all the user form stand here.
 *
 *   ...all the content have the prefix state: home.content....
 *
 *
 *
 */

var edukaApp = angular.module('edukaApp',depenModules)
     .config(function ($stateProvider,   $urlRouterProvider) {
            $urlRouterProvider.otherwise('/'); // redirect all unknown url to redirectState-state
             $stateProvider
                    .state("redirectState",redirectState)
                    .state("login", loginState)
                    .state("home",homeState)
                    .state("home.content",homeLayoutState)
                    .state("home.content.dashboardState",dashboardState)
                    .state("home.content.volunteerState",volunteerState)
                    .state('home.content.examListState',examListState)
                    .state('home.content.examQuizListState',examQuizListState)
                    .state("home.content.practQuizListState", practQuizListState)
                    .state("home.content.testListState", testListState)
                    .state("home.content.testQuizListState", testQuizListState)
                    .state("home.content.detailQuiz",detailQuiz)
                    .state("home.content.reviewQuizState",reviewQuizState)

                    .state("home.content.chapterListState",chapterListState)
                    .state("home.content.unitListState",unitListState)

                    .state("home.content.managePublishingState",managePublishingState)
                    .state("home.content.composePaperState",composePaperState)
                    .state("home.content.quizReportErrorState",quizReportErrorState)

                    .state("volunteerQuizState",volunteerQuizState)
                    .state("manageQuiz",manageQuizState)


    })
    .controller('redirectController', function($scope,$state,$cookieStore) {
        if($scope.user === undefined){
            //try to get user in cookie
            ////////////////////////////////////
            // Very useful when refresh page
            ///////////////////////////////////
            $scope.user = $cookieStore.get('eduka');
        }
        if($scope.user === undefined || $scope.user == null){
            // don't see user in cookie and warehouse, that mean should logout
            $state.go('login');
        }else{
            $state.go('home');
        }
    })
;



