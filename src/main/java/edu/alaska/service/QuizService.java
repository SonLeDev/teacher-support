package edu.alaska.service;

import edu.alaska.builder.EduQuizBuilder;
import edu.alaska.builder.EdukaBuilder;
import edu.alaska.common.AWSUtil;
import edu.alaska.common.Constances;
import edu.alaska.common.Utils;
import edu.alaska.form.QuizForm;
import edu.alaska.form.ReqFilterForm;
import edu.alaska.jdbc.builder.DefaultQueryRowHandler;
import edu.alaska.jdbc.builder.QueryBuilder;
import edu.alaska.jdbc.builder.quiz.QuizSimpleQuerySpec;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import edu.alaska.domain.*;
import edu.alaska.jdbc.mapper.*;
import edu.alaska.repository.*;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.transaction.Transactional;
import java.io.IOException;
import java.util.*;

/**
 * Created by SonLee on 5/26/2017.
 */
@Service
public class QuizService {
    private static final Logger LOGGER = Logger.getLogger(QuizService.class);
    @Autowired
    private EduQuizRepository eduQuizRepository;

    @Autowired
    private ExaminationQuizsRepository examinationQuizsRepository;

    @Autowired
    private EduUnitRepository eduUnitRepository;

    @Autowired
    private TestQuizsRepository testQuizsRepository;

    @Autowired
    private EduQuizImageRepository eduQuizImageRepository;

    @Autowired
    private EduQuizReportsRepository eduQuizReportsRepository;

    @Autowired
    private NamedParameterJdbcTemplate jdbcTemplate;


    public List<EduQuiz> save(List<EduQuiz> quizs){
        return eduQuizRepository.save(quizs);
    }
    
    public List<EduQuiz> getQuizTableByUserId(Long userId){
    	List<EduQuiz> eduQuizs = eduQuizRepository.findQuizByUserId(userId);
    	return eduQuizs;
    }

    public List<EduQuiz> getQuizByUserIdAndSubjectId(Long userId, Long subjectId) {
        return eduQuizRepository.findByUserIdAndSubjectId(userId,subjectId);
    }

    public EduQuiz getQuizTableById(Long quizId) {
        return eduQuizRepository.findOne(quizId);
    }

    public List filterQuizs(ReqFilterForm filterForm) {
        LOGGER.info(String.format("%s filterQuizs ", Constances.LOG_TAG_INFO));

            if(ArrayUtils.isNotEmpty(filterForm.getSelectedQuizIds())){
            EdukaCallbackHandler handler = new ComposePaperPreviewQuizsHandler(filterForm.getSelectedQuizIds());
            jdbcTemplate.query(handler.getQueryStmt(),handler.getParamMapper(),handler);
            List<Map> listQuiz = handler.getRetData();
            return listQuiz;

        }else if(ArrayUtils.isNotEmpty(filterForm.getSelectedUnitIds())){
            EdukaCallbackHandler handler = new ComposePaperFilterQuizsHandler
                    (filterForm.getSelectedUnitIds(),filterForm.getRandomQuiz(),filterForm.getLimitedRow());

            jdbcTemplate.query(handler.getQueryStmt(),handler.getParamMapper(),handler);
            List<Map> listQuiz = handler.getRetData();
            return listQuiz;

        }else if(Boolean.TRUE.equals(filterForm.getReportQuizError())){
            /*
                filter quiz for Report Quiz Error
                - get all quiz is reported by userId
             */
            if(filterForm.getUserId() == null || filterForm.getSubjectId() == null) return null;

            EdukaCallbackHandler handler = new QuizReportErrorHandler(filterForm.getUserId(),filterForm.getSubjectId());
            jdbcTemplate.query(handler.getQueryStmt(),handler.getParamMapper(),handler);
            return handler.getRetData();
        }
        else{
            EduQuizHanlder handler = new EduQuizHanlder();
            jdbcTemplate.query(handler.getQueryByFilterForm(filterForm),handler.getParameterMapping(),handler);
            return handler.getRetQuizs();
        }

    }

    public List getQuizListByUserId(Long userId,Long subjectId){

        LOGGER.info(String.format("%s getQuizListByUserId :: %d", Constances.LOG_TAG_INFO,userId));

        EduQuizHanlder handler = new EduQuizHanlder();
        jdbcTemplate.query(handler.getQueryByUserIdAndSubjectId(userId,subjectId),handler.getParameterMapping(),handler);
        return handler.getRetQuizs();
    }

    public Map getQuizById(Long quizId){

        LOGGER.info(String.format("%s getQuizById :: %d", Constances.LOG_TAG_INFO,quizId));

        EduQuizHanlder handler = new EduQuizHanlder();
        jdbcTemplate.query(handler.getQueryByQuizId(quizId),handler.getParameterMapping(),handler);
        if(CollectionUtils.isNotEmpty(handler.getRetQuizs())){
            return handler.getRetQuizs().get(0);
        }
        return null;
    }

    public List getQuizListByExamId(Long examId) {

        LOGGER.info(String.format("%s getQuizListByExamId :: %d", Constances.LOG_TAG_INFO,examId));

        EduQuizHanlder handler = new EduQuizHanlder();
        jdbcTemplate.query(handler.getQueryByExamId(examId),handler.getParameterMapping(),handler);
        if(CollectionUtils.isNotEmpty(handler.getRetQuizs())){
            return handler.getRetQuizs();
        }
        return null;
    }

    public List getQuizListBySubjectIdAndUserid(Long subjectId, Long userId) {
        LOGGER.info(String.format("%s getQuizListBySubjectIdAndUserid :: %d,%d", Constances.LOG_TAG_INFO,subjectId,userId));
        EduQuizHanlder handler = new EduQuizHanlder();
        jdbcTemplate.query(handler.getQueryBySubjectIdAndUserId(subjectId,userId),handler.getParameterMapping(),handler);
        if(CollectionUtils.isNotEmpty(handler.getRetQuizs())){
            return handler.getRetQuizs();
        }
        return null;
    }

    public List getQuizListByTestId(Long testId) {
        LOGGER.info(String.format("%s getQuizListByTestId :: %d", Constances.LOG_TAG_INFO,testId));
        EduQuizHanlder handler = new EduQuizHanlder();
        jdbcTemplate.query(handler.getQueryByTestId(testId,null),handler.getParameterMapping(),handler);
        if(CollectionUtils.isNotEmpty(handler.getRetQuizs())){
            return handler.getRetQuizs();
        }
        return null;
    }

    public Collection getQuizListByUpdateDate(Date updateDate) {
        LOGGER.info(String.format("%s getQuizListByUpdateDate :: ", Constances.LOG_TAG_INFO));
        EduQuizHanlder handler = new EduQuizHanlder();
        jdbcTemplate.query(handler.getQueryByUpdateDate(updateDate),handler.getParameterMapping(),handler);
        if(CollectionUtils.isNotEmpty(handler.getRetQuizs())){
            return handler.getRetQuizs();
        }
        return null;
    }
    @Transactional
    public EduQuiz saveAndUpload(String quizObjJson, MultipartFile[] file) {
        LOGGER.info("----- saveAndUploadImage()");
        ObjectMapper mapper = new ObjectMapper();
        EduQuiz eduQuiz = null;
        try {
            QuizForm quizForm = mapper.readValue(quizObjJson, QuizForm.class);

            eduQuiz = saveQuizForm(quizForm);

            if (eduQuiz == null){
                return null;
            }
            assert eduQuiz!=null;

            if(file!=null && file.length>0){

                uploadQuizImage2AWS(file, eduQuiz);
            }

            LOGGER.info(String.format("----- saveAndUploadImage() successful, id =%d",eduQuiz.getId()));

        } catch (JsonParseException e1) {
            LOGGER.error(">>>>> saveAndUploadImage >> JsonParseException : " + e1.getMessage());
        } catch (JsonMappingException e1) {
            LOGGER.error(">>>>> saveAndUploadImage >> JsonMappingException : " + e1.getMessage());
        } catch (IOException e1) {
            LOGGER.error(">>>>> saveAndUploadImage >> IOException : " + e1.getMessage());
        } catch (IllegalStateException e) {
            LOGGER.error(">>>>> saveAndUploadImage >> IllegalStateException : " + e.getMessage());
        }

        return eduQuiz;
    }

    private void uploadQuizImage2AWS(MultipartFile[] file, EduQuiz eduQuiz) throws IOException {
        if (eduQuiz != null && eduQuiz.getId() != null && file != null && file.length >0) {
            EduQuizImage eduQImage = eduQuizImageRepository.findByQuizId(eduQuiz.getId());
            if(eduQImage==null){
                eduQImage = new EduQuizImage();
                eduQImage.setQuizId(eduQuiz.getId());
            }
            eduQImage.setMdfDate(Utils.getDateTimeZonVN());

            /**
             * Hard-code here : index 0 - mapping to questImg
             *                  index 1 - mapping to dtailAnsImg
             */
            if (file.length >= 1 && file[0]!= null) {
                String urlAWSImg = AWSUtil.uploadImageToAWSS3(file[0]);
                eduQImage.setQuestImg(urlAWSImg);
            }

            if (file.length >= 2 && file[1]!= null) {
                String urlAWSImg = AWSUtil.uploadImageToAWSS3(file[1]);
                eduQImage.setDtailAnsImg(urlAWSImg);
            }

            eduQuizImageRepository.save(eduQImage);
            LOGGER.debug("----- eduQuizImageRepository.save() successful ");
        }
    }

    /**
     * Save quiz
     *  Attach to ExamId
     *  Attach to TestId
     * @param quizForm
     * @return
     */
    @Transactional
    public EduQuiz saveQuizForm(QuizForm quizForm) {

        LOGGER.info("----- saveQuizForm()");
        Boolean isEditQuiz = false;
        EduUnit eduUnit = getEduUnit(quizForm);
        if (eduUnit == null) return null;

        Long quizId = 0l;
        EduQuiz eduQuizTemp = null;
        if (quizForm!= null
                && (quizForm.getId() != null && quizForm.getId()!=0)
                || (quizForm.getQuizId() != null && quizForm.getQuizId()!=0)) {
            isEditQuiz = Boolean.TRUE;
            quizId = (quizForm.getQuizId() != null&& quizForm.getQuizId() !=0)?quizForm.getQuizId():quizForm.getId();
            eduQuizTemp = eduQuizRepository.findOne(quizId);
        }
        if(eduQuizTemp==null){
            eduQuizTemp = EduQuizBuilder.instance(quizForm,eduUnit).build();
        }else{
            // TODO : onChange when edit
            EduQuizBuilder.onChange(eduQuizTemp,quizForm,eduUnit);
        }
        if (quizForm!= null && quizForm.getId() != null) {
            eduQuizTemp.setId(quizForm.getId());
        }
        if (quizForm!= null && quizForm.getQuizId() != null) {
            eduQuizTemp.setId(quizForm.getQuizId());
        }

        EduQuiz eduQuiz = eduQuizRepository.save(eduQuizTemp);
        LOGGER.info(String.format("saveQuizForm::eduQuizRepository.save::id=%d",eduQuiz.getId()));

        // update remove image or not
        if(isEditQuiz){
            EduQuizImage eduQImage = eduQuizImageRepository.findByQuizId(eduQuiz.getId());
            Boolean isChangedImage = Boolean.FALSE;
            if(eduQImage!=null){
                if(StringUtils.isNotEmpty(eduQImage.getQuestImg())
                        && StringUtils.isEmpty(quizForm.getQuestImg())
                        && (quizForm.getImgQuestFile() == null
                            || quizForm.getImgQuestFile().equals(""))){
                    // remove image of quest
                    eduQImage.setQuestImg("");
                    isChangedImage = Boolean.TRUE;
                }
                if(StringUtils.isNotEmpty(eduQImage.getDtailAnsImg())
                        && StringUtils.isEmpty(quizForm.getDtailAnsImg())
                        && (quizForm.getImgAnswerDetailFile() == null
                            || quizForm.equals(""))){
                    // remove image of detail answer
                    eduQImage.setDtailAnsImg("");
                    isChangedImage = Boolean.TRUE;
                }
                eduQImage.setMdfDate(Utils.getDateTimeZonVN());
                if(isChangedImage = Boolean.TRUE){
                    eduQuizImageRepository.save(eduQImage);
                }
            }
        }

        if(quizForm.getExamId()!=null && quizForm.getExamId()!=0){
            /*
                Case edit quiz : may be has already link quizId and examId
                insure quizId and examId don't exist in exam_quiz
              */
            if(examinationQuizsRepository.findByExamIdAndQuizId(quizForm.getExamId(),eduQuiz.getId()) == null){
                ExaminationQuizs examQuiz = examinationQuizsRepository
                        .save(EdukaBuilder.instance()
                                .buildExamQuiz(quizForm.getExamId(),eduQuiz.getId(),quizForm.getNoOfQuiz()));
                LOGGER.info(String.format("saveQuizForm::examinationQuizsRepository.save::examId=%d, quizId=%d"
                        ,examQuiz.getExamId(),examQuiz.getQuizId()));
            }


        }
        if(quizForm.getTestId()!=null && quizForm.getTestId()!=0){
            if(testQuizsRepository.findByTestIdAndQuizId(quizForm.getTestId(),eduQuiz.getId())==null){
                TestQuizs testQuizs = testQuizsRepository
                        .save(EdukaBuilder.instance()
                                .buildTestQuiz(quizForm.getTestId(),eduQuiz.getId(),quizForm.getNoOfQuiz()));
                LOGGER.info(String.format("saveQuizForm::testQuizsRepository.save::testId=%d, quizId=%d"
                        ,testQuizs.getTestId(),testQuizs.getQuizId()));
            }
        }

        // using for publish unit
        if(eduQuiz.getId()!=null && quizForm.getUnitId()!=null
                && quizForm.getUnitId() !=0){
            UnitQuizs unitQuizs = new UnitQuizs();
            unitQuizs.setQuizId(eduQuiz.getId());
            unitQuizs.setUnitId(quizForm.getUnitId());
            // unitQuizsRepository.save(unitQuizs);
        }

        LOGGER.info(String.format("----- saveQuizForm() successful, id =%d",eduQuiz.getId()));
        return eduQuiz;
    }
    private EduUnit getEduUnit(QuizForm quizForm) {
        if (quizForm.getId() !=null && quizForm.getUnitId()==null){
            LOGGER.error(String.format(">>>>> saveAndUploadImage() unsuccessful !!!, unitId is NULL"));
            return null;
        }
        EduUnit eduUnit = eduUnitRepository.findOne(quizForm.getUnitId());

        if(eduUnit == null){
            LOGGER.error(String.format(">>>>> saveAndUploadImage() unsuccessful !!!, unitId =%d don't exist in EduUnit",quizForm.getUnitId()));
            return null;
        }
        return eduUnit;
    }

    @Transactional
    public Boolean deleteQuiz(Long quizId) {
        Integer rs = eduQuizRepository.updateDeletedQuiz(quizId);
        examinationQuizsRepository.updateDeletedQuiz(quizId);
        testQuizsRepository.updateDeletedQuiz(quizId);
        if(rs > 0){ return Boolean.TRUE; }
        return Boolean.FALSE;
    }

    @Transactional
    public Boolean publishQuiz(Long quizId) {
        return changePublished(quizId);
    }

    @Transactional
    public Boolean unPublishQuiz(Long quizId) {
        return changePublished(quizId);
    }

    private Boolean changePublished(Long quizId){
        ExaminationQuizs examQuiz = examinationQuizsRepository.findByQuizId(quizId);
        if(examQuiz!=null){
            examQuiz.setPublished(!examQuiz.getPublished());
            examinationQuizsRepository.save(examQuiz);
            return Boolean.TRUE;
        }else{
            TestQuizs testQuiz = testQuizsRepository.findByQuizId(quizId);
            if(testQuiz!=null){
                testQuiz.setPublished(!testQuiz.getPublished());
                return Boolean.TRUE;
            }else{
                EduQuiz eduQuiz = eduQuizRepository.findOne(quizId);
                Boolean isPublished = Boolean.TRUE;
                if(eduQuiz!=null){
                    isPublished = eduQuiz.getPublished()!=null?eduQuiz.getPublished():Boolean.FALSE;
                }
                Integer rs = eduQuizRepository.updatePublished(quizId,!isPublished);
                return Boolean.TRUE;
            }
        }
    }

    public Collection getSimpleQuizs(){
        QueryBuilder queryBuilder = new DefaultQueryRowHandler(new QuizSimpleQuerySpec());
        jdbcTemplate.query(queryBuilder.buildQuery(), queryBuilder);
        return queryBuilder.getResultList();
    }

    public Long reportQuiz(Long quizId) {
        EduQuizReports eduQuizReports = eduQuizReportsRepository.findByQuizId(quizId);
        if(eduQuizReports == null){
            eduQuizReports = new EduQuizReports();
            eduQuizReports.setDateReport(Utils.getDateTimeZonVN());
            eduQuizReports.setQuizId(quizId);
            eduQuizReports.setTypeReport("ADMIN REPORT");
            eduQuizReports.setUserIdReport(1l);
        }else{
            //update report error again;
            Boolean status = Boolean.FALSE;
            if(eduQuizReports.getStatusReport()!= null){
                status = eduQuizReports.getStatusReport();
            }
            eduQuizReports.setStatusReport(!status);
        }
        if(eduQuizReports !=null){
            eduQuizReportsRepository.save(eduQuizReports);
        }
        return eduQuizReports.getId();
    }
}
