package edu.alaska.service;

import edu.alaska.builder.PaperTestBuilder;
import edu.alaska.domain.PaperTest;
import edu.alaska.form.PaperTestForm;
import edu.alaska.repository.PaperTestRepository;
import org.apache.commons.lang.ArrayUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by sonle on 7/25/17.
 */
@Service
public class PaperTestService {
    final static Logger LOGGER = Logger.getLogger(PaperTestService.class);

    @Autowired
    private PaperTestRepository paperTestRepository;

    public List<PaperTest> findByAuthorId(Long authorId) {
        return paperTestRepository.findByAuthorId(authorId);
    }

    @Transactional
    public PaperTest save(PaperTestForm paperTestForm) {
        PaperTest paperTest = paperTestRepository.save(PaperTestBuilder.instance().build(paperTestForm));

        if(ArrayUtils.isNotEmpty(paperTestForm.getQuizIds())){
            attachQuizsToExam(paperTest.getId(),paperTestForm.getQuizIds());
        }
        LOGGER.info("save paperTest successful");
        return paperTest;
    }

    @Transactional
    private void attachQuizsToExam(Long testId, Long[] quizIds) {
        /** for simplify the case
            - remove all mapping quizId-testId
            - insert again
          */
        assert testId !=null && testId !=0;
        assert quizIds != null && quizIds.length > 0;

        paperTestRepository.removeAllTestId(testId);

        Set<Long> quizIdSet = new HashSet<>(Arrays.asList(quizIds));
        paperTestRepository.assignQuizToPaperTest(testId,quizIdSet);
        LOGGER.info("assignQuizToPaperTest successful");

    }


    public Object attachQuizsToExam(PaperTestForm paperTestForm) {
        attachQuizsToExam(paperTestForm.getId(),paperTestForm.getQuizIds());
        LOGGER.info("attachQuizsToExam successful");
        return "OK";
    }

    public Object publishQuizsExam(PaperTestForm paperTestForm){
        Set<Long> quizIdSet = new HashSet<>(Arrays.asList(paperTestForm.getQuizIds()));
        paperTestRepository.publishQuizPaperTest(paperTestForm.getId(),quizIdSet);
        LOGGER.info("publishQuizPaperTest successful");
        return "OK";
    }
}
