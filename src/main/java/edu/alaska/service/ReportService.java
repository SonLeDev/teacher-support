package edu.alaska.service;

import edu.alaska.jdbc.mapper.ReportUserInputQuizHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * Created by sonle on 8/6/17.
 */
@Service
public class ReportService {

    @Autowired
    private NamedParameterJdbcTemplate jdbcTemplate;
    public List<Map> reportUserInputQuiz() {
        ReportUserInputQuizHandler queryHandler = new ReportUserInputQuizHandler();
        jdbcTemplate.query(ReportUserInputQuizHandler.QUERY_REPORT_USER_INPUT_QUIZ,queryHandler);
        return queryHandler.getReportData();
    }
}
