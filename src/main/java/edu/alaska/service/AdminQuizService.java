package edu.alaska.service;

import javax.transaction.Transactional;

import edu.alaska.domain.EduQuiz;
import edu.alaska.repository.EduQuizImageRepository;
import edu.alaska.repository.EduQuizRepository;
import edu.alaska.repository.EduUnitRepository;
import edu.alaska.repository.ExaminationQuizsRepository;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

/**
 * Created by sstvn on 6/1/17.
 */
@Service
public class AdminQuizService {

    final static Logger LOGGER = Logger.getLogger(AdminQuizService.class);

    @Autowired
    private EduQuizRepository eduQuizRepository;

    @Autowired
    private ExaminationQuizsRepository examinationQuizsRepository;

    @Autowired
    private EduUnitRepository eduUnitRepository;

    @Autowired
    private EduQuizImageRepository eduQuizImageRepository;

    private QuizService quizService;

    @Transactional
    public EduQuiz saveAndUploadImage(String quizObjJson, MultipartFile[] file) {
        return quizService.saveAndUpload(quizObjJson, file);
    }


}
