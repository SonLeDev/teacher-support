package edu.alaska.service;

import edu.alaska.common.AWSUtil;
import edu.alaska.domain.EduChapters;
import edu.alaska.domain.EduUnit;
import edu.alaska.form.ChaptersUnitForm;
import edu.alaska.jdbc.QueryTemplate;
import edu.alaska.jdbc.mapper.ChaptersUnitHandler;
import edu.alaska.jdbc.mapper.EduUnitSelectFieldHandler;
import edu.alaska.repository.EduChaptersRepository;
import edu.alaska.repository.EduUnitRepository;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.transaction.Transactional;
import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by sonle on 7/12/17.
 */
@Service
public class ChapterService {

    @Autowired
    EduChaptersRepository eduChaptersRepository;

    @Autowired
    EduUnitRepository eduUnitRepository;

    @Autowired
    NamedParameterJdbcTemplate jdbcTemplate;

    public List<EduChapters> getChapterList(Long classId, Long subjectId) {
        return eduChaptersRepository.findByClassIdAndSubjectId(classId,subjectId);
    }

    public List<EduUnit> getUnitByChapterId(Long chapterId) {
        return eduUnitRepository.findByChapterId(chapterId);
    }

    @Transactional
    public EduChapters saveChapterAndUploadImage(String chapterJson, MultipartFile iconFile) {
        ObjectMapper mapper = new ObjectMapper();
        EduChapters chapter = null;
        try {
            chapter = mapper.readValue(chapterJson, EduChapters.class);
            String urlAWSImg = "";
            if(iconFile!=null){
                urlAWSImg = AWSUtil.uploadImageToAWSS3(iconFile);
            }
            // case edit, don't update when urlAWSImg is empty
            if(chapter.getId()==null || !urlAWSImg.equals("")){
                chapter.setChpIcon(urlAWSImg);
            }
            eduChaptersRepository.save(chapter);
        }  catch (JsonParseException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        } catch (JsonMappingException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        } catch (IOException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        } catch (IllegalStateException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return chapter;
    }

    public EduUnit saveUnitAndUpload(String unitJson, MultipartFile iconFile) {

        ObjectMapper mapper = new ObjectMapper();
        EduUnit unit = null;
        try {
            unit = mapper.readValue(unitJson, EduUnit.class);
            String urlAWSImg = "";
            if(iconFile!=null){
                urlAWSImg = AWSUtil.uploadImageToAWSS3(iconFile);
            }
            unit.setUnitIcon(urlAWSImg);
            eduUnitRepository.save(unit);
        }  catch (JsonParseException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        } catch (JsonMappingException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        } catch (IOException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        } catch (IllegalStateException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return unit;

    }

    public Collection<ChaptersUnitForm> findChapterUnit(Long subjectId) {
        String query = "";
        Map<String,Object> params = new HashMap<>();
        params.put("subjectId",subjectId);
        ChaptersUnitHandler handler = new ChaptersUnitHandler();
        jdbcTemplate.query(QueryTemplate.SELECT_CHAPTERS_UNIT,params,handler);
        return handler.getChaptersUnitLst();
    }

    public List<EduUnit> getUnitList(Long subjectId) {
        return eduUnitRepository.findBySubjectId(subjectId);
    }

    public List<String> getUnitNames(Long subjectId) {
        return eduUnitRepository.findUnitNames(subjectId);
    }

    public List<Map> getUnitSelectedList(Long subjectId) {
        EduUnitSelectFieldHandler unitSelectFieldHandler = new EduUnitSelectFieldHandler();
        jdbcTemplate.query(unitSelectFieldHandler.getQueryStmt(subjectId)
                ,unitSelectFieldHandler.getParamMapper(),unitSelectFieldHandler);
        return unitSelectFieldHandler.getRetData();
    }
}
