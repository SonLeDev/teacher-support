package edu.alaska.service;

import edu.alaska.common.Constances;
import edu.alaska.common.Utils;
import edu.alaska.domain.TestPaper;
import edu.alaska.domain.TestPublish;
import edu.alaska.jdbc.mapper.TestingHandler;
import edu.alaska.repository.TestPapperRepository;
import edu.alaska.repository.TestPublishRepository;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by sonle on 8/31/17.
 */
@Service
public class TestService {

    final static Logger LOGGER = Logger.getLogger(TestService.class);


    @Autowired
    TestPapperRepository testPapperRepository;

    @Autowired
    TestPublishRepository testPublishRepository;

    @Autowired
    NamedParameterJdbcTemplate jdbcTemplate;


    public Integer getMaxNoOfQuiz(Long testId) {

        return testPapperRepository.getMaxNoOfQuiz(testId);
    }

    public Collection<TestPaper> getListTestByAuthor(Long classId, Long subjectId, Long author) {
        // get list of quiz of testing
        if(author == 1){
            // show all for admin account
            return testPapperRepository.findByClassIdAndSubjectIdOrderByMdfDate(classId,subjectId);
        }
        return testPapperRepository.findByClassIdAndSubjectIdAndAuthorIdOrderByMdfDate(classId,subjectId,author);
    }

    public Collection<Map> getBasicListBySubjectId(Long subjectId) {
        return testPapperRepository.findBasicListBySubjectId(subjectId);
    }

    @Transactional
    public Object publish(Long testId) {
        TestPublish testPublish = testPublishRepository.findByTestId(testId);
        if(testPublish!=null){
            testPublish.setPublish(!testPublish.getPublish());
            testPublish.setPublished(!testPublish.getPublished());
            testPublish.setMdfDate(Utils.getDateTimeZonVN());
        }else{
            testPublish = new TestPublish();
            testPublish.setMdfDate(Utils.getDateTimeZonVN());
            testPublish.setPublish(Boolean.TRUE);
            testPublish.setPublished(Boolean.TRUE);
            testPublish.setTestId(testId);
            testPublish.setRegDate(Utils.getDateTimeZonVN());
        }

        testPublishRepository.save(testPublish);
        LOGGER.info(String.format("%s test publish id=%d to status=%B",
                Constances.LOG_TAG_INFO,testPublish.getId(),testPublish.getPublish()));
        return testPublish;
    }


    public Boolean delete(Long testId) {
        testPapperRepository.delete(testId);
        return Boolean.TRUE;
    }

    public TestPaper getTestPaper(Long testId) {
        return testPapperRepository.findOne(testId);
    }

    public Map getTestFull(Long testId) {
        Map nameParamMapper = new HashMap();
        TestingHandler testingHandler = new TestingHandler();
        jdbcTemplate.query(testingHandler.findTesting(testId,nameParamMapper),nameParamMapper,testingHandler);
        if(CollectionUtils.isNotEmpty(testingHandler.getDataList())){
            return testingHandler.getDataList().get(0);
        }
        return null;
    }
}
