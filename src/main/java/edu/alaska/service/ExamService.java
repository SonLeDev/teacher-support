package edu.alaska.service;

import edu.alaska.common.Constances;
import edu.alaska.common.Utils;
import edu.alaska.domain.ExaminationPublish;
import edu.alaska.jdbc.mapper.ExamHandler;
import edu.alaska.jdbc.mapper.ExaminationHandler;
import edu.alaska.repository.ExaminationPapperRepository;
import edu.alaska.repository.ExaminationPublishRepository;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by sonle on 8/10/17.
 */
@Service
public class ExamService {

    final static Logger LOGGER = Logger.getLogger(ExamService.class);

    @Autowired
    ExaminationPapperRepository examRepository;

    @Autowired
    ExaminationPublishRepository examinationPublishRepository;

    @Autowired
    NamedParameterJdbcTemplate jdbcTemplate;



    public Collection<edu.alaska.domain.ExaminationPaper> getExamListBySubjectId(Long subjectId) {
        LOGGER.info(String.format("%s getExamListBySubjectId : %d",Constances.LOG_TAG_INFO,subjectId));
        return examRepository.findBySubjectId(subjectId);
    }

    public Collection<Map> getBasicExamListBySubjectId(Long subjectId) {
        return examRepository.findBasicBySubjectId(subjectId);
    }

    public Integer getMaxNoOfQuiz(Long examId) {
        String queryStr = "SELECT MAX(orders) as maxNoOfQuiz FROM examination_quizs WHERE exam_id =:examId";
        Map param = new HashMap();
        param.put("examId",examId);
        Integer maxNoOfQuiz = jdbcTemplate.queryForObject(queryStr,param,Integer.class);
        return maxNoOfQuiz;
    }

    public Object getListExamByAuthor(Long classId, Long subjectId, Long author) {

        ExamHandler examHandler = new ExamHandler();

        jdbcTemplate.query(examHandler.getExamListQuery(classId,subjectId,author)
                            ,examHandler.getParam(),examHandler);

        return examHandler.getDataList();
    }

    @Transactional
    public Object publish(Long examId) {
        ExaminationPublish examinationPublish = examinationPublishRepository.findByExamId(examId);
        if(examinationPublish != null){
            // revert boolean
            examinationPublish.setPublish(!examinationPublish.getPublish());
            examinationPublish.setPublished(!examinationPublish.getPublished());
            examinationPublish.setMdfDate(Utils.getDateTimeZonVN());

        }else {
            examinationPublish = new ExaminationPublish();
            examinationPublish.setExamId(examId);
            examinationPublish.setRegDate(Utils.getDateTimeZonVN());
            examinationPublish.setPublish(Boolean.TRUE);
            examinationPublish.setPublished(Boolean.TRUE);
            examinationPublish.setMdfDate(Utils.getDateTimeZonVN());

        }
        examinationPublishRepository.save(examinationPublish);
        LOGGER.info(String.format("%s exam publish id=%d to status=%B",
                Constances.LOG_TAG_INFO,examinationPublish.getId(),examinationPublish.getPublish()));
        return  examinationPublish;
    }


    public Boolean delete(Long examId) {
        examRepository.delete(examId);
        return Boolean.TRUE;
    }

    public edu.alaska.domain.ExaminationPaper getExam(Long examId) {
        return examRepository.findOne(examId);
    }

    public Map getExamFull(Long examId) {
        edu.alaska.jdbc.tables.ExaminationPaper examTable = new edu.alaska.jdbc.tables.ExaminationPaper();
        ExaminationHandler examHandler = new ExaminationHandler();
        Map nameParam = new HashMap();
        jdbcTemplate.query(examTable.queryFullExamination(nameParam,examId),nameParam,examHandler);
        if(CollectionUtils.isNotEmpty(examHandler.getDataList())){
            return examHandler.getDataList().get(0);
        }
        return null;
    }
}
