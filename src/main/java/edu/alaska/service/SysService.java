package edu.alaska.service;

import edu.alaska.repository.SysRepository;
import org.joda.time.Instant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Date;

@Service
public class SysService {

    @Autowired
    SysRepository sysRepository;

    @Transactional
    public Long generateId(String domain){
        //TODO : upgrade for using synchronize block
        Instant in = new Instant();
        Long key = in.getMillis();
        sysRepository.storeSysId(key,domain,new Date());
        return key;
    }
}
