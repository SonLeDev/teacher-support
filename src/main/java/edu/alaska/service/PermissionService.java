package edu.alaska.service;

import edu.alaska.builder.RouterBuilder;
import edu.alaska.domain.User;
import edu.alaska.form.ResRouterForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by sonle on 11/24/17.
 */
@Service
public class PermissionService {

    @Autowired
    private UserService userService;

    public List<Map> getUserPermission(Long userId) {

        return null;
    }

    public List getRouters(Long userId) {
        User user = userService.getUser(userId);
        Long subjectId = user.getSubjectId();
        RouterBuilder routerBuilder = RouterBuilder.instance();
        // activeGroupMenu

        List<ResRouterForm> routerList = new ArrayList<>();
        routerList.add(routerBuilder.buildRouterSubjectGroup(subjectId));

        if(user.getUserName().contains("admin")){
            routerList.add(routerBuilder.buildRouterSubjectGroup(2l));
            routerList.add(routerBuilder.buildRouterSubjectGroup(3l));
            routerList.add(routerBuilder.buildRouterSubjectGroup(4l));

            routerList.add(routerBuilder.buildRouterAdminManage(user.getId()));
        }

        return routerList;
    }
}
