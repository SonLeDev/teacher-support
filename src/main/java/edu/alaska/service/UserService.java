package edu.alaska.service;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.alaska.builder.SessionBuilder;
import edu.alaska.domain.EduChapters;
import edu.alaska.domain.EduUnit;
import edu.alaska.domain.User;
import edu.alaska.repository.EduChaptersRepository;
import edu.alaska.repository.EduUnitRepository;
import edu.alaska.repository.UserRepository;

@Service
public class UserService {
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private EduChaptersRepository eduChaptersRepository;
	
	@Autowired
	private EduUnitRepository eduUnitRepository;
	
	public User validLogin(String userName, String password) {
		User user = userRepository.getUserByUserNameAndPassword(userName, password);
		return user;
	}
	
	public SessionBuilder buildSession(User user){
		SessionBuilder builder = new SessionBuilder();
		builder.setUserId(user.getId());
		builder.setUserName(user.getUserName());
		builder.setPermission(user.getPermission());
		builder.setSubjectId(user.getSubjectId());
		builder.setClassId(user.getClassId());
		return builder;
	}
	
	public void getChapterBySubjectId(SessionBuilder builder, Long subjectId){
		List<EduChapters> chapters = eduChaptersRepository.findChapterBySubjectId(subjectId);
		if (CollectionUtils.isNotEmpty(chapters)){
			builder.setChapter(chapters);
		}
	}
	
	public void getUnitBySubjectId(SessionBuilder builder, Long subjectId){
		List<EduUnit> units = eduUnitRepository.findBySubjectId(subjectId);
		if (CollectionUtils.isNotEmpty(units)){
			builder.setUnit(units);
		}
	}

	public User getUserByUserName(String userName){
		return userRepository.findByUserName(userName);
	}

    public Collection<Map> getBasicListBySubjectId(Long subjectId,Long classId) {
		return userRepository.findBasic(subjectId,classId);
    }

    public User getUser(Long userId) {
		return userRepository.findOne(userId);
    }
}
