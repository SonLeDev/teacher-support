package edu.alaska.service;

import edu.alaska.jdbc.mapper.EduQuizHanlder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * Created by sonle on 8/6/17.
 */
@Service
public class AdminManageService {

    @Autowired
    private NamedParameterJdbcTemplate jdbcTemplate;

    public List<Map<String,Object>> showAllQuiz() {
        EduQuizHanlder quizHandler = new EduQuizHanlder();
        jdbcTemplate.query(quizHandler.getQueryStmtAllFields(),quizHandler);
        return quizHandler.getRetQuizs();
    }
}
