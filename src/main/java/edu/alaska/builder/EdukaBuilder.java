package edu.alaska.builder;

import edu.alaska.domain.ExaminationQuizs;
import edu.alaska.domain.TestQuizs;

public class EdukaBuilder {

    private ExaminationQuizs examinationQuizs;
    private TestQuizs testQuizs;

    private EdukaBuilder(){};

    private static EdukaBuilder instance;

    public static EdukaBuilder instance(){
        //TODO : make sure it singleton here
        instance = new EdukaBuilder();
        return instance;
    }

    public ExaminationQuizs buildExamQuiz(Long examId,Long quizId,Integer order){
        examinationQuizs = new ExaminationQuizs();
        examinationQuizs.setExamId(examId);
        examinationQuizs.setQuizId(quizId);
        examinationQuizs.setOrders(order);
        /**
         * TODO: should be publish is false
         *
         */
        examinationQuizs.setPublish(Boolean.TRUE);
        examinationQuizs.setPublished(Boolean.FALSE);
        return examinationQuizs;
    }

    public TestQuizs buildTestQuiz(Long testId, Long quizId, int noOfQuiz) {
        testQuizs = new TestQuizs();
        testQuizs.setTestId(testId);
        testQuizs.setQuizId(quizId);
        testQuizs.setOrders(noOfQuiz);
        testQuizs.setPublish(Boolean.TRUE);
        testQuizs.setPublished(Boolean.FALSE);
        return testQuizs;
    }
}
