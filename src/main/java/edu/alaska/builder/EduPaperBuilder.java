package edu.alaska.builder;

import edu.alaska.common.Utils;
import edu.alaska.domain.ExaminationPaper;
import edu.alaska.domain.TestPaper;
import edu.alaska.form.RequestExamForm;
import edu.alaska.form.RequestTestForm;

public class EduPaperBuilder {

    private static EduPaperBuilder instance;
    private ExaminationPaper examinationPaper;
    private TestPaper testPaper;
    private EduPaperBuilder(){};

    public static EduPaperBuilder instance(){
        //TODO : make sure it singleton here
        instance = new EduPaperBuilder();
        return instance;
    }

    public TestPaper buildTestPaper(RequestTestForm requestTestForm, TestPaper testPaper){

        testPaper.setAuthorId(requestTestForm.getAuthorId());
        testPaper.setClassId(requestTestForm.getClassId());
        testPaper.setSubjectId(requestTestForm.getSubjectId());
        testPaper.setCode(requestTestForm.getCode());
        testPaper.setName(requestTestForm.getName());
        testPaper.setTimeExpire(requestTestForm.getTimeExpire());
        testPaper.setDescription(requestTestForm.getDescription());
        testPaper.setInstruction(requestTestForm.getInstruction());
        testPaper.setMdfDate(Utils.getDateTimeZonVN());
        return testPaper;
    }

    public ExaminationPaper buildExamPaper(RequestExamForm requestExamForm, ExaminationPaper examinationPaper){

        examinationPaper.setExamName(requestExamForm.getExamName());
        examinationPaper.setExamCode(requestExamForm.getExamCode());
        examinationPaper.setNumOfQuiz(requestExamForm.getNumOfQuiz());

        assert requestExamForm.getSubjectId()!=null;
        examinationPaper.setSubjectId(requestExamForm.getSubjectId());
        examinationPaper.setClassId(requestExamForm.getClassId());

        examinationPaper.setAuthor(requestExamForm.getAuthor());

        examinationPaper.setDescription(requestExamForm.getDescription());
        examinationPaper.setInstruction(requestExamForm.getInstruction());

        examinationPaper.setTimeExpire(requestExamForm.getTimeExpire());
        examinationPaper.setYearExam(requestExamForm.getYearExam());
        return examinationPaper;
    }


}
