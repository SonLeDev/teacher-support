package edu.alaska.builder;

import edu.alaska.domain.PaperTest;
import edu.alaska.form.PaperTestForm;

import java.util.Date;

/**
 * Created by sonle on 7/27/17.
 */
public class PaperTestBuilder {
    private PaperTest paperTest;

    private static PaperTestBuilder instance = new PaperTestBuilder();

    public static PaperTestBuilder instance(){
        return instance;
    }

    public PaperTest build(PaperTestForm form){
        paperTest = new PaperTest();
        paperTest.setId(form.getId());
        paperTest.setName(form.getName());
        paperTest.setCode(form.getCode());
        paperTest.setTimeExpire(form.getTimeExpire());
        paperTest.setAuthorId(form.getAuthorId());
        paperTest.setSubjectId(form.getSubjectId());
        paperTest.setClassId(form.getClassId());
        paperTest.setCreatedDate(new Date());
        paperTest.setTestTypeId(1l);
//        paperTest.setTestTypeId(form.getTestType());
        return paperTest;
    }

}
