package edu.alaska.builder;

import edu.alaska.form.ResRouterForm;
import edu.alaska.form.StateForm;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

/**
 * Created by sonle on 11/27/17.
 */
public class RouterBuilder {

    /**
     HOME_CONTENT_... should consistence with state which defined in edukaApp.js
     */
    private static final String HOME_CONTENT_PRACTQUIZLISTSTATE = "home.content.practQuizListState";
    private static final String HOME_CONTENT_EXAMLISTSTATE = "home.content.examListState";
    private static final String HOME_CONTENT_CHAPTERLISTSTATE = "home.content.chapterListState";
    private static final String HOME_CONTENT_TESTLISTSTATE = "home.content.testListState";

    private static final String HOME_CONTENT_VOLUNTEERSTATE = "home.content.volunteerState";
    private static final String HOME_CONTENT_REVIEWQUIZSTATE = "home.content.reviewQuizState";
    private static final String HOME_CONTENT_MANAGEPUBLISHINGQUIZ = "home.content.managePublishingState";
    private static final String HOME_CONTENT_QUIZ_REPORT_ERROR_STATE = "home.content.quizReportErrorState";


    private static final Map<Long,String> mapSubject;
    static{
        mapSubject = new HashMap<>();
        mapSubject.put(1l,"Toán Học");
        mapSubject.put(2l,"Vật Lý");
        mapSubject.put(3l,"Hoá Học");
        mapSubject.put(4l,"Tiếng Anh");
    }

    public RouterBuilder(){}

    public ResRouterForm buildRouterSubjectGroup(Long subjectId){
        // TODO : extend for privilege of user on this permission

        String param = String.format("({subjectId:%d})",subjectId);
        StateForm statePract = new StateForm();
        statePract.setTitle("Câu Hỏi Ôn Tập");
        statePract.setStateName((new StringBuilder(HOME_CONTENT_PRACTQUIZLISTSTATE)).append(param).toString());

        StateForm stateExam = new StateForm();
        stateExam.setTitle("Đề Thi Thử");
        stateExam.setStateName((new StringBuilder(HOME_CONTENT_EXAMLISTSTATE)).append(param).toString());

        StateForm stateTest = new StateForm();
        stateTest.setTitle("Bài Kiểm Tra");
        stateTest.setStateName((new StringBuilder(HOME_CONTENT_TESTLISTSTATE)).append(param).toString());

        StateForm stateChapterUnit = new StateForm();
        stateChapterUnit.setTitle("Chương - Chuyên Đề");
        stateChapterUnit.setStateName((new StringBuilder(HOME_CONTENT_CHAPTERLISTSTATE)).append(param).toString());

        StateForm stateReportQuiz = new StateForm();
        stateReportQuiz.setTitle("Câu Hỏi Lỗi");
        stateReportQuiz.setStateName((new StringBuilder(HOME_CONTENT_QUIZ_REPORT_ERROR_STATE)).append(param).toString());



        ResRouterForm routerForm = new ResRouterForm();
        routerForm.setGroupName(mapSubject.get(subjectId));
        routerForm.setStates(new LinkedList<>());
        routerForm.getStates().add(statePract);
        routerForm.getStates().add(stateExam);
        routerForm.getStates().add(stateTest);
        routerForm.getStates().add(stateReportQuiz);
        routerForm.getStates().add(stateChapterUnit);


        return routerForm;
    }

    public static RouterBuilder instance() {
        return new RouterBuilder();
    }

    public ResRouterForm buildRouterAdminManage(Long userId) {

        // TODO : extends for userId own somethings

        ResRouterForm routerForm = new ResRouterForm();
        routerForm.setGroupName("Quản Lý");
        routerForm.setActiveGroup("active");
        routerForm.setActiveChild("active-child");
        routerForm.setStates(new ArrayList<>());

        for(Long subjectId : mapSubject.keySet()){
            String param = String.format("({subjectId:%d})",subjectId);
            StateForm stateApproval = new StateForm();
            stateApproval.setTitle("Duyệt câu hỏi - " + mapSubject.get(subjectId));
            stateApproval.setStateName(
                    (new StringBuilder(HOME_CONTENT_REVIEWQUIZSTATE)).append(param).toString());
            routerForm.getStates().add(stateApproval);
        }

        StateForm stateVolunteers = new StateForm();
        stateVolunteers.setTitle("Giáo viên - Cộng tác viên");
        stateVolunteers.setStateName(
                (new StringBuilder(HOME_CONTENT_VOLUNTEERSTATE)).append("()").toString());

        routerForm.getStates().add(stateVolunteers);

        return routerForm;
    }
}
