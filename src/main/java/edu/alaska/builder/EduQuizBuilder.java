package edu.alaska.builder;

import edu.alaska.common.Utils;
import edu.alaska.domain.EduQuiz;
import edu.alaska.domain.EduUnit;
import edu.alaska.form.QuizForm;
import org.apache.commons.lang.StringUtils;

/**
 * Created by SonLee on 5/26/2017.
 */
public class EduQuizBuilder {
    private static EduQuiz eduQuiz;
    private static EduQuizBuilder builder = new EduQuizBuilder();

    private EduQuizBuilder(){

    }

    public static EduQuizBuilder instance(QuizForm quizForm, EduUnit eduUnit){
        eduQuiz = new EduQuiz();

        //valid question, answerA,answerB,answerC,answerD,correctAnswer,unitId

        eduQuiz.setAnswerA(quizForm.getAnswerA());
        eduQuiz.setAnswerB(quizForm.getAnswerB());
        eduQuiz.setAnswerC(quizForm.getAnswerC());
        eduQuiz.setAnswerD(quizForm.getAnswerD());
        eduQuiz.setCorrectAnswer(quizForm.getCorrectAnswer());

        if(StringUtils.isEmpty(quizForm.getCorrectAnswer())){
            StringBuilder correctAnswer = new StringBuilder();

            if(quizForm.getChkAnswerA()!=null && quizForm.getChkAnswerA()) correctAnswer.append("A");
            if(quizForm.getChkAnswerB()!=null && quizForm.getChkAnswerB()) correctAnswer.append("B");
            if(quizForm.getChkAnswerC()!=null && quizForm.getChkAnswerC()) correctAnswer.append("C");
            if(quizForm.getChkAnswerD()!=null && quizForm.getChkAnswerD()) correctAnswer.append("D");
            if(quizForm.getChkAnswer()!=null){
                correctAnswer.append(quizForm.getChkAnswer());
            }
            eduQuiz.setCorrectAnswer(correctAnswer.toString());
        }
        eduQuiz.setDetailAnswer("");
        if(StringUtils.isNotEmpty(quizForm.getDetailAnswer())){
            eduQuiz.setDetailAnswer(quizForm.getDetailAnswer());
        }

        eduQuiz.setSolution("");
        if(StringUtils.isNotEmpty(quizForm.getSolution())){
            eduQuiz.setSolution(quizForm.getSolution());
        }

      //  eduQuiz.setExplainAnswer("still not implement !!!");
        eduQuiz.setQuestion(quizForm.getQuestion());
//        eduQuiz.setChapterName(quizForm.getChapterName());
        eduQuiz.setLevel(quizForm.getLevel());
        if(quizForm.getNoOfQuest()!=null){
            eduQuiz.setNoOfQuest(quizForm.getNoOfQuest());
        }
        eduQuiz.setSubjectId(quizForm.getSubjectId());
        eduQuiz.setEduUnit(eduUnit);
        eduQuiz.setUserId(quizForm.getUserId());
        eduQuiz.setUpdateDate(Utils.getDateTimeZonVN());
        eduQuiz.setPublished(Boolean.FALSE);
        eduQuiz.setIsPract(quizForm.getIsPract());

        return builder;
    }

    public EduQuiz build(){
        if(eduQuiz==null) return new EduQuiz();
        return eduQuiz;
    }

    public static void onChange(EduQuiz eduQuiz, QuizForm quizForm,EduUnit eduUnit) {

        eduQuiz.setQuestion(quizForm.getQuestion());
        eduQuiz.setAnswerA(quizForm.getAnswerA());
        eduQuiz.setAnswerB(quizForm.getAnswerB());
        eduQuiz.setAnswerC(quizForm.getAnswerC());
        eduQuiz.setAnswerD(quizForm.getAnswerD());
        eduQuiz.setCorrectAnswer(quizForm.getCorrectAnswer());
        if(!eduQuiz.getEduUnit().equals(eduUnit)){
            eduQuiz.setEduUnit(eduUnit);
        }

        eduQuiz.setDetailAnswer("");
        if(StringUtils.isNotEmpty(quizForm.getDetailAnswer())){
            eduQuiz.setDetailAnswer(quizForm.getDetailAnswer());
        }

        eduQuiz.setSolution("");
        if(StringUtils.isNotEmpty(quizForm.getSolution())){
            eduQuiz.setSolution(quizForm.getSolution());
        }

        eduQuiz.setLevel(quizForm.getLevel());
        if(quizForm.getNoOfQuest()!=null){
            eduQuiz.setNoOfQuest(quizForm.getNoOfQuest());
        }

    }
}
