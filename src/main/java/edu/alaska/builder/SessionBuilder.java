package edu.alaska.builder;

import java.util.List;

import edu.alaska.domain.EduChapters;
import edu.alaska.domain.EduUnit;

public class SessionBuilder {
	private Long userId;
	private String userName;
	private Long subjectId;
	private String permission;
	private String privilegeChapter;
	private String privilegeUnit;
	private Long classId;
	private List<EduChapters> chapter;
	private List<EduUnit> unit;

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Long getSubjectId() {
		return subjectId;
	}

	public void setSubjectId(Long subjectId) {
		this.subjectId = subjectId;
	}

	public String getPermission() {
		return permission;
	}

	public void setPermission(String permission) {
		this.permission = permission;
	}

	public String getPrivilegeChapter() {
		return privilegeChapter;
	}

	public void setPrivilegeChapter(String privilegeChapter) {
		this.privilegeChapter = privilegeChapter;
	}

	public String getPrivilegeUnit() {
		return privilegeUnit;
	}

	public void setPrivilegeUnit(String privilegeUnit) {
		this.privilegeUnit = privilegeUnit;
	}

	public List<EduChapters> getChapter() {
		return chapter;
	}

	public void setChapter(List<EduChapters> chapter) {
		this.chapter = chapter;
	}

	public List<EduUnit> getUnit() {
		return unit;
	}

	public void setUnit(List<EduUnit> unit) {
		this.unit = unit;
	}

	public Long getClassId() {
		return classId;
	}

	public void setClassId(Long classId) {
		this.classId = classId;
	}
}
