package edu.alaska.common;

/**
 * Created by SonLee on 5/25/2017.
 */

import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.PutObjectResult;
import org.apache.log4j.Logger;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;

public class AWSUtil {
    final static Logger LOGGER = Logger.getLogger(AWSUtil.class);

    private static final String ACCESS_KEY = "AKIAJAL6FF3DBKPAH4WQ";
    private static final String SECRET_KEY = "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";
    private static final String BUCKET_NAME = "edukaio";
    private static final String IMAGE_LOCATION = "quizImg";

    public static String uploadImageToAWSS3(MultipartFile multipartFile) throws IllegalStateException,
            IOException {
        LOGGER.info("----- uploadImageToAWSS3()");

        if(multipartFile==null){
            LOGGER.warn("===== uploadImageToAWSS3 empty files to upload !!!");
            return "";
        }

        String fileName = null;
        try {

            AWSCredentials credentials = new BasicAWSCredentials(ACCESS_KEY, SECRET_KEY);
            AmazonS3 s3 = new AmazonS3Client(credentials);
            Region usWest2 = Region.getRegion(Regions.AP_SOUTHEAST_1);
            s3.setRegion(usWest2);
            InputStream stream = multipartFile.getInputStream();
            fileName = System.currentTimeMillis() + "_" + multipartFile.getOriginalFilename();
            ObjectMetadata objectMetadata = new ObjectMetadata();

            PutObjectRequest putObjectRequest = new PutObjectRequest(BUCKET_NAME, IMAGE_LOCATION + "/" + fileName, stream, objectMetadata);
            putObjectRequest.setCannedAcl(CannedAccessControlList.PublicRead);
            PutObjectResult putObjectResult = s3.putObject(putObjectRequest);
            if(putObjectResult !=null){
                LOGGER.info("----- s3.putObject() successful");
//                return Constances.AWS_IMG_URL + ("/" + IMAGE_LOCATION + "/" + fileName);
                return ("/" + IMAGE_LOCATION + "/" + fileName);
            }

           /* java.security.Security.setProperty("networkaddress.cache.ttl", S3_CACHE);
            s3 = new AmazonS3Client(credentials);	s3.setEndpoint(END_POINT_URL);
            InputStream stream = multipartFile.getInputStream();
            fileName = System.currentTimeMillis() + "_" + fileName;
            ObjectMetadata objectMetadata = new ObjectMetadata();
            PutObjectRequest putObjectRequest = new PutObjectRequest(BUCKET_NAME, IMAGE_LOCATION + "/" + fileName, stream,objectMetadata);
            //skip if do not want to access the image directly from S3
            putObjectRequest.setCannedAcl(CannedAccessControlList.PublicRead);
            //skip if do not want to access the image directly from S3
            s3.putObject(putObjectRequest);*/
        } catch (AmazonServiceException e) {
            LOGGER.error(">>>>> uploadImageToAWSS3 unsuccessful : " + e.getErrorMessage());
        }
        return "";
    }
}