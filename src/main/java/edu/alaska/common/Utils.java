package edu.alaska.common;

import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created by sstvn on 5/9/17.
 */

public class Utils {

    public static HttpHeaders creatingHeader(String accessToken){
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("accessToken",accessToken);

        return headers;
    }

    public static HttpHeaders creatingHeader(String accessToken, String userId) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("accessToken",accessToken);
        headers.set("userId",userId);

        return headers;
    }

    public static Date getDateTimeZonVN(){
        Calendar ca = Calendar.getInstance();
        TimeZone tzInVN = TimeZone.getTimeZone("Asia/Saigon");
        TimeZone tzInAmerica = TimeZone.getTimeZone("America/New_York");
        ca.setTimeZone(tzInAmerica);
        return ca.getTime();
    }
}
