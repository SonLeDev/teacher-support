package edu.alaska.common;

/**
 * Created by SonLee on 5/30/2017.
 */
public class Constances {
    public static final String LOG_TAG_DEBUG = "eduka::debug::";
    public static final String LOG_TAG_INFO = "eduka::info::";
    public static final String LOG_TAG_ERROR = "eduka::error::";
    public static final String AWS_IMG_URL = "https://s3-ap-southeast-1.amazonaws.com/edukaio";

}

