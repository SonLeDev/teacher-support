package edu.alaska.common;

import java.sql.ResultSet;
import java.util.Map;

/**
 * Created by sonle on 8/26/17.
 */
public interface CallbackJdbcResultSet {
    public void putMoreTo(Map<String, Object> rsRow, ResultSet rs);
}
