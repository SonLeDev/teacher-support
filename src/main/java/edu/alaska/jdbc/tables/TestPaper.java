package edu.alaska.jdbc.tables;

import org.apache.commons.lang.StringUtils;

import java.util.Map;

/**
 * Created by sonle on 11/21/17.
 */
public class TestPaper {
    public static final String NAME_TABLE = "test_paper";
    public static final String ALIAS_TABLE = "tpp";

    public static String asTable(){
        return String.join(" as ",NAME_TABLE,ALIAS_TABLE);
    }

    public static enum ColumnAlias{

        ID("id","testId"),
        SUBJECT_ID("subject_id","subjectId"),
        CLASS_ID("class_id","classId"),
        NAME("name","testName"),
        CODE("code","testCode"),
        INSTRUCTION("instruction","instruction"),
        DESCRIPTION("description","description"),
        AUTHOR("author_id","author"),
        DURATION("time_expire","duration"),
        MDF_DATE("mdf_date","mdfDate"),
        TEST_TYPE_ID("test_type_id","testTypeId");

        private String nameColumn;
        private String aliasField;
        ColumnAlias(String nameColumn, String aliasField) {
            this.aliasField = aliasField;
            this.nameColumn = nameColumn;
        }

        public String getAliasField() {
            return aliasField;
        }

        public String getNameColumn() {
            return nameColumn;
        }

        public String asField() {
            return String.join(" as ",dotColumn(), aliasField);
        }

        public String dotColumn(){
            return String.join(".",ALIAS_TABLE,nameColumn);
        }
    }

    public String queryTestByTestId(Long testId, Map nameParamMapper){

        String nameParam = "testId";
        nameParamMapper.put(nameParam,testId);

        String[] selectedFields = {ColumnAlias.ID.asField(),ColumnAlias.NAME.asField()
                                    ,ColumnAlias.CODE.asField(),ColumnAlias.AUTHOR.asField()
                                    ,ColumnAlias.CLASS_ID.asField(),ColumnAlias.SUBJECT_ID.asField()
                                    ,ColumnAlias.DESCRIPTION.asField(),ColumnAlias.INSTRUCTION.asField()
                                    ,ColumnAlias.DURATION.asField(),ColumnAlias.MDF_DATE.asField()
                                    ,TestPublish.ColumnAlias.PUBLISHED.asField()
                                    ,EduTestType.ColumnAlias.NAME.asField()
                                    ,EduTestGroup.ColumnAlias.NAME.asField()
                                    ,EduTestGroup.ColumnAlias.TEST_GROUP_PUBLISHED.asField()};

        StringBuilder qry = new StringBuilder("SELECT ");
        qry.append(StringUtils.join(selectedFields,","));
        qry.append(" ,SUM(IF(").append(TestQuizs.ColumnAlias.PUBLISHED.dotColumn())
                .append(",1,0)) numQuizPublised");
        qry.append(" ,SUM(IF(").append(TestQuizs.ColumnAlias.PUBLISHED.dotColumn())
                .append(",0,1)) numQuizUnPublised");

        qry.append(" FROM ");
        qry.append(TestPaper.asTable());
        qry.append(" LEFT JOIN ").append(TestPublish.asTable())
                .append(" ON ").append(TestPublish.ColumnAlias.TEST_ID.dotColumn())
                                .append(" = ").append(ColumnAlias.ID.dotColumn());
        qry.append(" LEFT JOIN ").append(EduTestType.asTable())
                .append(" ON ").append(EduTestType.ColumnAlias.ID.dotColumn())
                                .append(" = ").append(ColumnAlias.TEST_TYPE_ID.dotColumn());
        qry.append(" LEFT JOIN ").append(EduTestGroup.asTable())
                .append(" ON ").append(EduTestGroup.ColumnAlias.ID.dotColumn())
                                .append(" = ").append(EduTestType.ColumnAlias.GROUP_ID.dotColumn());
        qry.append(" LEFT JOIN ").append(TestQuizs.asTable())
                .append(" ON ").append(TestQuizs.ColumnAlias.TEST_ID.dotColumn())
                                .append(" = ").append(ColumnAlias.ID.dotColumn());

        qry.append(" WHERE 1=1 ");
        qry.append(" AND ").append(TestQuizs.ColumnAlias.DELETED).append(" = 0");
        qry.append(" AND ").append(ColumnAlias.ID.dotColumn())
                            .append("=:").append(nameParam);
        qry.append(" GROUP BY ").append(ColumnAlias.ID.dotColumn());

        return qry.toString();

    }

}
