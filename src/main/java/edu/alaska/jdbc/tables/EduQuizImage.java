package edu.alaska.jdbc.tables;

/**
 * Created by sonle on 11/21/17.
 */
public class EduQuizImage {

    public static String NAME_TABLE = "edu_quiz_image";
    public static String ALIAS_TABLE = "eqzimg";
    public static String asTable(){
        return String.join(" as ",NAME_TABLE,ALIAS_TABLE);
    }

    public static enum ColumnAlias {

        ID("id", "quizImgId"),
        QUIZ_ID("quiz_id", "quizId"),
        QUEST_IMG("quest_img", "questImg"),
        DETAIL_ANS_IMG("dtail_ans_img", "detailAnsImg"),
        MDF_DATE("mdf_date", "mdfDate")
        ;
        private String nameColumn;
        private String aliasField;

        ColumnAlias(String nameColumn, String aliasField) {
            this.aliasField = aliasField;
            this.nameColumn = nameColumn;
        }

        public String getAliasField() {
            return aliasField;
        }

        public String getNameColumn() {
            return nameColumn;
        }

        public String asField() {
            return String.join(" as ", dotColumn(), aliasField);
        }
        public String dotColumn(){
            return String.join(".",ALIAS_TABLE,nameColumn);
        }
    }
}
