package edu.alaska.jdbc.tables;

import org.apache.commons.lang.StringUtils;

import java.util.Map;

/**
 * Created by sonle on 11/20/17.
 */
public class  ExaminationPaper{

    public static final String NAME_TABLE = "examination_paper";
    public static final String ALIAS_TABLE = "expp";

    public static String asTable(){
        return String.join(" as ",NAME_TABLE,ALIAS_TABLE);
    }
    public static enum ColumnAlias{
        ID("id","examId"),
        AUTHOR("author","author"),
        EXAM_CODE("exam_code","examCode"),
        EXAM_NAME("exam_name","examName"),
        NUM_OF_QUEST("num_of_quiz","numOfQuiz"), // number of all quiz inside a examination
        DURATION("time_expire","duration"),
        SUBJECT_ID("subject_id","subjectId"),
        CLASS_ID("class_id","classId"),
        INSTRUCTION("instruction","instruction"),
        DESCRIPTION("description","description"),
        EXAM_LEVEL("level","level"),
        YEAR_EXAM("year_exam","yearExam"),
        MDF_DATE("mdf_date","mdfDate");

        private String nameColumn;
        private String aliasField;
        ColumnAlias(String nameColumn, String aliasField) {
            this.aliasField = aliasField;
            this.nameColumn = nameColumn;
        }

        public String getAliasField() {
            return aliasField;
        }

        public String getNameColumn() {
            return nameColumn;
        }

        public String asField() {
            return String.join(" as ",dotColumn(), aliasField);
        }

        public String dotColumn(){
            return String.join(".",ALIAS_TABLE,nameColumn);
        }
    }

    public String queryFullExamination(Map nameParamMap,Long examId ){

        String paramExamId = "examId";
        nameParamMap.put(paramExamId,examId);

        StringBuilder qry = new StringBuilder();

        String[] selectedFields = {ColumnAlias.ID.asField(),ColumnAlias.AUTHOR.asField()
                                    ,ColumnAlias.EXAM_CODE.asField(),ColumnAlias.EXAM_NAME.asField()
                                    ,ColumnAlias.NUM_OF_QUEST.asField(),ColumnAlias.DURATION.asField()
                                    ,ColumnAlias.SUBJECT_ID.asField(),ColumnAlias.CLASS_ID.asField()
                                    ,ExaminationPublish.ColumnAlias.PUBLISHED.asField()
                                    ,EduTags.ColumnAlias.TAG_NAME.asField(),EduTags.ColumnAlias.TAG_PUBLISHED.asField()
                                    };
        qry.append("SELECT ");
        qry.append(StringUtils.join(selectedFields,","));
        qry.append(" ,SUM(IF(").append(ExaminationQuizs.ColumnAlias.PUBLISHED.dotColumn())
                        .append(",1,0)) numQuizPublised");
        qry.append(" ,SUM(IF(").append(ExaminationQuizs.ColumnAlias.PUBLISHED.dotColumn())
                .append(",0,1)) numQuizUnPublised");
        qry.append(" FROM ");
        qry.append(ExaminationPaper.asTable());
        qry.append(" LEFT JOIN ").append(ExaminationPublish.asTable())
            .append(" ON ").append(ColumnAlias.ID.dotColumn())
                .append(" = ").append(ExaminationPublish.ColumnAlias.EXAM_ID.dotColumn());
        qry.append(" LEFT JOIN ").append(ExaminationQuizs.asTable())
                .append(" ON ").append(ColumnAlias.ID.dotColumn())
                .append(" = ").append(ExaminationQuizs.ColumnAlias.EXAM_ID.dotColumn());
        qry.append(" LEFT JOIN ").append(ExaminationTags.asTable())
                .append(" ON ").append(ColumnAlias.ID.dotColumn())
                .append(" = ").append(ExaminationTags.ColumnAlias.EXAM_ID.dotColumn());
        qry.append(" LEFT JOIN ").append(EduTags.asTable())
                .append(" ON ").append(EduTags.ColumnAlias.ID.dotColumn())
                .append(" = ").append(ExaminationTags.ColumnAlias.TAG_ID.dotColumn());

        qry.append(" WHERE 1=1 ");
        qry.append(" AND ").append(ExaminationQuizs.ColumnAlias.DELETED).append(" = 0");
        qry.append(" AND ").append(ColumnAlias.ID.dotColumn())
                .append("=:").append(paramExamId);
        qry.append(" GROUP BY ").append(ColumnAlias.ID.dotColumn());
        return qry.toString();
    }

}

