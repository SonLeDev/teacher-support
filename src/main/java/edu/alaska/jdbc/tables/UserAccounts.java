package edu.alaska.jdbc.tables;

/**
 * Created by sonle on 11/25/17.
 */
public class UserAccounts {
    public static String NAME_TABLE = "user";
    public static String ALIAS_TABLE = "ua";
    public static String asTable(){
        return String.join(" as ",NAME_TABLE,ALIAS_TABLE);
    }

    public static enum ColumnAlias {
        ID("id", "userId"),
        USER_NAME("user_name", "userName"),
        PASSWORD("password","password"),
        SUBJECT_ID("subject_id","subjectId"),
        CLASS_ID("class_id","classId"),
        ;
        private String nameColumn;
        private String aliasField;

        ColumnAlias(String nameColumn, String aliasField) {
            this.aliasField = aliasField;
            this.nameColumn = nameColumn;
        }

        public String getAliasField() {
            return aliasField;
        }

        public String getNameColumn() {
            return nameColumn;
        }

        public String asField() {
            return String.join(" as ", dotColumn(), aliasField);
        }
        public String dotColumn(){
            return String.join(".",ALIAS_TABLE,nameColumn);
        }
    }
}
