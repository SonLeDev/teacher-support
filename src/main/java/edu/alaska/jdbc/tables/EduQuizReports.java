package edu.alaska.jdbc.tables;

/**
 * Created by sonle on 11/21/17.
 */
public class EduQuizReports {

    public static String NAME_TABLE = "edu_quiz_reports";
    public static String ALIAS_TABLE = "eqzrpt";
    public static String asTable(){
        return String.join(" as ",NAME_TABLE,ALIAS_TABLE);
    }

    public static enum ColumnAlias {

        ID("id", "quizImgId"),
        QUIZ_ID("quiz_id", "quizId"),
        TYPE_REPORT("type_report", "typeReport"),
        DATE_REPORT("date_report", "dateReport"),
        USER_ID_REPORT("user_id_report", "userIdReport"),
        STATUS_REPORT("status_report", "statusReport")
        ;
        private String nameColumn;
        private String aliasField;

        ColumnAlias(String nameColumn, String aliasField) {
            this.aliasField = aliasField;
            this.nameColumn = nameColumn;
        }

        public String getAliasField() {
            return aliasField;
        }

        public String getNameColumn() {
            return nameColumn;
        }

        public String asField() {
            return String.join(" as ", dotColumn(), aliasField);
        }
        public String dotColumn(){
            return String.join(".",ALIAS_TABLE,nameColumn);
        }
    }
}
