package edu.alaska.jdbc.tables;

/**
 * Created by sonle on 11/20/17.
 */
public class ExaminationPublish{

    public static final String NAME_TABLE = "examination_publish";
    public static final String ALIAS_TABLE = "expbl";
    public static String asTable(){
        return String.join(" as ",NAME_TABLE,ALIAS_TABLE);
    }
    public static enum ColumnAlias {
        ID("id", "examPublishId"),
        EXAM_ID("exam_id", "examId"),
        PUBLISHED("published", "published");

        private String nameColumn;
        private String aliasField;

        ColumnAlias(String nameColumn, String aliasField) {
            this.aliasField = aliasField;
            this.nameColumn = nameColumn;
        }

        public String getAliasField() {
            return aliasField;
        }

        public String getNameColumn() {
            return nameColumn;
        }

        public String asField() {
            return String.join(" as ", dotColumn(), aliasField);
        }
        public String dotColumn(){
            return String.join(".",ALIAS_TABLE,nameColumn);
        }
    }
}
