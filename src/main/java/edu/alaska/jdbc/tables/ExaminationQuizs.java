package edu.alaska.jdbc.tables;

/**
 * Created by sonle on 11/20/17.
 */
class ExaminationQuizs{

    public static final String NAME_TABLE = "examination_quizs";
    public static final String ALIAS_TABLE = "exqz";
    public static String asTable(){
        return String.join(" as ",NAME_TABLE,ALIAS_TABLE);
    }

    public static enum ColumnAlias {
        EXAM_ID("exam_id", "examId"),
        QUIZ_ID("quiz_id", "quizId"),
        ORDER("order", "noOfQuest"), // noOfQuest: the order of Question in an examination
        PUBLISHED("published", "quizPublished"),
        DELETED("deleted", "deleted"),
        ;

        private String nameColumn;
        private String aliasField;

        ColumnAlias(String nameColumn, String aliasField) {
            this.aliasField = aliasField;
            this.nameColumn = nameColumn;
        }

        public String getAliasField() {
            return aliasField;
        }

        public String getNameColumn() {
            return nameColumn;
        }

        public String asField() {
            return String.join(" as ", dotColumn(), aliasField);
        }
        public String dotColumn(){
            return String.join(".",ALIAS_TABLE,nameColumn);
        }
    }
}
