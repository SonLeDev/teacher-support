package edu.alaska.jdbc.tables;

/**
 * Created by sonle on 11/20/17.
 */
public class EduTags{
    public static String NAME_TABLE = "edu_tags";
    public static String ALIAS_TABLE = "etg";
    public static String asTable(){
        return String.join(" as ",NAME_TABLE,ALIAS_TABLE);
    }

    public static enum ColumnAlias {
        ID("id", "tagId"),
        TAG_NAME("name", "tagName"),
        TAG_PUBLISHED("published","tagPublished")
        ;
        private String nameColumn;
        private String aliasField;

        ColumnAlias(String nameColumn, String aliasField) {
            this.aliasField = aliasField;
            this.nameColumn = nameColumn;
        }

        public String getAliasField() {
            return aliasField;
        }

        public String getNameColumn() {
            return nameColumn;
        }

        public String asField() {
            return String.join(" as ", dotColumn(), aliasField);
        }
        public String dotColumn(){
            return String.join(".",ALIAS_TABLE,nameColumn);
        }
    }
}
