package edu.alaska.jdbc.tables;

import java.util.Map;

/**
 * Created by sonle on 11/22/17.
 */
public class EduUnit {
    public static String NAME_TABLE = "edu_unit";
    public static String ALIAS_TABLE = "eun";
    public static String asTable(){
        return String.join(" as ",NAME_TABLE,ALIAS_TABLE);
    }

    private Long unitNo;
    private String unitIcon;
    private String description;

    public static enum ColumnAlias {
        ID("id", "unitId"),
        CHAPTER_ID("chapter_id", "chapterId"),
        SUBJECT_ID("subjectId", "subjectId"),
        UNIT_CODE("unit_code", "unitCode"),
        UNIT_NAME("unit_name", "unitName"),
        UNIT_NO("unit_no", "unitNo"),
        UNIT_ICON("unit_icon", "unitIcon"),
        DESCRIPTION("description", "description")
        ;
        private String nameColumn;
        private String aliasField;

        ColumnAlias(String nameColumn, String aliasField) {
            this.aliasField = aliasField;
            this.nameColumn = nameColumn;
        }

        public String getAliasField() {
            return aliasField;
        }

        public String getNameColumn() {
            return nameColumn;
        }

        public String asField() {
            return String.join(" as ", dotColumn(), aliasField);
        }
        public String dotColumn(){
            return String.join(".",ALIAS_TABLE,nameColumn);
        }
    }

    public static String queryIdAndName(Long subjectId, Map paramMapper){
        String nameParam1 = "subjectId";
        if(paramMapper!=null) paramMapper.put(nameParam1,subjectId);
        StringBuilder qry = new StringBuilder("SELECT ");

        qry.append(String.join(",",EduUnit.ColumnAlias.ID.asField()
                    ,EduUnit.ColumnAlias.UNIT_NAME.asField()));
        qry.append(" FROM ").append(EduUnit.asTable());
        qry.append(" WHERE ").append(EduUnit.ColumnAlias.SUBJECT_ID)
                    .append(" =:").append(nameParam1);
        return qry.toString();
    }



}
