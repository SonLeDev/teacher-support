package edu.alaska.jdbc.tables;

/**
 * Created by sonle on 11/21/17.
 */
public class EduQuiz {

    public static String NAME_TABLE = "edu_quiz";
    public static String ALIAS_TABLE = "eqz";
    public static String asTable(){
        return String.join(" as ",NAME_TABLE,ALIAS_TABLE);
    }

    public static enum ColumnAlias {

        ID("id", "quizId"),
        LEVEL("level", "quizLevel"),
        QUESTION("question","question"),
        ANSWER_A("answera","answerA"),
        ANSWER_B("answerb","answerB"),
        ANSWER_C("answerc","answerC"),
        ANSWER_D("answerd","answerD"),
        CORRECT_ANSWER("correct_answer","correctAnswer"),
        DETAIL_ANSWER("detail_answer","detailAnswer"),
        SOLUTION("solution","solution"),
        UNIT_ID("unit_id","unitId"),
        USER_ID("user_id","userId"),
        REG_DATE("reg_date","regDate"),
        MDF_DATE("mdf_date","mdfDate"),
        UPDATE_DATE("update_date","updateDate"),
        SUBJECT_ID("subject_id","subjectId"),
        PUBLISHED("published","quizPublished"),
        DELETED("deleted","quizDeleted"),
        IS_PRACT("is_pract","isPract");

        private String nameColumn;
        private String aliasField;

        ColumnAlias(String nameColumn, String aliasField) {
            this.aliasField = aliasField;
            this.nameColumn = nameColumn;
        }

        public String getAliasField() {
            return aliasField;
        }

        public String getNameColumn() {
            return nameColumn;
        }

        public String asField() {
            return String.join(" as ", dotColumn(), aliasField);
        }
        public String dotColumn(){
            return String.join(".",ALIAS_TABLE,nameColumn);
        }
    }
}
