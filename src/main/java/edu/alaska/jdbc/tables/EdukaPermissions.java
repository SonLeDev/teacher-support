package edu.alaska.jdbc.tables;

/**
 * Created by sonle on 11/24/17.
 */
public class EdukaPermissions {

    public static String NAME_TABLE = "eduka_permissions";
    public static String ALIAS_TABLE = "epm";
    public static String asTable(){
        return String.join(" as ",NAME_TABLE,ALIAS_TABLE);
    }

    public static enum ColumnAlias {
        ID("id", "permissionId"),
        NAME("name", "permissionName"),
        CODE("code","permissionCode"),
        SUBJECT_ID("subject_id","subjectId"),
        CLASS_ID("class_id","classId"),
        ;
        private String nameColumn;
        private String aliasField;

        ColumnAlias(String nameColumn, String aliasField) {
            this.aliasField = aliasField;
            this.nameColumn = nameColumn;
        }

        public String getAliasField() {
            return aliasField;
        }

        public String getNameColumn() {
            return nameColumn;
        }

        public String asField() {
            return String.join(" as ", dotColumn(), aliasField);
        }
        public String dotColumn(){
            return String.join(".",ALIAS_TABLE,nameColumn);
        }
    }

}

class EdukaRouters{
    public static String NAME_TABLE = "eduka_routers";
    public static String ALIAS_TABLE = "ert";
    public static String asTable(){
        return String.join(" as ",NAME_TABLE,ALIAS_TABLE);
    }

    public static enum ColumnAlias {
        ID("id", "routerId"),
        GROUP_NAME("group_name", "groupName"),
        GROUP_CODE("group_code","groupCode"),
        ROUTER_STATE("router_state","routerState"),
        ROUTER_NAME("router_name","routerName")
        ;
        private String nameColumn;
        private String aliasField;

        ColumnAlias(String nameColumn, String aliasField) {
            this.aliasField = aliasField;
            this.nameColumn = nameColumn;
        }

        public String getAliasField() {
            return aliasField;
        }

        public String getNameColumn() {
            return nameColumn;
        }

        public String asField() {
            return String.join(" as ", dotColumn(), aliasField);
        }
        public String dotColumn(){
            return String.join(".",ALIAS_TABLE,nameColumn);
        }
    }
}

class EdukaPermissionOnRouter{
    public static String NAME_TABLE = "eduka_permission_on_router";
    public static String ALIAS_TABLE = "epmr";
    public static String asTable(){
        return String.join(" as ",NAME_TABLE,ALIAS_TABLE);
    }

    public static enum ColumnAlias {
        ID("id", "permissionOnRouterId"),
        PERMISSION_ID("permission_id", "permissionId"),
        ROUTER_ID("router_id","routerId"),
        ;
        private String nameColumn;
        private String aliasField;

        ColumnAlias(String nameColumn, String aliasField) {
            this.aliasField = aliasField;
            this.nameColumn = nameColumn;
        }

        public String getAliasField() {
            return aliasField;
        }

        public String getNameColumn() {
            return nameColumn;
        }

        public String asField() {
            return String.join(" as ", dotColumn(), aliasField);
        }
        public String dotColumn(){
            return String.join(".",ALIAS_TABLE,nameColumn);
        }
    }
}
class EdukaUserPermission{
    public static String NAME_TABLE = "eduka_user_permission";
    public static String ALIAS_TABLE = "eupm";
    public static String asTable(){
        return String.join(" as ",NAME_TABLE,ALIAS_TABLE);
    }

    public static enum ColumnAlias {
        ID("id", "userPermissionId"),
        USER_ID("user_id", "userId"),
        PERMISSION_ID("permission_id","permissionId"),
        ;
        private String nameColumn;
        private String aliasField;

        ColumnAlias(String nameColumn, String aliasField) {
            this.aliasField = aliasField;
            this.nameColumn = nameColumn;
        }

        public String getAliasField() {
            return aliasField;
        }

        public String getNameColumn() {
            return nameColumn;
        }

        public String asField() {
            return String.join(" as ", dotColumn(), aliasField);
        }
        public String dotColumn(){
            return String.join(".",ALIAS_TABLE,nameColumn);
        }
    }
}
