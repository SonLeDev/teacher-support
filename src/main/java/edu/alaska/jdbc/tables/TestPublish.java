package edu.alaska.jdbc.tables;

/**
 * Created by sonle on 11/21/17.
 */
public class TestPublish {
    public static final String NAME_TABLE = "test_publish";
    public static final String ALIAS_TABLE = "tpbl";

    public static String asTable(){
        return String.join(" as ",NAME_TABLE,ALIAS_TABLE);
    }

    public static enum ColumnAlias{

        ID("id","testPublishedId"),
        TEST_ID("test_id","testId"),
        PUBLISHED("published","testPublished"),
        MDF_DATE("mdf_date","mdfDate");

        private String nameColumn;
        private String aliasField;
        ColumnAlias(String nameColumn, String aliasField) {
            this.aliasField = aliasField;
            this.nameColumn = nameColumn;
        }

        public String getAliasField() {
            return aliasField;
        }

        public String getNameColumn() {
            return nameColumn;
        }

        public String asField() {
            return String.join(" as ",dotColumn(), aliasField);
        }

        public String dotColumn(){
            return String.join(".",ALIAS_TABLE,nameColumn);
        }
    }
}
