package edu.alaska.jdbc;

import edu.alaska.jdbc.mapper.ChaptersUnitHandler;

/**
 * Created by sonle on 7/3/17.
 */
public interface QueryTemplate {
    String SELECT_CHAPTERS_UNIT = ChaptersUnitHandler.SELECT_CHAPTERS_UNIT;

    StringBuilder SELECT_QUIZ_UNIT_NAME
            = new StringBuilder("SELECT ")
                .append(" eq.id,eq.question,eq.answera,eq.answerb,eq.answerc,eq.answerd")
                .append(" eq.correct_answer,eq.level,eq.")
                .append(" FROM ")
            ;


    /*
    CREATE TABLE `edu_quiz` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `answera` varchar(255) DEFAULT NULL,
  `answerb` varchar(255) DEFAULT NULL,
  `answerc` varchar(255) DEFAULT NULL,
  `answerd` varchar(255) DEFAULT NULL,
  `correct_answer` varchar(255) DEFAULT NULL,
  `detail_answer` varchar(5000) DEFAULT NULL,
  `level` int(11) NOT NULL DEFAULT '1',
  `question` varchar(1000) DEFAULT NULL,
  `solution` varchar(255) DEFAULT NULL,
  `no_of_quest` int(11) NOT NULL,
  `point_of_quiz` float DEFAULT NULL,
  `image_id` int(11) DEFAULT NULL,
  `chapter_id` int(11) DEFAULT NULL,
  `chapter_name` varchar(1000) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `explain_answer` varchar(255) DEFAULT NULL,
  `subject_id` bigint(11) DEFAULT NULL,
  `unit_id` bigint(11) DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=360 DEFAULT CHARSET=utf8;
     */
}
