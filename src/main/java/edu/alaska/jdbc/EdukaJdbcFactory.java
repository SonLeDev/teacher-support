package edu.alaska.jdbc;

/**
 * Created by sonle on 11/22/17.
 */
public class EdukaJdbcFactory {
    private static EdukaJdbcFactory instance = null;
    protected EdukaJdbcFactory() {
        // Exists only to defeat instantiation.
    }
    public synchronized static EdukaJdbcFactory getInstance() {
        if(instance == null) {
            instance = new EdukaJdbcFactory();
        }
        return instance;
    }


}
