package edu.alaska.jdbc.mapper;

import edu.alaska.jdbc.tables.EduUnit;
import org.springframework.jdbc.core.RowCallbackHandler;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

/**
 * Created by sonle on 7/3/17.
 */
public class EduUnitSelectFieldHandler implements RowCallbackHandler {

    private List<Map> retData = new ArrayList<>();
    private Map paramMapper = new HashMap();
    @Override
    public void processRow(ResultSet rs) throws SQLException {
        Map<String,Object> rowRs = new HashMap<>();
        rowRs.put(EduUnit.ColumnAlias.ID.getAliasField(),
                rs.getLong(EduUnit.ColumnAlias.ID.getAliasField()));
        rowRs.put(EduUnit.ColumnAlias.UNIT_NAME.getAliasField(),
                rs.getString(EduUnit.ColumnAlias.UNIT_NAME.getAliasField()));
        rowRs.put("wanted",Boolean.FALSE);
        retData.add(rowRs);
    }

    public String getQueryStmt(Long subjectId){
        return EduUnit.queryIdAndName(subjectId,paramMapper);
    }
    public Map getParamMapper(){return paramMapper;}

    public List<Map> getRetData(){
        return retData;
    }

}
