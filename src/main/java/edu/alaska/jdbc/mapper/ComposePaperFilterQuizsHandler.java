package edu.alaska.jdbc.mapper;

import edu.alaska.jdbc.tables.EduQuiz;
import edu.alaska.jdbc.tables.EduUnit;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by sonle on 11/22/17.
 */
public class ComposePaperFilterQuizsHandler extends EdukaCallbackHandler {

    private Long[] unitIdArr;
    /**
       randQuiz : affecting to limitedRow and rowMap.put("wanted",Boolean.TRUE);
     */
    private Boolean randQuiz = Boolean.FALSE;
    private Integer limitedRow = 1000;

    public ComposePaperFilterQuizsHandler(Long[] unitIds,Boolean randQuiz,Integer limitedRow){
        this.unitIdArr = unitIds;
        this.randQuiz = randQuiz;
        this.limitedRow = limitedRow;
    }

    @Override
    public void processRow(ResultSet rs) throws SQLException {
        Map<String,Object> rowMap = new HashMap<>();

        rowMap.put(EduQuiz.ColumnAlias.ID.getAliasField()
                ,rs.getLong(EduQuiz.ColumnAlias.ID.getAliasField()));
        rowMap.put(EduQuiz.ColumnAlias.QUESTION.getAliasField()
                    ,rs.getString(EduQuiz.ColumnAlias.QUESTION.getAliasField()));

        rowMap.put(EduUnit.ColumnAlias.ID.getAliasField()
                ,rs.getLong(EduUnit.ColumnAlias.ID.getAliasField()));
        rowMap.put(EduUnit.ColumnAlias.UNIT_NAME.getAliasField()
                ,rs.getString(EduUnit.ColumnAlias.UNIT_NAME.getAliasField()));

        rowMap.put("wanted",Boolean.FALSE);

        if(randQuiz){
            rowMap.put("wanted",Boolean.TRUE);
        }
        retData.add(rowMap);
    }

    @Override
    public String getQueryStmt() {

        if(ArrayUtils.isEmpty(unitIdArr)){return "";}

        String paramArrUnitId = "arrUnitId";
        String paramLimited = "limitedRow";
        paramMapper.put(paramArrUnitId,Arrays.asList(unitIdArr)) ;

        StringBuilder qry = new StringBuilder(SELECT);

        String[] selectedFields = {
                EduQuiz.ColumnAlias.ID.asField(),EduQuiz.ColumnAlias.QUESTION.asField()
                , EduUnit.ColumnAlias.ID.asField(),EduUnit.ColumnAlias.UNIT_NAME.asField()
        };

        qry.append(StringUtils.join(selectedFields,","));
        qry.append(FROM).append(EduQuiz.asTable());
        qry.append(INNER_JOIN).append(EduUnit.asTable())
                .append(ON).append(EduUnit.ColumnAlias.ID.dotColumn())
                .append(EQUAL).append(EduQuiz.ColumnAlias.UNIT_ID.dotColumn());
        qry.append(WHERE);
        qry.append(AND).append(EduQuiz.ColumnAlias.IS_PRACT.dotColumn()).append(EQUAL_TRUE);
        qry.append(AND).append(EduQuiz.ColumnAlias.DELETED.dotColumn()).append(EQUAL_FALSE);
        qry.append(AND).append(EduQuiz.ColumnAlias.PUBLISHED.dotColumn()).append(EQUAL_TRUE);
        qry.append(AND).append(EduQuiz.ColumnAlias.UNIT_ID.dotColumn()).append(IS_NOT_NULL);
        qry.append(AND).append(EduUnit.ColumnAlias.ID.dotColumn()).append(IN)
                .append("(:").append(paramArrUnitId).append(")");

        if(randQuiz){
            qry.append(ORDER_BY).append(" RAND() ");
            qry.append(LIMIT).append(":").append(paramLimited);
            paramMapper.put(paramLimited,limitedRow) ;
        }
        StringBuilder masterQuery = new StringBuilder();
        masterQuery.append(SELECT).append(" * ").append(FROM).append("(")
                .append(qry.toString()).append(")")
                .append(AS).append("master")
                .append(ORDER_BY).append("master.").append(EduUnit.ColumnAlias.ID.getAliasField());
        return masterQuery.toString();
    }
}
