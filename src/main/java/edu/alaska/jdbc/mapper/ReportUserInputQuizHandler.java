package edu.alaska.jdbc.mapper;

import edu.alaska.common.Constances;
import org.apache.log4j.Logger;
import org.springframework.jdbc.core.RowCallbackHandler;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by sonle on 8/6/17.
 */
public class ReportUserInputQuizHandler implements RowCallbackHandler {
    final static Logger LOGGER = Logger.getLogger(ReportUserInputQuizHandler.class);

    public static final String QUERY_REPORT_USER_INPUT_QUIZ = buildReportUserInputQuizQuery();

    private static final String buildReportUserInputQuizQuery(){
        StringBuilder queryBuilder = new StringBuilder();
        queryBuilder.append("SELECT u.id userId, u.user_name userName")
                    .append(", u.subject_id subjectId,sbj.subject_name subjectName\n")
                    .append(", '' as assignName")
                    .append(", COUNT(eq.id) totalQuiz")
                    .append(",(SELECT MAX(eqq.update_date) as lastUpdate")
                        .append(" FROM edu_quiz eqq WHERE eqq.user_id = u.id ) as lastUpdate ");
        queryBuilder.append("FROM user u ")
                    .append("LEFT JOIN edu_quiz  eq ON u.id = eq.user_id ")
                    .append("LEFT JOIN edu_subject sbj ON u.subject_id = sbj.id ")
                    .append("LEFT JOIN edu_unit eu ON eq.unit_id = eu.id ");

        queryBuilder.append(" WHERE eq.deleted=0 ");

        queryBuilder.append("GROUP BY u.id HAVING totalQuiz > 0 ");
        queryBuilder.append("ORDER BY subjectId ,lastUpdate DESC");

        LOGGER.debug(String.format("%s buildReportUserInputQuizQuery :: %s",
                    Constances.LOG_TAG_DEBUG,queryBuilder.toString()));

        return queryBuilder.toString();

    }

    List<Map<String,Object>> reportData = new ArrayList<>();

    /**
         Row data : userId, userName,subjectId, subjectName,assignName,totalQuiz,lastUpdate
    */
    @Override
    public void processRow(ResultSet rs) throws SQLException {
//        LOGGER.info(String.format("%s processRow:extract query buildReportUserInputQuizQuery",Constances.LOG_TAG_INFO));
        Map<String,Object> rsRow = new HashMap<>();
        rsRow.put("userId",rs.getLong("userId"));
        rsRow.put("userName",rs.getString("userName"));
        rsRow.put("subjectId",rs.getLong("subjectId"));
        rsRow.put("subjectName",rs.getString("subjectName"));
        rsRow.put("assignName",rs.getString("assignName"));
        rsRow.put("totalQuiz",rs.getInt("totalQuiz"));
        rsRow.put("lastUpdate",rs.getDate("lastUpdate"));
        reportData.add(rsRow);
    }

    public List getReportData(){
        return reportData;
    }
}
