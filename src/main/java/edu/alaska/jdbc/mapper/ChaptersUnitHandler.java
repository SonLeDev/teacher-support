package edu.alaska.jdbc.mapper;

import edu.alaska.form.ChaptersUnitForm;
import org.springframework.jdbc.core.RowCallbackHandler;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

/**
 * Created by sonle on 7/3/17.
 */
public class ChaptersUnitHandler implements RowCallbackHandler {

    public static final String SELECT_CHAPTERS_UNIT ;

    static {
        StringBuilder selectChaptersUnit = new StringBuilder();
        selectChaptersUnit.append("SELECT ec.id chapterId,ec.chp_name chapterName, eu.id unitId, eu.unit_name unitName ")
                .append("FROM edu_chapters ec ")
                .append(" LEFT JOIN edu_unit eu ON ec.id = eu.chapter_id")
                .append(" WHERE ec.subject_id =:subjectId ")
                .append(" ORDER BY ec.chp_order, eu.unit_no");
        SELECT_CHAPTERS_UNIT = selectChaptersUnit.toString();
    }

    Map<Long,ChaptersUnitForm> chaptersUnitMap = new HashMap<>();
    @Override
    public void processRow(ResultSet resultSet) throws SQLException {
            Long chapterId = resultSet.getLong("chapterId");
            ChaptersUnitForm chapterForm = null;
            if(chaptersUnitMap.containsKey(chapterId)){
                chapterForm = chaptersUnitMap.get(chapterId);
            }else{
                chapterForm = new ChaptersUnitForm();
                chapterForm.setChapterId(chapterId);
                chapterForm.setChapterName(resultSet.getString("chapterName"));
                chaptersUnitMap.put(chapterId,chapterForm);
            }

            assert chapterForm !=null;

        ChaptersUnitForm.UnitForm unitForm =  chapterForm.new UnitForm();
        unitForm.setUnitId(resultSet.getLong("unitId"));
        unitForm.setUnitName(resultSet.getString("unitName"));
        chapterForm.getUnitList().add(unitForm);
    }

    public Collection<ChaptersUnitForm> getChaptersUnitLst() {
        return chaptersUnitMap.values();
    }
}
