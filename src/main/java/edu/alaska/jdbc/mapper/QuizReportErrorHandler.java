package edu.alaska.jdbc.mapper;

import edu.alaska.jdbc.tables.EduQuiz;
import edu.alaska.jdbc.tables.EduQuizImage;
import edu.alaska.jdbc.tables.EduQuizReports;
import edu.alaska.jdbc.tables.UserAccounts;
import org.apache.commons.lang.StringUtils;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by sonle on 11/28/17.
 */
public class QuizReportErrorHandler extends EdukaCallbackHandler{


    private Long userId;
    private Long subjectId;

    public QuizReportErrorHandler(Long userId,Long subjectId){
        this.userId = userId;
        this.subjectId = subjectId;
    }

    @Override
    public String getQueryStmt() {

        String paramUserId = "userId";
        String paramSubjectId = "subjectId";
        getParamMapper().put(paramUserId,userId);
        getParamMapper().put(paramSubjectId,subjectId);

        String[] selectedField = {EduQuiz.ColumnAlias.ID.asField(),
                EduQuiz.ColumnAlias.QUESTION.asField()
                ,EduQuiz.ColumnAlias.ANSWER_A.asField()
                ,EduQuiz.ColumnAlias.ANSWER_B.asField()
                ,EduQuiz.ColumnAlias.ANSWER_C.asField()
                ,EduQuiz.ColumnAlias.ANSWER_D.asField()
                ,EduQuiz.ColumnAlias.DETAIL_ANSWER.asField()
                ,EduQuiz.ColumnAlias.UNIT_ID.asField()
                ,EduQuiz.ColumnAlias.LEVEL.asField()
                ,EduQuiz.ColumnAlias.CORRECT_ANSWER.asField()
                , EduQuizImage.ColumnAlias.QUEST_IMG.asField()
                , EduQuizImage.ColumnAlias.DETAIL_ANS_IMG.asField()
                , EduQuizReports.ColumnAlias.STATUS_REPORT.asField()
                , EduQuizReports.ColumnAlias.TYPE_REPORT.asField()
                , UserAccounts.ColumnAlias.USER_NAME.asField()
                , UserAccounts.ColumnAlias.ID.asField()
            };

        StringBuilder qry = new StringBuilder(SELECT);
        qry.append(StringUtils.join(selectedField,","));
        qry.append(FROM).append(EduQuiz.asTable());
        qry.append(INNER_JOIN).append(EduQuizReports.asTable())
                .append(ON).append(EduQuiz.ColumnAlias.ID.dotColumn())
                .append(EQUAL).append(EduQuizReports.ColumnAlias.QUIZ_ID.dotColumn());
        qry.append(LEFT_JOIN).append(EduQuizImage.asTable())
                .append(ON).append(EduQuiz.ColumnAlias.ID.dotColumn())
                .append(EQUAL).append(EduQuizImage.ColumnAlias.QUIZ_ID.dotColumn());
        qry.append(LEFT_JOIN).append(UserAccounts.asTable())
                .append(ON).append(EduQuiz.ColumnAlias.USER_ID.dotColumn())
                .append(EQUAL).append(EduQuizImage.ColumnAlias.ID.dotColumn());

        qry.append(WHERE);
        qry.append(AND).append(EduQuiz.ColumnAlias.USER_ID.dotColumn()).append(EQUAL).append(":").append(paramUserId);
        qry.append(AND).append(EduQuiz.ColumnAlias.SUBJECT_ID.dotColumn()).append(EQUAL).append(":").append(paramSubjectId);
        qry.append(AND).append(EduQuizReports.ColumnAlias.STATUS_REPORT.dotColumn()).append(EQUAL_TRUE);

        return qry.toString();
    }

    @Override
    public void processRow(ResultSet rs) throws SQLException {

        Map<String,Object> row = new HashMap<>();
        row.put(EduQuiz.ColumnAlias.ID.getAliasField(),rs.getLong(EduQuiz.ColumnAlias.ID.getAliasField()));
        row.put(EduQuiz.ColumnAlias.QUESTION.getAliasField(),rs.getString(EduQuiz.ColumnAlias.QUESTION.getAliasField()));
        row.put(EduQuiz.ColumnAlias.ANSWER_A.getAliasField(),rs.getString(EduQuiz.ColumnAlias.ANSWER_A.getAliasField()));
        row.put(EduQuiz.ColumnAlias.ANSWER_B.getAliasField(),rs.getString(EduQuiz.ColumnAlias.ANSWER_B.getAliasField()));
        row.put(EduQuiz.ColumnAlias.ANSWER_C.getAliasField(),rs.getString(EduQuiz.ColumnAlias.ANSWER_C.getAliasField()));
        row.put(EduQuiz.ColumnAlias.ANSWER_D.getAliasField(),rs.getString(EduQuiz.ColumnAlias.ANSWER_D.getAliasField()));
        row.put(EduQuiz.ColumnAlias.DETAIL_ANSWER.getAliasField(),rs.getString(EduQuiz.ColumnAlias.DETAIL_ANSWER.getAliasField()));
        row.put(EduQuiz.ColumnAlias.UNIT_ID.getAliasField(),rs.getLong(EduQuiz.ColumnAlias.UNIT_ID.getAliasField()));
        row.put(EduQuiz.ColumnAlias.LEVEL.getAliasField(),rs.getInt(EduQuiz.ColumnAlias.LEVEL.getAliasField()));
        row.put(EduQuiz.ColumnAlias.CORRECT_ANSWER.getAliasField(),rs.getString(EduQuiz.ColumnAlias.CORRECT_ANSWER.getAliasField()));
        row.put(EduQuizImage.ColumnAlias.QUEST_IMG.getAliasField(),rs.getString(EduQuizImage.ColumnAlias.QUEST_IMG.getAliasField()));
        row.put(EduQuizImage.ColumnAlias.DETAIL_ANS_IMG.getAliasField(),rs.getString(EduQuizImage.ColumnAlias.DETAIL_ANS_IMG.getAliasField()));
        row.put(EduQuizReports.ColumnAlias.STATUS_REPORT.getAliasField(),rs.getBoolean(EduQuizReports.ColumnAlias.STATUS_REPORT.getAliasField()));
        row.put(EduQuizReports.ColumnAlias.TYPE_REPORT.getAliasField(),rs.getString(EduQuizReports.ColumnAlias.TYPE_REPORT.getAliasField()));
        row.put(UserAccounts.ColumnAlias.USER_NAME.getAliasField(),rs.getString(UserAccounts.ColumnAlias.USER_NAME.getAliasField()));
        row.put(UserAccounts.ColumnAlias.ID.getAliasField(),rs.getLong(UserAccounts.ColumnAlias.ID.getAliasField()));


        getRetData().add(row);
    }
}
