package edu.alaska.jdbc.mapper;

import edu.alaska.jdbc.tables.EduTestGroup;
import edu.alaska.jdbc.tables.EduTestType;
import edu.alaska.jdbc.tables.TestPaper;
import edu.alaska.jdbc.tables.TestPublish;
import org.apache.log4j.Logger;
import org.springframework.jdbc.core.RowCallbackHandler;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by sonle on 8/29/17.
 */
public class TestingHandler implements RowCallbackHandler{

    final static Logger LOGGER = Logger.getLogger(TestingHandler.class);

    private List dataList = new ArrayList();

    public String findTesting(Long testId,Map nameParamMapper){
        TestPaper testPaper = new TestPaper();
        return testPaper.queryTestByTestId(testId,nameParamMapper);
    }

    @Override
    public void processRow(ResultSet rs) throws SQLException {
        Map<String,Object> row = new HashMap<>();

        row.put(TestPaper.ColumnAlias.ID.getAliasField()
                ,rs.getLong(TestPaper.ColumnAlias.ID.getAliasField()));
        row.put(TestPaper.ColumnAlias.NAME.getAliasField()
                ,rs.getString(TestPaper.ColumnAlias.NAME.getAliasField()));
        row.put(TestPaper.ColumnAlias.CODE.getAliasField()
                ,rs.getString(TestPaper.ColumnAlias.CODE.getAliasField()));
        row.put(TestPaper.ColumnAlias.AUTHOR.getAliasField()
                ,rs.getString(TestPaper.ColumnAlias.AUTHOR.getAliasField()));
        row.put(TestPaper.ColumnAlias.DURATION.getAliasField()
                ,rs.getLong(TestPaper.ColumnAlias.DURATION.getAliasField()));
        row.put(TestPaper.ColumnAlias.SUBJECT_ID.getAliasField()
                ,rs.getLong(TestPaper.ColumnAlias.SUBJECT_ID.getAliasField()));
        row.put(TestPaper.ColumnAlias.CLASS_ID.getAliasField()
                ,rs.getLong(TestPaper.ColumnAlias.CLASS_ID.getAliasField()));
        row.put(TestPaper.ColumnAlias.MDF_DATE.getAliasField()
                ,rs.getLong(TestPaper.ColumnAlias.MDF_DATE.getAliasField()));
        row.put(TestPublish.ColumnAlias.PUBLISHED.getAliasField()
                ,rs.getBoolean(TestPublish.ColumnAlias.PUBLISHED.getAliasField()));
        row.put(EduTestType.ColumnAlias.NAME.getAliasField()
                ,rs.getString(EduTestType.ColumnAlias.NAME.getAliasField()));
        row.put(EduTestGroup.ColumnAlias.NAME.getAliasField()
                ,rs.getString(EduTestGroup.ColumnAlias.NAME.getAliasField()));
        row.put(EduTestGroup.ColumnAlias.TEST_GROUP_PUBLISHED.getAliasField()
                ,rs.getString(EduTestGroup.ColumnAlias.TEST_GROUP_PUBLISHED.getAliasField()));

        row.put("numQuizPublised",rs.getLong("numQuizPublised"));
        row.put("numQuizUnPublised",rs.getLong("numQuizUnPublised"));

        dataList.add(row);
    }

    public List<Map> getDataList() {
        return dataList;
    }

}
