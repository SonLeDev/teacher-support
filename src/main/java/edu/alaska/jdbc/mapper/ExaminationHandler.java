package edu.alaska.jdbc.mapper;

import edu.alaska.jdbc.tables.ExaminationPublish;
import edu.alaska.jdbc.tables.EduTags;
import edu.alaska.jdbc.tables.ExaminationPaper;
import org.apache.log4j.Logger;
import org.springframework.jdbc.core.RowCallbackHandler;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by sonle on 8/29/17.
 */
public class ExaminationHandler implements RowCallbackHandler{

    final static Logger LOGGER = Logger.getLogger(ExaminationHandler.class);

    private List dataList = new ArrayList();

    @Override
    public void processRow(ResultSet rs) throws SQLException {
        Map<String,Object> row = new HashMap<>();

        row.put(ExaminationPaper.ColumnAlias.ID.getAliasField()
                ,rs.getLong(ExaminationPaper.ColumnAlias.ID.getAliasField()));
        row.put(ExaminationPaper.ColumnAlias.AUTHOR.getAliasField()
                ,rs.getString(ExaminationPaper.ColumnAlias.AUTHOR.getAliasField()));
        row.put(ExaminationPaper.ColumnAlias.EXAM_CODE.getAliasField()
                ,rs.getString(ExaminationPaper.ColumnAlias.EXAM_CODE.getAliasField()));
        row.put(ExaminationPaper.ColumnAlias.EXAM_NAME.getAliasField()
                ,rs.getString(ExaminationPaper.ColumnAlias.EXAM_NAME.getAliasField()));
        row.put(ExaminationPaper.ColumnAlias.NUM_OF_QUEST.getAliasField()
                ,rs.getLong(ExaminationPaper.ColumnAlias.NUM_OF_QUEST.getAliasField()));
        row.put(ExaminationPaper.ColumnAlias.DURATION.getAliasField()
                ,rs.getLong(ExaminationPaper.ColumnAlias.DURATION.getAliasField()));
        row.put(ExaminationPaper.ColumnAlias.SUBJECT_ID.getAliasField()
                ,rs.getLong(ExaminationPaper.ColumnAlias.SUBJECT_ID.getAliasField()));
        row.put(ExaminationPaper.ColumnAlias.CLASS_ID.getAliasField()
                ,rs.getLong(ExaminationPaper.ColumnAlias.CLASS_ID.getAliasField()));
        row.put(ExaminationPublish.ColumnAlias.PUBLISHED.getAliasField()
                ,rs.getBoolean(ExaminationPublish.ColumnAlias.PUBLISHED.getAliasField()));
        row.put(EduTags.ColumnAlias.TAG_NAME.getAliasField()
                ,rs.getString(EduTags.ColumnAlias.TAG_NAME.getAliasField()));
        row.put(EduTags.ColumnAlias.TAG_PUBLISHED.getAliasField()
                ,rs.getString(EduTags.ColumnAlias.TAG_PUBLISHED.getAliasField()));

        row.put("numQuizPublised",rs.getLong("numQuizPublised"));
        row.put("numQuizUnPublised",rs.getLong("numQuizUnPublised"));

        dataList.add(row);
    }

    public List<Map> getDataList() {
        return dataList;
    }

}
