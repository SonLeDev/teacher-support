package edu.alaska.jdbc.mapper;

import edu.alaska.common.CallbackJdbcResultSet;
import edu.alaska.form.ReqFilterForm;
import org.apache.log4j.Logger;
import org.springframework.jdbc.core.RowCallbackHandler;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by sonle on 8/6/17.
 */
public class EduQuizHanlder implements RowCallbackHandler {

    final static Logger LOGGER = Logger.getLogger(EduQuizHanlder.class);

    private CallbackJdbcResultSet callbackRs;

    public EduQuizHanlder(){}

    public EduQuizHanlder(CallbackJdbcResultSet callbackRs){
        this.callbackRs = callbackRs;
    }

    private List<Map<String,Object>> retQuiz = new ArrayList<>();

    private EduQuizQueryBuilder queryBuilder = EduQuizQueryBuilder.getInstance();

    public Map getParameterMapping(){
        return queryBuilder.getParameterMap();
    }

    public String getQueryByFilterForm(ReqFilterForm filterForm) {

        if(filterForm.getExamId()!=null && filterForm.getExamId()!=0){
            queryBuilder.qryByExamId(this,filterForm.getExamId(),filterForm.getPublished());
        }
        if(filterForm.getTestId()!=null && filterForm.getTestId()!=0){
            queryBuilder.qryByTestId(this,filterForm.getTestId(),filterForm.getPublished());
        }
        if(filterForm.getUnitId()!=null && filterForm.getUnitId() !=0){
            queryBuilder.qryByUnitId(filterForm.getUnitId(),filterForm.getPublished());
        }
        if(filterForm.getUserId()!=null && filterForm.getUserId() !=0){
            queryBuilder.qryByUserId(filterForm.getUserId());
        }
        if(filterForm.getUpdateDate()!=null){
            queryBuilder.qryByUpdateDate(filterForm.getUpdateDate());
        }

        queryBuilder.qryQuizReportedNotError();
        queryBuilder.qryQuizValidable();

        return queryBuilder.buildSelectAllFields();
    }

    public String getQueryStmtAllFields(){
        return queryBuilder.buildSelectAllFields();
    }

    public String getQueryByUserId(Long userId){
        return queryBuilder.qryByUserId(userId).qryQuizValidable().buildSelectAllFields();
    }

    public String getQueryByUserIdAndSubjectId(Long userId, Long subjectId) {
        return queryBuilder.qryByUserId(userId).qryBySubjectId(subjectId).qryQuizValidable().buildSelectAllFields();

    }

    public String getQueryBySubjectIdAndUserId(Long subjectId,Long userId){
        return queryBuilder.qryBySubjectId(subjectId).qryByUserId(userId).qryQuizValidable().buildSelectAllFields();
    }

    public String getQueryByQuizId(Long quizId){return queryBuilder.qryByQuizId(quizId).qryQuizValidable().buildSelectAllFields();}

    public String getQueryByExamId(Long examId,Boolean published){
        return queryBuilder.qryByExamId(this,examId,published)
                .qryQuizValidable().buildSelectAllFields();}
    public String getQueryByExamId(Long examId){
        return queryBuilder.qryByExamId(this,examId,null)
                .qryQuizValidable().buildSelectAllFields();}

    public String getQueryByTestId(Long testId,Boolean published) {
        return queryBuilder
                .qryByTestId(this,testId,published)
                .qryQuizValidable().buildSelectAllFields();
    }

    public String getQueryByUpdateDate(Date updateDate) {
        return queryBuilder.qryByUpdateDate(updateDate).qryQuizValidable().buildSelectAllFields();
    }

    @Override
    public void processRow(ResultSet rs) throws SQLException {

        Map<String,Object> rsRow  = new HashMap<>();
        rsRow.put("quizId",rs.getLong("quizId"));
        rsRow.put("question",rs.getString("question"));
        rsRow.put("level",rs.getInt("level"));
        rsRow.put("answerA",rs.getString("answerA"));
        rsRow.put("answerB",rs.getString("answerB"));
        rsRow.put("answerC",rs.getString("answerC"));
        rsRow.put("answerD",rs.getString("answerD"));
        rsRow.put("correctAnswer",rs.getString("correctAnswer"));
        rsRow.put("detailAnswer",rs.getString("detailAnswer"));
        rsRow.put("solution",rs.getString("solution"));
        rsRow.put("unitId",rs.getLong("unitId"));
        rsRow.put("unitName",rs.getString("unitName"));
        rsRow.put("userId",rs.getLong("userId"));
        rsRow.put("subjectId",rs.getLong("subjectId"));
        rsRow.put("author",rs.getString("author"));
        rsRow.put("questImg",rs.getString("questImg"));
        rsRow.put("dtailAnsImg",rs.getString("dtailAnsImg"));
        rsRow.put("updateDate",rs.getDate("updateDate"));


        if(callbackRs!=null){
            callbackRs.putMoreTo(rsRow,rs);
        }
        retQuiz.add(rsRow);

    }

    public List<Map<String,Object>> getRetQuizs() {
        return retQuiz;
    }


    public void setCallbackRs(CallbackJdbcResultSet callbackRs) {
        this.callbackRs = callbackRs;
    }



}
/**
 * This using for building query edu_quiz,
 * you only build query base on public method on this builder.
 */
 class EduQuizQueryBuilder{

    private static final StringBuilder SELECT_ALL_FIELD_BUILDER;
    private static final StringBuilder SELECT_BASIC_FIELD_BUILDER;
    private static final StringBuilder FROM_BASIC_BUILDER;

    private static final StringBuilder ORDER_BY_BUILDER;

    /**
     * Where is dynamic for each of times create new handler
     */
    private StringBuilder SELECT_EXT_BUILDER;
    private StringBuilder WHERE_BUILDER;
    private StringBuilder LEFT_JOIN_BUILDER;

    private final Map nameParamMap = new HashMap();
    private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd 00:00:00");
    static {
        SELECT_ALL_FIELD_BUILDER = new StringBuilder();
        SELECT_ALL_FIELD_BUILDER.append("SELECT eq.id quizId,eq.question question,eq.level level ")
                .append(",eq.answera answerA,eq.answerb answerB,eq.answerc answerC,eq.answerd answerD ")
                .append(",eq.correct_answer correctAnswer,eq.detail_answer detailAnswer,eq.solution ")
                .append(",eq.unit_id unitId, eu.unit_name unitName")
                .append(",eq.user_id userId, u.user_name author")
                .append(",eq.subject_id subjectId")
                .append(",eq.update_date updateDate")
                .append(",eqi.quest_img questImg, eqi.dtail_ans_img dtailAnsImg ");

        SELECT_BASIC_FIELD_BUILDER = new StringBuilder()
                .append("SELECT eq.id quizId,question ");

        FROM_BASIC_BUILDER = new StringBuilder();
        FROM_BASIC_BUILDER.append(" FROM edu_quiz eq ")
                .append(" LEFT JOIN edu_unit eu ON eq.unit_id = eu.id ")
                .append(" LEFT JOIN user u ON eq.user_id = u.id ")
                .append(" LEFT JOIN edu_quiz_image eqi ON eq.id = eqi.quiz_id ")
                .append(" LEFT JOIN edu_quiz_reports eqrp ON eq.id = eqrp.quiz_id ");

        ORDER_BY_BUILDER = new StringBuilder();
        ORDER_BY_BUILDER.append(" ORDER BY eq.update_date DESC");

    }

    /**
     * The builder is stateful object, take care about multi-thread
     * Should not using single-ton object
     */
    private static EduQuizQueryBuilder instance;

    public static EduQuizQueryBuilder getInstance(){
        return instance = new EduQuizQueryBuilder();
    }

    public Map getParameterMap(){ return nameParamMap;}

    public EduQuizQueryBuilder qryQuizReportedError(){
        validateWhere();
        WHERE_BUILDER.append(" eqrp.status_report=1 ");
        return this;
    }

    public EduQuizQueryBuilder qryQuizReportedNotError(){
        validateWhere();
        WHERE_BUILDER.append(" ( eqrp.status_report=0 OR eqrp.status_report IS NULL ) ");
        return this;
    }

    public EduQuizQueryBuilder qryQuizValidable(){
        validateWhere();
        WHERE_BUILDER.append(" eq.deleted=0 ");
        return this;
    }

    public EduQuizQueryBuilder qryQuizInValid(){
        validateWhere();
        WHERE_BUILDER.append(" eq.deleted=1 ");
        return this;
    }

    public EduQuizQueryBuilder qryQuizPublished(){
        validateWhere();
        WHERE_BUILDER.append(" eq.published=1 ");
        return this;
    }

    public EduQuizQueryBuilder qryQuizUnPublished(){
        validateWhere();
        WHERE_BUILDER.append(" eq.published=0 ");
        return this;
    }

    public EduQuizQueryBuilder qryBySubjectId(Long subjectId){
        validateWhere();
        if(!nameParamMap.containsKey("subjectId")){
            nameParamMap.put("subjectId",subjectId);
            WHERE_BUILDER.append(" eq.subject_id=:subjectId ");
        }
        return this;
    }

    public EduQuizQueryBuilder qryByUserId(Long userId){
        validateWhere();
        if(!nameParamMap.containsKey("userId")){
            nameParamMap.put("userId",userId);
            WHERE_BUILDER.append(" u.id=:userId ");
        }

        return this;
    }

    public EduQuizQueryBuilder qryByQuizId(Long quizId){
        validateWhere();
        if(!nameParamMap.containsKey("quizId")){
            nameParamMap.put("quizId",quizId);
            WHERE_BUILDER.append(" eq.id=:quizId ");
        }
        return this;
    }

    public EduQuizQueryBuilder qryByUnitId(Long unitId, Boolean published) {
        validateWhere();
        if(!nameParamMap.containsKey("unitId")){
            nameParamMap.put("unitId",unitId);
            nameParamMap.put("published",published);
            WHERE_BUILDER.append(" eq.unit_id =:unitId AND eq.is_pract =1 AND eq.published=:published ");
        }
        return this;
    }

    public EduQuizQueryBuilder qryByUpdateDate(Date updateDate){
        validateWhere();
        if(!nameParamMap.containsKey("updateDate")){
            new SimpleDateFormat("yyyy-MM-dd 00:00:00");
            dateFormat.applyPattern("yyyy-MM-dd 00:00:00");
            nameParamMap.put("updateDateF",dateFormat.format(updateDate));
            dateFormat.applyPattern("yyyy-MM-dd 23:59:59");
            nameParamMap.put("updateDateT",dateFormat.format(updateDate));
            WHERE_BUILDER.append(" eq.update_date >:updateDateF AND eq.update_date <:updateDateT ");
        }
        return this;
    }


    public EduQuizQueryBuilder qryByTestId(EduQuizHanlder eduQuizHanlder, Long testId,Boolean published) {
        validateLeftJoin();
        validateSelectExt();
        if(!nameParamMap.containsKey("testId")){
            LEFT_JOIN_BUILDER.append(" INNER JOIN test_quizs tqs ")
                    .append(" ON tqs.quiz_id = eq.id ")
                    .append(" AND tqs.test_id =:testId  ")
                    .append(" AND tqs.deleted = 0 ");

            nameParamMap.put("testId",testId);
            SELECT_EXT_BUILDER.append(",tqs.orders noOfQuiz");

            if(published!=null){
                LEFT_JOIN_BUILDER.append(" AND tqs.published =:published");
                nameParamMap.put("published",published);
            }

            CallbackJdbcResultSet callbackGetMoreFiled = (rsRow,rs) ->{
                try {
                    rsRow.put("noOfQuest",rs.getInt("noOfQuest"));
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            };

            eduQuizHanlder.setCallbackRs(callbackGetMoreFiled);
        }
        return this;
    }

    public EduQuizQueryBuilder qryByExamId(EduQuizHanlder eduQuizHanlder, Long examId,Boolean published){
        validateLeftJoin();
        validateSelectExt();
        if(!nameParamMap.containsKey("examId")){
            LEFT_JOIN_BUILDER.append(" INNER JOIN examination_quizs exq ")
                    .append(" ON exq.quiz_id = eq.id ")
                    .append(" AND exq.exam_id =:examId ")
                    .append(" AND exq.deleted = 0 ");

            nameParamMap.put("examId",examId);

            if(published!=null){
                LEFT_JOIN_BUILDER.append(" AND exq.published =:published");
                nameParamMap.put("published",published);
            }

            SELECT_EXT_BUILDER.append(",exq.orders noOfQuest");

            CallbackJdbcResultSet callbackGetMoreField = (rsRow,rs) ->{
                try {
                    rsRow.put("noOfQuest",rs.getInt("noOfQuest"));
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            };

            eduQuizHanlder.setCallbackRs(callbackGetMoreField);
        }
        return this;
    }

    private void validateSelectExt() {
        if (SELECT_EXT_BUILDER == null) {
            SELECT_EXT_BUILDER = new StringBuilder("");
        }
    }
    private void validateWhere(){
        if(WHERE_BUILDER == null || WHERE_BUILDER.length()<=0){
            WHERE_BUILDER = new StringBuilder(" WHERE ");
        }else{
            WHERE_BUILDER.append(" AND ");
        }
    }

    private void validateLeftJoin(){
        if(LEFT_JOIN_BUILDER == null){
            LEFT_JOIN_BUILDER = new StringBuilder();
        }
    }

    public String buildSelectAllFields(){

        StringBuilder qryBuilder = new StringBuilder();
        qryBuilder.append(SELECT_ALL_FIELD_BUILDER.toString());
        if(SELECT_EXT_BUILDER!=null && SELECT_EXT_BUILDER.length()>0){
            qryBuilder.append(SELECT_EXT_BUILDER.toString());
        }

        qryBuilder.append(FROM_BASIC_BUILDER.toString());

        // append dynamic LEFT JOIN
        if(LEFT_JOIN_BUILDER !=null && LEFT_JOIN_BUILDER.length() >0){
            qryBuilder.append(LEFT_JOIN_BUILDER.toString());

        }
        // append dynamic WHERE
        if(WHERE_BUILDER!=null && WHERE_BUILDER.length() > 0){
            qryBuilder.append(WHERE_BUILDER.toString());
        }

        qryBuilder.append(ORDER_BY_BUILDER.toString());

        return qryBuilder.toString();
    }
    public String buildSelectBasicFields(){

        StringBuilder qryBuilder = new StringBuilder();
        qryBuilder.append(SELECT_BASIC_FIELD_BUILDER.toString());
        qryBuilder.append(FROM_BASIC_BUILDER.toString());
        if(WHERE_BUILDER!=null && WHERE_BUILDER.length() > 0){
            qryBuilder.append(WHERE_BUILDER.toString());
        }
        // append dynamic LEFT JOIN
        if(LEFT_JOIN_BUILDER !=null && LEFT_JOIN_BUILDER.length() >0){
            qryBuilder.append(LEFT_JOIN_BUILDER.toString());

        }
        qryBuilder.append(ORDER_BY_BUILDER.toString());

        return qryBuilder.toString();
    }



}
