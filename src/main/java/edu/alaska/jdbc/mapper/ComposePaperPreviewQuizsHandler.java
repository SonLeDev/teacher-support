package edu.alaska.jdbc.mapper;

import edu.alaska.jdbc.tables.EduQuiz;
import edu.alaska.jdbc.tables.EduUnit;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by sonle on 11/22/17.
 */
public class ComposePaperPreviewQuizsHandler extends EdukaCallbackHandler {

    private Long[] quizIdArr;

    public ComposePaperPreviewQuizsHandler(Long[] quizIdArr){
        this.quizIdArr = quizIdArr;
    }

    @Override
    public void processRow(ResultSet rs) throws SQLException {
        Map<String,Object> rowMap = new HashMap<>();

        rowMap.put(EduQuiz.ColumnAlias.ID.getAliasField()
                ,rs.getLong(EduQuiz.ColumnAlias.ID.getAliasField()));
        rowMap.put(EduQuiz.ColumnAlias.QUESTION.getAliasField()
                    ,rs.getString(EduQuiz.ColumnAlias.QUESTION.getAliasField()));
        rowMap.put(EduQuiz.ColumnAlias.ANSWER_A.getAliasField()
                ,rs.getString(EduQuiz.ColumnAlias.ANSWER_A.getAliasField()));
        rowMap.put(EduQuiz.ColumnAlias.ANSWER_B.getAliasField()
                ,rs.getString(EduQuiz.ColumnAlias.ANSWER_B.getAliasField()));
        rowMap.put(EduQuiz.ColumnAlias.ANSWER_C.getAliasField()
                ,rs.getString(EduQuiz.ColumnAlias.ANSWER_C.getAliasField()));
        rowMap.put(EduQuiz.ColumnAlias.ANSWER_D.getAliasField()
                ,rs.getString(EduQuiz.ColumnAlias.ANSWER_D.getAliasField()));

        retData.add(rowMap);
    }

    @Override
    public String getQueryStmt() {

        if(ArrayUtils.isEmpty(quizIdArr)){return "";}

        String paramArrUnitId = "arrQuizId";

        paramMapper = Collections.singletonMap(paramArrUnitId, Arrays.asList(quizIdArr));

        StringBuilder query = new StringBuilder(SELECT);

        String[] selectedFields = {
                EduQuiz.ColumnAlias.ID.asField(),EduQuiz.ColumnAlias.QUESTION.asField()
                ,EduQuiz.ColumnAlias.ANSWER_A.asField(),EduQuiz.ColumnAlias.ANSWER_B.asField()
                ,EduQuiz.ColumnAlias.ANSWER_C.asField(),EduQuiz.ColumnAlias.ANSWER_D.asField()
        };

        query.append(StringUtils.join(selectedFields,","));
        query.append(FROM).append(EduQuiz.asTable());
        query.append(INNER_JOIN).append(EduUnit.asTable())
                .append(ON).append(EduUnit.ColumnAlias.ID.dotColumn())
                .append(EQUAL).append(EduQuiz.ColumnAlias.UNIT_ID.dotColumn());
        query.append(WHERE);
        query.append(AND).append(EduQuiz.ColumnAlias.IS_PRACT.dotColumn()).append(EQUAL_TRUE);
        query.append(AND).append(EduQuiz.ColumnAlias.DELETED.dotColumn()).append(EQUAL_FALSE);
        query.append(AND).append(EduQuiz.ColumnAlias.PUBLISHED.dotColumn()).append(EQUAL_TRUE);
        query.append(AND).append(EduQuiz.ColumnAlias.UNIT_ID.dotColumn()).append(IS_NOT_NULL);
        query.append(AND).append(EduQuiz.ColumnAlias.ID.dotColumn()).append(IN)
                .append("(:").append(paramArrUnitId).append(")");

        query.append(ORDER_BY).append(EduQuiz.ColumnAlias.UNIT_ID.dotColumn());

        return query.toString();
    }
}
