package edu.alaska.jdbc.mapper;

import org.springframework.jdbc.core.RowCallbackHandler;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by sonle on 11/22/17.
 */
public abstract class EdukaCallbackHandler implements RowCallbackHandler {

    public static final String SELECT = " SELECT ";
    public static final String WHERE = " WHERE 1=1 ";
    public static final String FROM = " FROM ";
    public static final String LEFT_JOIN = " LEFT JOIN ";
    public static final String INNER_JOIN = " INNER JOIN ";
    public static final String RIGHT_JOIN = " RIGHT JOIN ";
    public static final String ON = " ON ";
    public static final String AS = " AS ";
    public static final String AND = " AND ";
    public static final String ORDER_BY = " ORDER BY ";
    public static final String LIMIT = " LIMIT ";
    public static final String EQUAL = " = ";
    public static final String EQUAL_FALSE = " = 0 ";
    public static final String EQUAL_TRUE = " = 1 ";
    public static final String IS_NOT_NULL = " IS NOT NULL ";
    public static final String IN = " IN ";


    protected List<Map> retData = new ArrayList<>();
    protected Map paramMapper = new HashMap();

    public abstract String getQueryStmt();

    public Map getParamMapper(){return paramMapper;}

    public List<Map> getRetData(){
        return retData;
    }
}
