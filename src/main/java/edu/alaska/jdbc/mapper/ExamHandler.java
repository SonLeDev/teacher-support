package edu.alaska.jdbc.mapper;

import org.apache.log4j.Logger;
import org.springframework.jdbc.core.RowCallbackHandler;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by sonle on 8/29/17.
 */
public class ExamHandler implements RowCallbackHandler{

    final static Logger LOGGER = Logger.getLogger(ExamHandler.class);

    String queryStr = "SELECT ep.id id,ep.exam_code examCode,ep.exam_name examName " +
            ",num_of_quiz numOfQuiz " +
            ",(select count(1) from examination_quizs exq where exq.published = 1 and exq.exam_id = ep.id) countQuiz " +
            ",ep.time_expire timeExpire,ep.year_exam examYear,ep.description,ep.instruction " +
            ",COALESCE(expl.published, 0) published, expl.mdf_date publishDate " +
            " FROM examination_paper ep " +
            " LEFT JOIN examination_publish expl ON ep.id = expl.exam_id " +
            " WHERE ep.subject_id =:subjectId " +
            " AND ep.class_id =:classId " +
            " AND author =:author " +
            " ORDER BY ep.mdf_date DESC";

    String queryStrWithoutAuthor = "SELECT ep.id id,ep.exam_code examCode,ep.exam_name examName " +
            ",num_of_quiz numOfQuiz " +
            ",(select count(1) from examination_quizs exq where exq.published = 1 and exq.exam_id = ep.id) countQuiz " +
            ",ep.time_expire timeExpire,ep.year_exam examYear,ep.description,ep.instruction " +
            ",COALESCE(expl.published, 0) published, expl.mdf_date publishDate " +
            " FROM examination_paper ep " +
            " LEFT JOIN examination_publish expl ON ep.id = expl.exam_id " +
            " WHERE ep.subject_id =:subjectId " +
            " AND ep.class_id =:classId " +
            " ORDER BY ep.mdf_date DESC";

    private List dataList = new ArrayList();
    private Map param ;
    @Override
    public void processRow(ResultSet rs) throws SQLException {
        Map<String,Object> row = new HashMap<>();
        row.put("examId",rs.getLong("id"));
        row.put("examCode",rs.getString("examCode"));
        row.put("examName",rs.getString("examName"));
        row.put("numOfQuiz",rs.getLong("numOfQuiz"));
        row.put("countQuiz",rs.getLong("countQuiz"));
        row.put("timeExpire",rs.getLong("timeExpire"));
        row.put("examYear",rs.getString("examYear"));
        row.put("instruction",rs.getString("instruction"));
        row.put("published",rs.getBoolean("published"));
        row.put("publishDate",rs.getDate("publishDate"));
        dataList.add(row);
    }

    public List getDataList() {
        return dataList;
    }

    public String getExamListQuery(Long classId, Long subjectId, Long author){
        param = new HashMap();
        param.put("classId",classId);
        param.put("subjectId",subjectId);
        if(author != 1){
            param.put("author",author);
            return queryStr;
        }
        return queryStrWithoutAuthor;
    }

    public Map getParam() {
        return param;
    }
}
