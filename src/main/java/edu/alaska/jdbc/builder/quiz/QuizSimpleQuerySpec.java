package edu.alaska.jdbc.builder.quiz;

import edu.alaska.jdbc.builder.IJoinTable;
import edu.alaska.jdbc.builder.IQuerySpecification;
import edu.alaska.jdbc.builder.ITable;

import java.util.*;

/**
 * Created by sonle on 9/24/17.
 */
public class QuizSimpleQuerySpec implements IQuerySpecification {

    @Override
    public ITable getRootTable() {
        return new QuizSimpleTable();
    }

    @Override
    public List<IJoinTable> getCombineTables() {
        return null;
    }

    class QuizSimpleTable extends QuizTable {

        @Override
        public Map<String, String> getSelectedColumnAliasMap() {
            Map<String,String> map = new HashMap<>();
            map.put("id","id");
            map.put("question","question");
            map.put("answera","answerA");
            map.put("answerb","answerB");
            map.put("answerc","answerC");
            map.put("answerd","answerD");
            return map;
        }
        @Override
        public Set<String> getSelectedColumns() {
            return getSelectedColumnAliasMap().keySet();
        }

        @Override
        public Collection<String> getALiasColumns() {
            return getSelectedColumnAliasMap().values();
        }

    }
}

