package edu.alaska.jdbc.builder.quiz;

import edu.alaska.jdbc.builder.ITable;

/**
 * Created by sonle on 9/25/17.
 */
public abstract class QuizTable implements ITable {
    @Override
    public String getTableName() {
        return ITable.QUIZ_TABLE_NAME;
    }

    @Override
    public String getAliasTable() {
        return ITable.QUIZ_TABLE_ALIAS;
    }
}
