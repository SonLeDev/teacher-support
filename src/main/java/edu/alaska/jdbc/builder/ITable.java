package edu.alaska.jdbc.builder;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

/**
 * Created by sonle on 9/24/17.
 */
public interface ITable {

    String QUIZ_TABLE_NAME = "edu_quiz";
    String QUIZ_TABLE_ALIAS = "edq";

    String getTableName();
    String getAliasTable();

    /**
     *
     * @return <column_name - alias>
     */
    Map<String,String> getSelectedColumnAliasMap();

    Set<String> getSelectedColumns();

    Collection<String> getALiasColumns();

}
