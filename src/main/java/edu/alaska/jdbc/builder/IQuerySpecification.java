package edu.alaska.jdbc.builder;

import java.util.List;

/**
 * Created by sonle on 9/24/17.
 *
 *
 */
public interface IQuerySpecification {
    /**
     * Map : name rootTable - artifactName
     * @return
     */
    ITable getRootTable();
    List<IJoinTable> getCombineTables();


}
