package edu.alaska.jdbc.builder;

import org.springframework.jdbc.core.RowCallbackHandler;

import java.util.*;

/**
 * Created by sonle on 9/24/17.
 */
public abstract class QueryBuilder implements RowCallbackHandler{

    protected static final String BLANK_CHAR = " ";
    protected static final String AS_CHAR = " AS ";
    protected static final String ON_CHAR = " ON ";
    protected static final String DOT_CHAR = ".";
    protected static final String COMMA_CHAR = ",";
    protected static final String SELECT_CHAR = "SELECT ";
    protected static final String FROM_CHAR = " FROM ";
    protected static final String WHERE_CHAR = " WHERE ";
    protected static final String ORDER_BY_CHAR = " ORDER BY ";
    protected static final String LIMIT_CHAR = " LIMIT ";
    protected static final String GROUP_BY_CHAR = " GROUP BY ";


    protected IQuerySpecification querySpecification;
    protected ITable rootTable;
    protected List<IJoinTable> joinTables;
    protected StringBuilder sqlStmtBuilder = new StringBuilder();


    /**
     * Holding all the alias name that were selected in queryStmt
     * Using for extract the resultSet by name
     */
    protected Set<String> selectedFields = new HashSet<>();

    /**
     * catch the result from processRow of RowCallbackHandler
     */
    protected Collection<Map> resultList = new ArrayList<>();


    protected QueryBuilder(IQuerySpecification querySpecification){
        this.querySpecification = querySpecification;
        rootTable = querySpecification.getRootTable();
        joinTables = querySpecification.getCombineTables();
    }

    public String buildQuery(){

        this.buildSelectFields();
        this.buildFromTable();
        this.buildJoinTables();
        this.buildWhereCond();
        this.buildGroupBy();
        this.buildOrderBy();
        this.buildLimitResultRow();

        return sqlStmtBuilder.toString();
    }

    protected abstract void buildSelectFields();
    protected abstract void buildFromTable();
    protected abstract void buildJoinTables();
    protected abstract void buildWhereCond();
    protected abstract void buildGroupBy();
    protected abstract void buildOrderBy();
    protected abstract void buildLimitResultRow();

    public Collection getResultList(){
        return resultList;
    }
}
