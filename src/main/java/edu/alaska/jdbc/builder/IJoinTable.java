package edu.alaska.jdbc.builder;

/**
 * Created by sonle on 9/24/17.
 */
public interface IJoinTable {

    enum JoinType {
        INNER_JOIN("INNER JOIN"),
        LEFT_JOIN("LEFT JOIN"),
        RIGHT_JOIN("RIGHT JOIN")
        ;
        private String type;
        JoinType(String type) {
            this.type = type;
        }
        public String getName(){
            return type;
        }
    }

    JoinType getJoinType();

    ITable getTable();

    /**
     * prerequisite already know about the combined table and which column for combination first
     * ex : the combine table is test_quizs(tq), combined table is edu_quiz(eq)
     *      return (String) : eq.id = tq.quiz_id (...if more... AND ...)
     * @return
     */
    String getOnJoinCustomCond();

    /**
     * Default will get the given column then pair with rootTable's id
     * @return
     */
    String getOnJoinDefaultCond();

    //TODO : how to get params and where condition from combineTable
}
