package edu.alaska.jdbc.builder;

import org.apache.commons.collections4.CollectionUtils;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

/**
 * Created by sonle on 9/24/17.
 */
public class DefaultQueryRowHandler extends QueryBuilder{

    public DefaultQueryRowHandler(IQuerySpecification querySpecification) {
        super(querySpecification);
    }

    /**
     * input : rootTable,joinTables
     * output : SELECT statement, selectedFields
     */
    @Override
    protected void buildSelectFields() {
        StringBuilder selectBuilder = new StringBuilder();
        selectBuilder.append(SELECT_CHAR);

        // append select root table

        for(String colName : rootTable.getSelectedColumns()){
            String colAlias = rootTable.getSelectedColumnAliasMap().get(colName);
            selectedFields.add(colAlias);
            selectBuilder.append(rootTable.getAliasTable())
                    .append(DOT_CHAR).append(colName)
                    .append(AS_CHAR).append(colAlias)
                    .append(COMMA_CHAR);
        }

        // append select more from join tables
        if(CollectionUtils.isNotEmpty(joinTables)){
            for(IJoinTable combineTable : joinTables){
                ITable table = combineTable.getTable();
                for(String colName : table.getSelectedColumns()){
                    String colAlias = table.getSelectedColumnAliasMap().get(colName);
                    selectedFields.add(colAlias);
                    selectBuilder.append(table.getAliasTable())
                            .append(DOT_CHAR).append(colName)
                            .append(AS_CHAR).append(colAlias)
                            .append(COMMA_CHAR);
                }
            }
        }
        // remove that last comma
        if(selectBuilder.length()> 0) selectBuilder.setLength(selectBuilder.length()-1);
        this.sqlStmtBuilder.append(selectBuilder);

    }

    /**
     * input : rootTable
     * output : FROM statement
     */
    @Override
    protected void buildFromTable() {
        StringBuilder fromBuilder = new StringBuilder();
        fromBuilder.append(" FROM ")
                .append(rootTable.getTableName())
                .append(AS_CHAR)
                .append(rootTable.getAliasTable());
        this.sqlStmtBuilder.append(BLANK_CHAR).append(fromBuilder);
    }

    /**
     * input : joinTables
     * output : [INNER/LEFT/RIGHT] JOIN ... ON ... statement
     */
    @Override
    protected void buildJoinTables() {
        StringBuilder joinBuilder = new StringBuilder();
        if(CollectionUtils.isNotEmpty(joinTables)){
            for(IJoinTable joinTB : joinTables){
                joinBuilder.append(joinTB.getJoinType().getName())
                        .append(BLANK_CHAR).append(joinTB.getTable().getTableName())
                        .append(AS_CHAR).append(joinTB.getTable().getAliasTable())
                        .append(ON_CHAR).append(joinTB.getOnJoinDefaultCond());
            }
        }
        this.sqlStmtBuilder.append(BLANK_CHAR).append(joinBuilder);

    }

    /**
     * input : rootTable,joinTables
     * output : WHERE statement, Map<aliasColumnName,value> parameter base
     */
    @Override
    protected void buildWhereCond() {

    }


    @Override
    protected void buildGroupBy() {
        // depending on querySpecification
    }

    @Override
    protected void buildOrderBy() {
        // depending on querySpecification
    }

    @Override
    protected void buildLimitResultRow() {
        // depending on querySpecification
    }

    @Override
    public void processRow(ResultSet rs) throws SQLException {
        if(rs!=null){
            Map<String,Object> rowResultMap = new HashMap();
            resultList.add(rowResultMap);
            for(String field : selectedFields){
                Object value = rs.getObject(field);
                rowResultMap.put(field,value);

            }
        }
    }
}
