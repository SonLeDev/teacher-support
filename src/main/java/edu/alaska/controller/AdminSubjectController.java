package edu.alaska.controller;

import edu.alaska.common.Constances;
import edu.alaska.form.JsonResponse;
import edu.alaska.service.ExamService;
import edu.alaska.service.TestService;
import edu.alaska.service.UserService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by sonle on 8/10/17.
 */
@RestController
@RequestMapping("/subject")
public class AdminSubjectController {

    final static Logger LOGGER = Logger.getLogger(AdminSubjectController.class);

    @Autowired
    private ExamService examService;

    @Autowired
    private UserService userService;

    @Autowired
    private TestService testService;

    @GetMapping("/{subjectId}/exam/list")
    public JsonResponse getExamList(@PathVariable("subjectId") Long subjectId){
        LOGGER.info(String.format("%s getExamList::/subject/%d/exam/list",Constances.LOG_TAG_INFO,subjectId));
        return new JsonResponse(examService.getExamListBySubjectId(subjectId));
    }

    @GetMapping("/{subjectId}/exam/basic/list")
    public JsonResponse getExamBasicList(@PathVariable("subjectId") Long subjectId){
        LOGGER.info(String.format("%s getExamList::/subject/%d/exam/list",Constances.LOG_TAG_INFO,subjectId));
        return new JsonResponse(examService.getBasicExamListBySubjectId(subjectId));
    }

    @GetMapping("/{subjectId}/test/basic/list")
    public JsonResponse getTestBasicList(@PathVariable("subjectId") Long subjectId){
        LOGGER.info(String.format("%s getTestBasicList::/subject/%d/test/basic/list",Constances.LOG_TAG_INFO,subjectId));
        return new JsonResponse(testService.getBasicListBySubjectId(subjectId));
    }

    @GetMapping("/{subjectId}/user/account/basic/list")
    public JsonResponse getUserAccountBasicList(@PathVariable("subjectId") Long subjectId){
        Long classId = 12l;
        return new JsonResponse(userService.getBasicListBySubjectId(subjectId,classId));
    }


}
