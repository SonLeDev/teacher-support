package edu.alaska.controller;

import edu.alaska.form.JsonResponse;
import edu.alaska.service.ReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by sonle on 8/6/17.
 */
@RestController
@RequestMapping("/admin/report")
public class AdminReportController {

    @Autowired
    private ReportService reportService;

    @GetMapping("/userInputQuiz")
    public JsonResponse userInputQuiz(){
        return new JsonResponse(reportService.reportUserInputQuiz());
    }


}
