package edu.alaska.controller;

import edu.alaska.common.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by SonLee on 5/8/2017.
 */
@RestController
public class EduClientController {

//    public static final String HOST_AWS = "http://ec2-34-208-25-53.us-west-2.compute.amazonaws.com:8080";
    public static final String HOST_AWS = "http://localhost:8080";

    @Autowired
    RestTemplate restTemplate;

    @RequestMapping("/")
    public ResponseEntity eduCtl(HttpServletRequest request){

        ResponseEntity<String> responseEntity = new ResponseEntity<>("You are not permission on this site",
                HttpStatus.OK);
        return responseEntity;
    }

    @RequestMapping("/edu/registration")
    public ResponseEntity eduRegistration(){
        RestTemplate restTemplate = new RestTemplate();
        String uri = HOST_AWS + "/login/registration";
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        Map<String,String> data = new HashMap<>();
        data.put("userName","SonLe");
        data.put("className","12");
        data.put("deviceId","123abc");

        HttpEntity entity = new HttpEntity<Map>(data, headers);
        ResponseEntity<Map> personEntity = restTemplate.exchange(uri, HttpMethod.POST, entity, Map.class);
        return personEntity;
    }

    @RequestMapping("/edu/loginFb")
    public ResponseEntity loginFb(){
        RestTemplate restTemplate = new RestTemplate();
        String uri = HOST_AWS + "/login/facebookLogin";
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        Map<String,String> data = new HashMap<>();
        data.put("fbToken","abcdadfsasdf");
        data.put("className","12");
        data.put("deviceId","123abc");

        HttpEntity entity = new HttpEntity<Map>(data, headers);
        ResponseEntity<Map> personEntity = restTemplate.exchange(uri, HttpMethod.POST, entity, Map.class);
        return personEntity;
    }

    @RequestMapping("/edu/content")
    public ResponseEntity eduContent(){
        RestTemplate restTemplate = new RestTemplate();
        String uri = HOST_AWS + "/content/class/12/subjectList";
        HttpHeaders headers = Utils.creatingHeader("ASQLWEUJOX3EKJQUTL");
        Map<String,String> data = new HashMap<>();
        HttpEntity entity = new HttpEntity<Map>(data, headers);
        ResponseEntity<List> personEntity = restTemplate.exchange(uri, HttpMethod.GET, entity, List.class);
        return personEntity;
    }



    @RequestMapping("/edu/content/class/{classId}")
    public ResponseEntity eduContentByClass(@PathVariable("classId") Long classId){
        RestTemplate restTemplate = new RestTemplate();
        String uri = HOST_AWS + "/content/class/"+classId+"/subjectList";
        HttpHeaders headers = Utils.creatingHeader("ASQLWEUJOX3EKJQUTL");
        Map<String,String> data = new HashMap<>();
        HttpEntity entity = new HttpEntity<Map>(data, headers);
        ResponseEntity<List> personEntity = restTemplate.exchange(uri, HttpMethod.GET, entity, List.class);
        return personEntity;
    }
    @RequestMapping("/edu/content/class/{classId}/subjectChapters")
    public ResponseEntity eduSubjectChaptersByClass(@PathVariable("classId") Long classId){
        RestTemplate restTemplate = new RestTemplate();
            String uri = HOST_AWS + "/content/class/"+classId+"/subjectChapters";
        HttpHeaders headers = Utils.creatingHeader("ASQLWEUJOX3EKJQUTL");
        Map<String,String> data = new HashMap<>();
        HttpEntity entity = new HttpEntity<Map>(data, headers);
        ResponseEntity<List> personEntity = restTemplate.exchange(uri, HttpMethod.GET, entity, List.class);
        return personEntity;
    }

    @RequestMapping("/edu/content/math/chapter/{chapterNo}")
    public ResponseEntity getChapterNo(@PathVariable("chapterNo") Long chapterNo){
        RestTemplate restTemplate = new RestTemplate();

        String uri = HOST_AWS + "/content/class/12/subject/1/chapter/"+chapterNo;
        HttpHeaders headers = Utils.creatingHeader("ASQLWEUJOX3EKJQUTL");
        Map<String,String> data = new HashMap<>();
        HttpEntity entity = new HttpEntity<Map>(data, headers);
        ResponseEntity<List> personEntity = restTemplate.exchange(uri, HttpMethod.GET, entity, List.class);
        return personEntity;
    }

    @RequestMapping("/edu/content/examList")
    public ResponseEntity showExamList(){
        RestTemplate restTemplate = new RestTemplate();
        String uri = HOST_AWS + "/content/class/12/subject/1/examList";
        HttpHeaders headers = Utils.creatingHeader("ASQLWEUJOX3EKJQUTL");
        Map<String,String> data = new HashMap<>();
        HttpEntity entity = new HttpEntity<Map>(data, headers);
        ResponseEntity<List> personEntity = restTemplate.exchange(uri, HttpMethod.GET, entity, List.class);
        return personEntity;
    }

    @RequestMapping("/edu/student/takeExam")
    public ResponseEntity studentTakeExamination(){
        RestTemplate restTemplate = new RestTemplate();
        String uri = HOST_AWS + "/student/exam/1";
        HttpHeaders headers = Utils.creatingHeader("E8H5K6S27Z5Q30TIR1","3");
        Map<String,String> data = new HashMap<>();
        HttpEntity entity = new HttpEntity<Map>(data, headers);
        ResponseEntity<Map> personEntity = restTemplate.exchange(uri, HttpMethod.GET, entity, Map.class);
        return personEntity;
    }

}
