package edu.alaska.controller;

import edu.alaska.form.JsonResponse;
import edu.alaska.service.ChapterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by sonle on 10/16/17.
 */
@RestController
@RequestMapping("/unit")
public class AdminUnitChapterController {

    @Autowired
    ChapterService chapterService;

    ///unit/chapter/unitList/{subjectId}
    @GetMapping("/chapter/unitList/{subjectId}")
    public JsonResponse getUnitList(@PathVariable("subjectId") Long subjectId){
        return new JsonResponse(chapterService.getUnitList(subjectId));
    }

    @GetMapping("/names/{subjectId}")
    public JsonResponse getUnitNames(@PathVariable("subjectId") Long subjectId){
        return new JsonResponse(chapterService.getUnitNames(subjectId));
    }
    @GetMapping("/selected/list/{subjectId}")
    public JsonResponse getUnitSelectedList(@PathVariable("subjectId") Long subjectId){
        return new JsonResponse(chapterService.getUnitSelectedList(subjectId));
    }
}
