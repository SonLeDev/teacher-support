package edu.alaska.controller;

import edu.alaska.domain.EduChapters;
import edu.alaska.domain.EduUnit;
import edu.alaska.form.JsonResponse;
import edu.alaska.service.ChapterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * Created by sonle on 7/12/17.
 */

@RestController
@RequestMapping("/chapter")
public class AdminChapterController {

    @Autowired
    private ChapterService chapterService;

    @GetMapping("/unit/subject/{subjectId}")
    public JsonResponse getChapterUnitBySubject(@PathVariable(value = "subjectId") Long subjectId) {
        return new JsonResponse(chapterService.findChapterUnit(subjectId));
    }

    @PostMapping("/list")
    public JsonResponse getChapterList(@RequestParam(value="subjectId", required=false) Long subjectId,
                                    @RequestParam(value="classId", required=false) Long classId) {
        List<EduChapters> chapterList = chapterService.getChapterList(classId,subjectId);
        return new JsonResponse(chapterList);
    }



    @GetMapping("/{chapterId}/units")
    public JsonResponse getUnitsByChapterId(@PathVariable("chapterId") Long chapterId){
        List<EduUnit> unitLst = chapterService.getUnitByChapterId(chapterId);
        return new JsonResponse(unitLst);
    }

    @PostMapping("/saveChapterAndUpload")
    public JsonResponse saveChapterAndUpload(@RequestParam(value = "chapter") String chapterJson,
                                          @RequestPart(value = "iconFile", required=false) MultipartFile iconFile){

        EduChapters chapter = chapterService.saveChapterAndUploadImage(chapterJson, iconFile);

        return new JsonResponse(chapter);
    }
    @PostMapping("/saveUnitAndUpload")
    public JsonResponse saveUnitAndUpload(@RequestParam(value = "unit") String unitJson,
                                          @RequestPart(value = "iconFile", required=false) MultipartFile iconFile){

        EduUnit unit = chapterService.saveUnitAndUpload(unitJson, iconFile);

        return new JsonResponse(unit);
    }
}
