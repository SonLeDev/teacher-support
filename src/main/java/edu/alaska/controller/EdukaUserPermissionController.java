package edu.alaska.controller;

import edu.alaska.form.JsonResponse;
import edu.alaska.service.PermissionService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by sonle on 11/24/17.
 */
@RestController
@RequestMapping("/permission")
public class EdukaUserPermissionController {

    final static Logger LOGGER = Logger.getLogger(EdukaUserPermissionController.class);

    @Autowired
    PermissionService permissionService;

    @GetMapping("/user/{userId}")
    public JsonResponse getPermissionUser(@PathVariable(value = "userId") Long userId) {
        LOGGER.info(String.format("request /permission/user/%d",userId));
        return new JsonResponse(permissionService.getUserPermission(userId));
    }

    @GetMapping("/router/user/{userId}")
    public JsonResponse getRouterByUserId(@PathVariable(value = "userId") Long userId) {
        LOGGER.info(String.format("request /permission/router/user/%d",userId));
        return new JsonResponse(permissionService.getRouters(userId));
    }
}
