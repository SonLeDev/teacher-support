package edu.alaska.controller;

import edu.alaska.domain.User;
import edu.alaska.service.QuizService;
import edu.alaska.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

/**
 * Created by sonle on 8/16/17.
 */
@Controller
@RequestMapping("/view")
public class QuizController {

    @Autowired
    private QuizService quizService;

    @Autowired
    private UserService userService;

    @RequestMapping("quiz/subject/{subjectId}/user/{userName}")
    public String quiz(@PathVariable("subjectId") Long subjectId,
                       @PathVariable("userName") String userName,
                       Model model) {

        User user = userService.getUserByUserName(userName);

        assert user!=null;

        List quizList = quizService.getQuizListBySubjectIdAndUserid(subjectId,user.getId());
        model.addAttribute("quizList",quizList);
        model.addAttribute("message", "When $a \\ne 0$, there are two solutions to \\(ax^2 + bx + c = 0\\) and they are\n" +
                "$$x = {-b \\pm \\sqrt{b^2-4ac} \\over 2a}.$$");
        return "quiz/view";
    }

}
