package edu.alaska.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import edu.alaska.common.Constances;
import edu.alaska.form.ReqFilterForm;
import edu.alaska.domain.EduQuiz;
import edu.alaska.domain.EduSubject;
import edu.alaska.domain.EduUnit;
import edu.alaska.form.JsonResponse;
import edu.alaska.form.QuizForm;
import edu.alaska.repository.EduQuizRepository;
import edu.alaska.repository.EduSubjectRepository;
import edu.alaska.repository.EduUnitRepository;
import edu.alaska.service.ChapterService;
import edu.alaska.service.ExamService;
import edu.alaska.service.QuizService;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 * Created by SonLee on 5/26/2017.
 */
@RestController
@RequestMapping("/quiz")
public class AdminQuizController {
    final static Logger LOGGER = Logger.getLogger(AdminQuizController.class);


    @Autowired
    private EduQuizRepository eduQuizRepository;

    @Autowired
    private EduSubjectRepository eduSubjectRepository;
    
    @Autowired
    private EduUnitRepository eduUnitRepository;
    
    @Autowired
    private QuizService quizService;


    @Autowired
    ChapterService chapterService;

    @Autowired
    ExamService examService;

    @GetMapping("/chaptersGroupBySubjectId")
    public JsonResponse getChaptersBySubjectId(@RequestParam(value="subjectId", required=true) Long subjectId) {
        return new JsonResponse(chapterService.findChapterUnit(subjectId));
    }

    /**
     *
     * @param quizObjJson
     * @param imgQuestFile : mapping to questImg, index 0
     * @param imgAnswerDetailFile : mapping to dtailAnsImg, index 1
     * @param request
     * @return
     */
    @PostMapping("/saveQuizAndUpload")
    public JsonResponse saveQuizAndUpload(@RequestParam(value = "quizObjJson") String quizObjJson,
                                          @RequestPart(value = "imgQuestFile", required=false) MultipartFile imgQuestFile,
                                          @RequestPart(value = "imgAnswerDetailFile", required=false) MultipartFile imgAnswerDetailFile,
                                          HttpServletRequest request){

    	MultipartFile multipartFile[] = {imgQuestFile, imgAnswerDetailFile};
        EduQuiz eduQuiz = quizService.saveAndUpload(quizObjJson, multipartFile);

        return new JsonResponse(eduQuiz);
    }

    /**
     * save quiz with header APPLICATION_FORM_URLENCODED_VALUE
     * using for save VietNamese
     * @param quizForm
     * @return
     */
    @PostMapping(value = "/save",
            consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public JsonResponse saveQuizForm(QuizForm quizForm){

        EduQuiz eduQuiz = quizService.saveQuizForm(quizForm);

        return new JsonResponse(eduQuiz);
    }

    // /quiz/save
    @Deprecated
    @PostMapping("/save")
    public JsonResponse saveQuiz(HttpServletRequest request
                , @RequestBody QuizForm quizForm){

        EduQuiz eduQuiz = quizService.saveQuizForm(quizForm);

        return new JsonResponse(eduQuiz);
    }

    // /quiz/delete
    // using promisePost - x-www-form-urlencoded
    @PostMapping(value = "/delete")
    public JsonResponse deleteQuiz(@RequestParam("quizId") Long quizId){
        LOGGER.info(String.format(Constances.LOG_TAG_INFO+"deleteQuiz::/quiz/delele::quizId:%d"
                ,quizId));
        return new JsonResponse(
                quizService.deleteQuiz(quizId));
    }

    // /quiz/publish
    // using promisePost - x-www-form-urlencoded
    @PostMapping(value = "/publish")
    public JsonResponse publishQuiz(@RequestParam("quizId") Long quizId){
        LOGGER.info(String.format(Constances.LOG_TAG_INFO+"publishQuiz::/quiz/publish::quizId:%d"
                ,quizId));
        return new JsonResponse(
                quizService.publishQuiz(quizId));
    }
    @PostMapping(value = "/unPublish")
    public JsonResponse unPublishQuiz(@RequestParam("quizId") Long quizId){
        LOGGER.info(String.format(Constances.LOG_TAG_INFO+"unPublishQuiz::/quiz/unPublish::quizId:%d"
                ,quizId));
        return new JsonResponse(
                quizService.unPublishQuiz(quizId));
    }

    @GetMapping("/of/exam/{examId}")
    public JsonResponse getQuizLstByExamId(@PathVariable("examId") Long exampId){
         List<EduQuiz> quizLst = eduQuizRepository.selectQuizOfExamId(exampId);
         return new JsonResponse(quizLst);
    }

    @GetMapping("/chapter")
    public JsonResponse chapter(@RequestParam(value="subject", required=false) String subject
    		, @RequestParam(value="className", required=false) String className) {
    	List<EduUnit> eduUnits = new ArrayList<EduUnit>();
        if (!StringUtils.isEmpty(className) && !StringUtils.isEmpty(subject)) {
        	List<EduSubject> eduSubjects = eduSubjectRepository.getEduSubject(String.valueOf(className), subject);
        	if (CollectionUtils.isNotEmpty(eduSubjects)) {
        		eduUnits = eduUnitRepository.findBySubjectId(eduSubjects.get(0).getId());
        	}
        }
        return new JsonResponse(eduUnits);
    }

    @Deprecated // using chapters insteand of chapter
    @GetMapping("/chapterBySubjectId")
    public JsonResponse getChapterBySubjectId(@RequestParam(value="subjectId", required=false) Long subjectId) {
        List<EduUnit> eduUnits = new ArrayList<EduUnit>();
                eduUnits = eduUnitRepository.findBySubjectId(subjectId);
        return new JsonResponse(eduUnits);
    }


    
       
    @PostMapping("/list")
    public JsonResponse getQuizList(@RequestParam(value="subjectId", required=false) Long subjectId,
            @RequestParam(value="userId", required=false) Long userId) {
    	List<EduQuiz> eduQuizs = quizService.getQuizByUserIdAndSubjectId(userId,subjectId);

        return new JsonResponse(eduQuizs);
    }

    //quiz/list/filter
    @PostMapping("/list/filter")
    public JsonResponse filterQuizList(@RequestBody ReqFilterForm filterForm){

        return new JsonResponse(quizService.filterQuizs(filterForm));
    }


    ///quiz/list/subject/{subjectId}/user/{userId}
    @GetMapping("/list/subject/{subjectId}/user/{userId}")
    public JsonResponse getQuizListByUserId(@PathVariable("userId") Long userId,
                                            @PathVariable("subjectId") Long subjectId){
        List<EduQuiz> quizList= quizService.getQuizListByUserId(userId,subjectId);
        return new JsonResponse(quizList);
    }

    //quiz/list/exam/{examId}
    @GetMapping("/list/exam/{examId}")
    public JsonResponse getQuizListByExamId(@PathVariable("examId") Long examId){
        return new JsonResponse(quizService.getQuizListByExamId(examId));
    }

    //quiz/list/by/updateDate
    @PostMapping("/quiz/list/by/updateDate")
    public JsonResponse getQuizListByUpdateDate(@RequestParam(value = "updateDate") Date updateDate){
        return new JsonResponse(quizService.getQuizListByUpdateDate(updateDate));
    }

    //quiz/list/test/{testId}
    @GetMapping("/list/test/{testId}")
    public JsonResponse getQuizListByTestId(@PathVariable("testId") Long testId){
        return new JsonResponse(quizService.getQuizListByTestId(testId));
    }

    @GetMapping("/getSimpleQuizs")
    public JsonResponse getSimpleQuizs(){
        return new JsonResponse(quizService.getSimpleQuizs());
    }

    // /quiz/{quizId}
    @GetMapping("/{quizId}")
    public JsonResponse getQuizById(@PathVariable("quizId") Long quizId){
        return new JsonResponse(quizService.getQuizById(quizId));

    }
    // /quiz/report/{quizId}
    @GetMapping("/report/{quizId}")
    public JsonResponse reportQuiz(@PathVariable("quizId") Long quizId){
        return new JsonResponse(quizService.reportQuiz(quizId));
    }

}
