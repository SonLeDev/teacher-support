package edu.alaska.controller;


import edu.alaska.builder.EduPaperBuilder;
import edu.alaska.common.Constances;
import edu.alaska.domain.TestPaper;
import edu.alaska.form.JsonResponse;
import edu.alaska.form.RequestKeyForm;
import edu.alaska.form.RequestTestForm;
import edu.alaska.repository.TestPapperRepository;
import edu.alaska.service.TestService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/test")
public class AdminTestController {

    final static Logger LOGGER = Logger.getLogger(AdminTestController.class);

    @Autowired
    TestPapperRepository testPapperRepository;

    @Autowired
    TestService testService;

    @GetMapping("/{testId}")
    public JsonResponse getTestPaper(@PathVariable("testId") Long testId){
        return new JsonResponse(testService.getTestPaper(testId));
    }

    @GetMapping("/{testId}/full")
    public JsonResponse getTestPaperFull(@PathVariable("testId") Long testId){
        return new JsonResponse(testService.getTestFull(testId));
    }

    @GetMapping("/{testId}/maxNoOfQuiz")
    public JsonResponse getMaxNoOfQuiz(@PathVariable("testId") Long testId){
        return new JsonResponse(testService.getMaxNoOfQuiz(testId));
    }

    /**
     * /test/save
     * @param requestTestForm
     * @return
     */
    @PostMapping(value = "/save",
            consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public JsonResponse saveTesting(RequestTestForm requestTestForm){

        LOGGER.info(Constances.LOG_TAG_INFO+"/test/save::saveTesting");
        EduPaperBuilder paperBuilder = EduPaperBuilder.instance();
        TestPaper testPapper = null;
        try{
            TestPaper testPaper = null;
            if(requestTestForm.getId()!=null){
                testPapperRepository.findOne(requestTestForm.getId());
            }else{
                testPaper = new TestPaper();
            }
            testPapper = testPapperRepository.save(paperBuilder.buildTestPaper(requestTestForm,testPaper));
            LOGGER.info(String.format(Constances.LOG_TAG_INFO+"saveTesting::testId=%d",testPapper.getId()));

        }catch (Exception e){
            LOGGER.error(String.format(Constances.LOG_TAG_ERROR+"saveTesting"));
        }
        return new JsonResponse(testPapper);
    }

    @GetMapping("/list")
    public JsonResponse list(){
        return new JsonResponse(testPapperRepository.findAll());
    }

    @GetMapping("/list/by/subject/{subjectId}")
    public JsonResponse listExam(@PathVariable("subjectId") Long subjectId){
        return new JsonResponse(testPapperRepository.findBySubjectId(subjectId));
    }

    /**
     *
     * request list test by subjectId, userId and order by mdfDate
     * @return
     */
    @PostMapping(value = "/list/by/user",
                consumes = MediaType.APPLICATION_JSON_VALUE,
                produces = MediaType.APPLICATION_JSON_VALUE)
    public JsonResponse listTesting(@RequestBody RequestKeyForm requestExamForm){
        LOGGER.info(String.format(Constances.LOG_TAG_INFO+"listTesting::/test/list/by/user::classId:%d, subjectId:%d, author:%d"
                ,requestExamForm.getClassId()
                ,requestExamForm.getSubjectId()
                ,requestExamForm.getAuthor()));
        return new JsonResponse(
                testService.getListTestByAuthor(requestExamForm.getClassId()
                        ,requestExamForm.getSubjectId()
                        ,requestExamForm.getAuthor()));

    }
    // test/publish
    // header x-www-form-urlencoded
    @PostMapping(value = "/publish")
    public JsonResponse publishTesting(@RequestParam("testId") Long testId){
        LOGGER.info(String.format(Constances.LOG_TAG_INFO+"publishTesting::/test/publish::testId:%d"
                ,testId));
        return new JsonResponse(
                testService.publish(testId));
    }

    // /test/delete
    @PostMapping(value = "/delete")
    public JsonResponse deleteTest(@RequestParam("testId") Long testId){
        LOGGER.info(String.format("deleteTest::/test/delele::examId:%d",testId));
        return new JsonResponse(
                testService.delete(testId));
    }
}
