package edu.alaska.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import edu.alaska.security.Permission;
import edu.alaska.security.PermitBuilder;
import edu.alaska.builder.SessionBuilder;
import edu.alaska.form.JsonResponse;
import edu.alaska.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import edu.alaska.domain.User;

/**
 * Created by SonLee on 5/18/2017.
 */
@RestController
@RequestMapping("/admin")
public class AdminUserAccountController {
	@Autowired
	 private UserService userService;

    @RequestMapping("/user/list.html")
    public String examListTemplate(HttpServletRequest request) {
    	System.out.println(request.getParameterNames());
        return "partials/user/userList";
    }
    
    @RequestMapping("/user/multiselect.html")
    public String multiSelect() {
        return "partials/user/multiselect.tmpl";
    }

    // "/admin/user/checkLogin"
    @PostMapping("/user/checkLogin")
    public JsonResponse isLogin(@RequestParam(value = "userName") String userName
    		, @RequestParam(value = "password") String password
    		, HttpServletRequest request) {
    	/*SessionBuilder sessionBuilder = new SessionBuilder();
    	User user = new User();
    	if (!StringUtils.isEmpty(userName) && !StringUtils.isEmpty(password)) {
    	    user = userService.validLogin(userName, password);
    		if (user != null) {
    			sessionBuilder = userService.buildSession(user);
    			userService.getChapterBySubjectId(sessionBuilder, Long.valueOf(user.getSubjectId()));
    			userService.getUnitBySubjectId(sessionBuilder, Long.valueOf(user.getSubjectId()));
    			HttpSession session = request.getSession();
    			session.setAttribute("sessionBuilder", sessionBuilder);
    		}
    	}*/
		User user = userService.validLogin(userName, password);

		if(user!=null){
            user.setPassword("");
            if(user.getPermission().equals(Permission.MATH.getName())){
                user.setPermit(PermitBuilder.getInstance().permitMath());
            }else if(user.getPermission().equals(Permission.PHYSIC.getName())){
                user.setPermit(PermitBuilder.getInstance().permitPhysic());
            }else if(user.getPermission().equals(Permission.CHEMISTRY.getName())){
                user.setPermit(PermitBuilder.getInstance().permitChemistry());
            }else if(user.getPermission().equals(Permission.ENGLISH.getName())){
                user.setPermit(PermitBuilder.getInstance().permitEnglish());
            }
            else if(user.getPermission().equals(Permission.ALL.getName())){
                user.setPermit(PermitBuilder.getInstance().permitAll());
            }
			return new JsonResponse(user);
		}
    	return new JsonResponse("KO");
    }
    
    @PostMapping("/user/shareData")
    public JsonResponse shareData(HttpServletRequest request) {
    	HttpSession session = request.getSession();  
    	SessionBuilder sessionBuilder = (SessionBuilder) session.getAttribute("sessionBuilder");
    	return new JsonResponse(sessionBuilder);
    }
}
