package edu.alaska.controller;

import edu.alaska.form.JsonResponse;
import edu.alaska.service.SysService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/sys")
public class SysController {

    @Autowired
    SysService sysService;

    @PostMapping("/generateId")
    public JsonResponse generateId(HttpServletRequest request,
                                   @RequestParam(value = "domain") String domain){

        Long generatedId = sysService.generateId(domain);
        return new JsonResponse(generatedId);
    }
}
