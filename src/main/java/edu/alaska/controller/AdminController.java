package edu.alaska.controller;

import edu.alaska.common.AWSUtil;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Map;

/**
 * Created by SonLee on 5/18/2017.
 */
@Controller
@RequestMapping("/admin")
public class AdminController {

    private static final String HOA = "hoa";
	private static final String LY = "ly";
	private static final String TOAN = "toan";
	@Value("${welcome.message:test}")
    private String message = "Hello World";

    @RequestMapping("")
    public String login(Map<String, Object> model) {
        return "index";
    }

    @RequestMapping("/wiris/{subjectId}")
    public String wiris(@PathVariable(value = "subjectId",required = false) Long subjectId,Map<String, Object> model) {
        model.put("message", this.message);
        model.put("isChemistry", Boolean.FALSE);
        if(subjectId!=null && subjectId == 3l){
            model.put("isChemistry", Boolean.TRUE);
        }
        return "wirisEditor";
    }

    @RequestMapping("/toan")
    public String index(Map<String, Object> model) {
        model.put("subject", TOAN);
        return "index";
    }
    
    @RequestMapping("/ly")
    public String ly(Map<String, Object> model) {
        model.put("subject", LY);
        return "index";
    }
    
    @RequestMapping("/hoa")
    public String hoa(Map<String, Object> model) {
        model.put("subject", HOA);
        return "index";
    }

    @RequestMapping("/exam/list/template.html")
    public String examListTemplate() {
        return "partials/exam/listTmp";
    }

    @RequestMapping("/exam/new/template.html")
    public String newExamTemplate() {
        return "partials/exam/newTmp";
    }
    
    @RequestMapping("/quiz/new/template.html")
    public String newQuizTemplate() {
        return "partials/quiz/newTmp";
    }

    @RequestMapping("/quiz/list.html")
    public String getListQuiz() {
        return "partials/quiz/listQuiz";
    }


    @RequestMapping("/input/drawInput")
    public String drawInput(Map<String, Object> model) {
        return "myscriptMathWeb";
    }


    @RequestMapping(value = "/uploadImage", method = RequestMethod.POST)
    public String uploadImage(@RequestParam("file") MultipartFile file,
                              HttpServletRequest request){
        if (!file.isEmpty()) {
            try {
                AWSUtil.uploadImageToAWSS3(file);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return "view page";
    }
    
    @RequestMapping("/partialmodal")
    public String partialmodal() {
        return "partialmodal";
    }
}
