package edu.alaska.controller;

import edu.alaska.form.JsonResponse;
import edu.alaska.service.AdminManageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by sonle on 8/6/17.
 */
@RestController
@RequestMapping("/admin/manage")
public class AdminManageController {

    @Autowired
    private AdminManageService adminManageService;

    @GetMapping("/quiz")
    public JsonResponse manageQuiz(){
        return new JsonResponse(adminManageService.showAllQuiz());
    }


}
