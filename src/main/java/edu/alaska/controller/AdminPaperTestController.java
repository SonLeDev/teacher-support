package edu.alaska.controller;

import edu.alaska.form.JsonResponse;
import edu.alaska.form.PaperTestForm;
import edu.alaska.service.PaperTestService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * Created by SonLee on 5/26/2017.
 */
@RestController
@RequestMapping("/testing")
public class AdminPaperTestController {
    final static Logger LOGGER = Logger.getLogger(AdminPaperTestController.class);

    @Autowired
    private PaperTestService paperTestService;

    @PostMapping("/list")
    public JsonResponse list(@RequestParam(value="authorId", required=true)Long authorId){
        return new JsonResponse(paperTestService.findByAuthorId(authorId));
    }

    @PostMapping("/save")
    public JsonResponse save(@RequestBody PaperTestForm paperTestForm){
        return new JsonResponse(paperTestService.save(paperTestForm));
    }
    @PostMapping("/attachQuizsToExam")
    public JsonResponse attachQuizsToExam(@RequestBody PaperTestForm paperTestForm){
        return new JsonResponse(paperTestService.attachQuizsToExam(paperTestForm));
    }

}
