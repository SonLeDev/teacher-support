package edu.alaska.controller;


import edu.alaska.builder.EduPaperBuilder;
import edu.alaska.common.Constances;
import edu.alaska.domain.ExaminationPaper;
import edu.alaska.form.JsonResponse;
import edu.alaska.form.RequestExamForm;
import edu.alaska.form.RequestKeyForm;
import edu.alaska.repository.ExaminationPapperRepository;
import edu.alaska.service.ExamService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import java.util.concurrent.atomic.AtomicLong;

@RestController
@RequestMapping("/exam")
public class AdminExamController {

    final static Logger LOGGER = Logger.getLogger(AdminExamController.class);

    private static final String template = "Hello, %s!";
    private final AtomicLong counter = new AtomicLong();

    @Autowired
    ExaminationPapperRepository examinationPapperRepository;

    @Autowired
    ExamService examService;

    // exam/{examId}
    @GetMapping("/{examId}")
    public JsonResponse getExam(@PathVariable("examId") Long examId){
        return new JsonResponse(examService.getExam(examId));
    }
    // exam/{examId}/full
    @GetMapping("/{examId}/full")
    public JsonResponse getExamFull(@PathVariable("examId") Long examId){
        return new JsonResponse(examService.getExamFull(examId));
    }

    @GetMapping("/{examId}/maxNoOfQuiz")
    public JsonResponse getMaxNoOfQuiz(@PathVariable("examId") Long examId){
        return new JsonResponse(examService.getMaxNoOfQuiz(examId));
    }

    /**
     * /exam/save
     * @param requestExamForm
     * @return
     */
    @PostMapping(value = "/save",
            consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public JsonResponse saveExam(RequestExamForm requestExamForm){

        LOGGER.info(Constances.LOG_TAG_INFO+"/exam/save::saveExam");
        EduPaperBuilder paperBuilder = EduPaperBuilder.instance();
        ExaminationPaper examinationPapper = null;
        try{
            ExaminationPaper examinationPaper = null;
            if(requestExamForm.getId()!=null && requestExamForm.getId()!=0){
                examinationPaper = examinationPapperRepository.findOne(requestExamForm.getId());
            }else if(requestExamForm.getExamId()!=null && requestExamForm.getExamId()!=0){
                examinationPaper = examinationPapperRepository.findOne(requestExamForm.getExamId());
            }else{
                examinationPaper = new ExaminationPaper();
            }
            examinationPapper = examinationPapperRepository.save(paperBuilder.buildExamPaper(requestExamForm,examinationPaper));
            LOGGER.info(String.format(Constances.LOG_TAG_INFO+"saveExam::examId=%d",examinationPapper.getId()));

        }catch (Exception e){
            LOGGER.error(String.format(Constances.LOG_TAG_ERROR+"saveExam"));
        }
        return new JsonResponse(examinationPapper);
    }



    @GetMapping("/list")
    public JsonResponse list(){
        return new JsonResponse(examinationPapperRepository.findAll());
    }

    @GetMapping("/list/by/subject/{subjectId}")
    public JsonResponse listExam(@PathVariable("subjectId") Long subjectId){
        return new JsonResponse(examinationPapperRepository.findBySubjectId(subjectId));
    }

    /**
     * exam/list/by/user
     * request list exam by subjectId, userId and order by mdfDate
     * @return
     */
    @PostMapping(value = "/list/by/user",
                consumes = MediaType.APPLICATION_JSON_VALUE,
                produces = MediaType.APPLICATION_JSON_VALUE)
    public JsonResponse listExams(@RequestBody RequestKeyForm requestExamForm){
        LOGGER.info(String.format(Constances.LOG_TAG_INFO+"listExams::/exam/list/by/user::classId:%d, subjectId:%d, author:%d"
                ,requestExamForm.getClassId()
                ,requestExamForm.getSubjectId()
                ,requestExamForm.getAuthor()));
        return new JsonResponse(
                examService.getListExamByAuthor(requestExamForm.getClassId()
                        ,requestExamForm.getSubjectId()
                        ,requestExamForm.getAuthor()));

    }

    @PostMapping(value = "/publish")
    public JsonResponse publishExam(@RequestParam("examId") Long examId){
        LOGGER.info(String.format(Constances.LOG_TAG_INFO+"publishExam::/exam/publish::examId:%d"
                ,examId));
        return new JsonResponse(
                examService.publish(examId));
    }

    // /exam/delete
    @PostMapping(value = "/delete")
    public JsonResponse deleteExam(@RequestParam("examId") Long examId){
        LOGGER.info(String.format(Constances.LOG_TAG_INFO+"deleteExam::/exam/delele::examId:%d"
                ,examId));
        return new JsonResponse(
                examService.delete(examId));
    }
}
