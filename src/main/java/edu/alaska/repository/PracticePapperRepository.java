package edu.alaska.repository;


import edu.alaska.domain.PracticePapper;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by sstvn on 5/2/17.
 */
@Deprecated
public interface PracticePapperRepository extends JpaRepository<PracticePapper,Long> {
    //Collection<PracticePapper> findByClassIdAndSubjectId();
}
