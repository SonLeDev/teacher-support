package edu.alaska.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import edu.alaska.domain.EduUnit;

/**
 * Created by sstvn on 5/1/17.
 */
public interface EduUnitRepository extends JpaRepository<EduUnit,Long> {

	List<EduUnit> findBySubjectId(Long subjectId);

    List<EduUnit> findByChapterId(Long chapterId);

    @Query(value = "SELECT eu.unitName FROM EduUnit eu WHERE eu.subjectId=?1")
    List<String> findUnitNames(Long subjectId);
}
