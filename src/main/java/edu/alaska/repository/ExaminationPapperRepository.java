package edu.alaska.repository;


import edu.alaska.domain.ExaminationPaper;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Collection;
import java.util.Map;

/**
 * Created by sstvn on 5/2/17.
 */
public interface ExaminationPapperRepository extends JpaRepository<ExaminationPaper,Long> {

    Collection<ExaminationPaper> findByClassIdAndSubjectId(Long classId, Long subjectId);

    Collection<ExaminationPaper> findBySubjectId(Long subjectId);

    Collection<ExaminationPaper> findByClassIdAndSubjectIdAndAuthorOrderByMdfDate(Long classId, Long subjectId, Long author);

    @Query(value = "SELECT ep.id AS id,ep.examName AS name FROM ExaminationPaper AS ep WHERE ep.subjectId=:subjectId")
    Collection<Map> findBasicBySubjectId(@Param("subjectId") Long subjectId);

    @Query(value = "SELECT * FROM (select expp.id as examId, expp.author as author, expp.exam_code as examCode, " +
            "expp.exam_name as examName, expp.num_of_quiz as numOfQuest, expp.time_expire as duration " +
            ", expp.subject_id as subjectId " +
            ",IF(expb.published is null,0,expb.published) published " +
            ",etg.name as tagName, etg.published as tagPublished " +
            ",SUM(IF(exq.published,1,0)) numQuestPublised,SUM(IF(!exq.published,1,0)) numQuestUnPublised " +
            "From examination_paper expp " +
            "left join examination_quizs exq on expp.id = exq.exam_id " +
            "left join examination_publish expb on expb.exam_id = expp.id " +
            "left join examination_tags extg on expp.id = extg.exam_id " +
            "left join edu_tags etg on etg.id = extg.tag_id " +
            "where expp.id =:examId " +
            "group by expp.id ) as examForm",nativeQuery = true)
    Object findFullExam(@Param("examId") Long examId);



}
