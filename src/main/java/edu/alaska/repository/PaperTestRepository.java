package edu.alaska.repository;

import edu.alaska.domain.PaperTest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Set;

/**
 * Created by sonle on 7/25/17.
 */
@Deprecated
public interface PaperTestRepository extends JpaRepository<PaperTest,Long> {

    List<PaperTest> findByAuthorId(Long authorId);

    @Modifying
    @Query(value = "INSERT INTO paper_test_quiz(test_id,quiz_id)" +
            " SELECT :testId,eq.id FROM edu_quiz eq WHERE eq.id IN :quizIds",nativeQuery = true)
    void assignQuizToPaperTest(@Param("testId")Long testId, @Param("quizIds")Set<Long> quizIdSet);

    @Modifying
    @Query(value = "DELETE FROM paper_test_quiz WHERE test_id = :testId;",nativeQuery = true)
    void removeAllTestId(@Param("testId")Long testId);

    @Modifying
    @Query(value = "UPDATE paper_test_quiz set publish = 1 WHERE test_id =:testId AND quizId IN :quizIds",nativeQuery = true)
    void publishQuizPaperTest(@Param("testId")Long testId, @Param("quizIds")Set<Long> quizIdSet);

}
