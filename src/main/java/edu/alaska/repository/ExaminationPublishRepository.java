package edu.alaska.repository;


import edu.alaska.domain.ExaminationPublish;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by sstvn on 5/2/17.
 */
public interface ExaminationPublishRepository extends JpaRepository<ExaminationPublish,Long> {

    ExaminationPublish findByExamId(Long examId);
}
