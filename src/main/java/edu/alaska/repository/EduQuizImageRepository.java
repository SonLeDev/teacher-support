package edu.alaska.repository;


import edu.alaska.domain.EduQuizImage;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by sstvn on 5/7/17.
 */
public interface EduQuizImageRepository extends JpaRepository<EduQuizImage,Long> {
    EduQuizImage findByQuizId(Long id);
}
