package edu.alaska.repository;

import edu.alaska.domain.ExaminationQuizs;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface ExaminationQuizsRepository extends JpaRepository<ExaminationQuizs,Long> {

    ExaminationQuizs findByExamIdAndQuizId(Long examId, Long quizId);

    ExaminationQuizs findByQuizId(Long quizId);

    @Modifying
    @Query(value = "UPDATE examination_quizs SET deleted = 1 WHERE quiz_id=:quizId",nativeQuery = true)
    void updateDeletedQuiz(@Param("quizId") Long quizId);
}
