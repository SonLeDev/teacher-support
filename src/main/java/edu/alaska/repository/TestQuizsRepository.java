package edu.alaska.repository;

import edu.alaska.domain.TestQuizs;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface TestQuizsRepository extends JpaRepository<TestQuizs,Long> {

    TestQuizs findByTestIdAndQuizId(Long testId, Long quizId);

    TestQuizs findByQuizId(Long quizId);

    @Modifying
    @Query(value = "UPDATE test_quizs SET deleted = 1 WHERE quiz_id=:quizId",nativeQuery = true)
    void updateDeletedQuiz(@Param("quizId") Long quizId);
}
