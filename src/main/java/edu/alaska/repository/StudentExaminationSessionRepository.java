package edu.alaska.repository;

import edu.alaska.domain.StudentExaminationSession;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by SonLee on 5/3/2017.
 */
public interface StudentExaminationSessionRepository extends JpaRepository<StudentExaminationSession,Long> {

}
