package edu.alaska.repository;

import edu.alaska.domain.StudentClass;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by sstvn on 5/1/17.
 */
public interface StudentClassRepository extends CrudRepository<StudentClass,Long> {
}
