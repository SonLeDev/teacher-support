package edu.alaska.repository;

import edu.alaska.domain.StudentPracticeSession;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by sstvn on 5/7/17.
 */
public interface StudentPracticeSessionRepository extends JpaRepository<StudentPracticeSession,Long> {
}
