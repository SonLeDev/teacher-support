package edu.alaska.repository;

import edu.alaska.domain.EduSubject;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by sstvn on 5/2/17.
 */
public interface EduSubjectRepository extends CrudRepository<EduSubject,Long> {

    List<EduSubject> findByClassName(String className);
    
    @Query(value = "SELECT * FROM edu_subject es WHERE es.class_name = ?1 AND es.subject_name =?2",nativeQuery = true)
    public List<EduSubject> getEduSubject(String className, String subjectName);

}
