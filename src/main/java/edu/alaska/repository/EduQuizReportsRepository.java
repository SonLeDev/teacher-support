package edu.alaska.repository;

import edu.alaska.domain.EduQuizReports;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by sonle on 11/28/17.
 */
public interface EduQuizReportsRepository extends JpaRepository<EduQuizReports,Long> {
    EduQuizReports findByQuizId(Long quizId);
}
