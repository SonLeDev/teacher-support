package edu.alaska.repository;


import edu.alaska.domain.TestPaper;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Collection;
import java.util.Map;

/**
 * Created by sstvn on 5/2/17.
 */
public interface TestPapperRepository extends JpaRepository<TestPaper,Long> {

    Collection<TestPaper> findBySubjectId(Long subjectId);

    Collection<TestPaper> findByClassIdAndSubjectIdAndAuthorIdOrderByMdfDate(Long classId, Long subjectId, Long authorId);

    Collection<TestPaper> findByClassIdAndSubjectIdOrderByMdfDate(Long classId, Long subjectId);

    @Query(value = "SELECT COALESCE(MAX(orders),0) maxNoOfQuiz FROM test_quizs WHERE test_id =:testId",nativeQuery = true)
    Integer getMaxNoOfQuiz(@Param("testId")Long testId);


    @Query(value = "SELECT tp.id AS id,tp.name AS name FROM TestPaper AS tp WHERE tp.subjectId=:subjectId")
    Collection<Map> findBasicListBySubjectId(@Param("subjectId") Long subjectId);

}
