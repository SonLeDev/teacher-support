package edu.alaska.repository;

import edu.alaska.domain.SysId;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;
import java.util.Date;

public interface SysRepository extends CrudRepository<SysId,Long>{

    @Transactional
    @Modifying
    @Query(value = "INSERT INTO sys_id(keyId,domain,reg_date) VALUES (?1,?2,?3)",nativeQuery = true)
    void storeSysId(Long key, String domain, Date regDate);
}
