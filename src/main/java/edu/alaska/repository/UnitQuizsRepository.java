package edu.alaska.repository;

import edu.alaska.domain.UnitQuizs;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by sonle on 7/4/17.
 */
public interface UnitQuizsRepository extends JpaRepository<UnitQuizs,Long> {
}
