package edu.alaska.repository;

import edu.alaska.domain.EduImage;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by sstvn on 6/1/17.
 */
public interface EduImageRepository extends CrudRepository<EduImage,Long> {
}
