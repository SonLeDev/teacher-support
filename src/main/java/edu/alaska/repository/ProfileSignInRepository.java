package edu.alaska.repository;

import edu.alaska.domain.ProfileSignIn;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * Created by sstvn on 5/2/17.
 */
public interface ProfileSignInRepository extends JpaRepository<ProfileSignIn,Long> {

    @Query(value = "SELECT * FROM profile_sign_in WHERE access_token =:accessToken",nativeQuery = true)
    ProfileSignIn findByAccessToken(@Param("accessToken") String accessToken);
}
