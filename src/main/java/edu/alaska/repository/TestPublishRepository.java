package edu.alaska.repository;

import edu.alaska.domain.TestPublish;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by sstvn on 5/2/17.
 */
public interface TestPublishRepository extends JpaRepository<TestPublish,Long> {

    TestPublish findByTestId(Long examId);
}
