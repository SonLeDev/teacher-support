package edu.alaska.repository;



import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import edu.alaska.domain.User;
import org.springframework.data.repository.query.Param;

import java.util.Collection;
import java.util.Map;

/**
 * Created by sstvn on 5/7/17.
 */
public interface UserRepository extends JpaRepository<User,Long> {
	@Query(value = "SELECT * FROM user u WHERE u.user_name = ?1 AND u.password=?2",nativeQuery = true)
    User getUserByUserNameAndPassword(String userName, String password);

    User findByUserName(String userName);


    @Query(value = "SELECT u.id AS id, u.userName AS name FROM User u WHERE u.subjectId=:subjectId AND u.classId=:classId")
    Collection<Map> findBasic(@Param("subjectId") Long subjectId,@Param("classId") Long classId);
}
