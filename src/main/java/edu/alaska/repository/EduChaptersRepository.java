package edu.alaska.repository;



import java.util.List;

import edu.alaska.domain.EduChapters;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 * Created by sstvn on 5/7/17.
 */
public interface EduChaptersRepository extends JpaRepository<EduChapters,Long> {
	@Query(value = "SELECT * FROM edu_chapters c WHERE c.subject_id = ?1",nativeQuery = true)
    List<EduChapters> findChapterBySubjectId(Long subjectId);

    List<EduChapters> findByClassIdAndSubjectId(Long classId, Long subjectId);

}
