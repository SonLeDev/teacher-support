package edu.alaska.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import edu.alaska.domain.EduQuiz;
import org.springframework.data.repository.query.Param;

/**
 * Created by SonLee on 5/26/2017.
 */
public interface EduQuizRepository extends JpaRepository<EduQuiz,Long>{

    @Query(value = "SELECT * FROM edu_quiz q " +
                    " INNER JOIN examination_quizs eq ON eq.quiz_id = q.id " +
                    " WHERE eq.exam_id = ?1 AND eq.deleted=0",nativeQuery = true)
    public List<EduQuiz> selectQuizOfExamId(Long examId);
    
    @Query(value = "SELECT * FROM edu_quiz q WHERE q.user_id = ?1 AND q.deleted=0",nativeQuery = true)
    public List<EduQuiz> findQuizByUserId(Long userId);

    @Query(value = "SELECT * FROM edu_quiz q " +
                    "WHERE q.user_id = ?1 AND q.subject_id = ?2 AND q.deleted=0" +
                    " ORDER BY q.update_date DESC",nativeQuery = true)
    List<EduQuiz> findByUserIdAndSubjectId(Long userId, Long subjectId);

    @Modifying
    @Query(value = "UPDATE edu_quiz SET deleted = 1 WHERE id =:quizId ",nativeQuery = true)
    Integer updateDeletedQuiz(@Param("quizId")Long quizId);

    @Modifying
    @Query(value = "UPDATE edu_quiz SET published =:isBublished WHERE id =:quizId ",nativeQuery = true)
    Integer updatePublished(@Param("quizId") Long quizId,@Param("isBublished")Boolean isBublished);
}
