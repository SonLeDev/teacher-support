package edu.alaska.repository;



import edu.alaska.domain.EduExamQuiz;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by sstvn on 5/7/17.
 */
public interface EduExamQuizRepository extends JpaRepository<EduExamQuiz,Long> {
}
