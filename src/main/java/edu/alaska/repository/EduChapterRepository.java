package edu.alaska.repository;



import java.util.List;

import edu.alaska.domain.EduChapter;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 * Created by sstvn on 5/7/17.
 */
public interface EduChapterRepository extends JpaRepository<EduChapter,Long> {
	@Query(value = "SELECT * FROM edu_chapters c WHERE c.subject_id = ?1",nativeQuery = true)
    public List<EduChapter> findChapterBySubjectId(Long subjectId);
}
