package edu.alaska.form;

/**
 * Created by SonLee on 5/26/2017.
 */
public class QuizForm {
	private Long id;
    /**
     * there are some place using quizId instead of id
     */
	private Long quizId;

	@Deprecated
    private Integer noOfQuest;
    /**
     * noOfQuiz or noOfQuest is the same - the orders of quiz
     * -- become orders columm of examination_quizs and test_quizs
     */
    private Integer noOfQuiz;
    private String chapterName;
    private String question;
    private String answerA;
    private String answerB;
    private String answerC;
    private String answerD;
    private String solution;
    private String detailAnswer;
    private String previewImage;
    private Long examId;
    private Long testId;
    private Boolean chkAnswerA;
    private Boolean chkAnswerB;
    private Boolean chkAnswerC;
    private Boolean chkAnswerD;
    private String chkAnswer;

    private int level;
    private String correctAnswer;
    
    private Object imgQuestFile;
    private Object imgAnswerDetailFile;

    private Float pointOfQuiz;
    private Integer imageId;
    private Integer chapterId;
    private String chapterNameOption;

    private Long unitId;
    private Long userId;
    private Long subjectId;

    private Object updateDate;
    private String unitName;
    private String author;
    private String dtailAnsImg;
    private String questImg;

    private Boolean published;

    // Default is false
    private Boolean isPract = Boolean.FALSE;


    public Boolean getChkAnswerA() {
        return chkAnswerA;
    }

    public void setChkAnswerA(Boolean chkAnswerA) {
        this.chkAnswerA = chkAnswerA;
    }

    public Boolean getChkAnswerB() {
        return chkAnswerB;
    }

    public void setChkAnswerB(Boolean chkAnswerB) {
        this.chkAnswerB = chkAnswerB;
    }

    public Boolean getChkAnswerC() {
        return chkAnswerC;
    }

    public void setChkAnswerC(Boolean chkAnswerC) {
        this.chkAnswerC = chkAnswerC;
    }

    public Boolean getChkAnswerD() {
        return chkAnswerD;
    }

    public void setChkAnswerD(Boolean chkAnswerD) {
        this.chkAnswerD = chkAnswerD;
    }

    public String getChapterName() {
        return chapterName;
    }

    public void setChapterName(String chapterName) {
        this.chapterName = chapterName;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getSolution() {
        return solution;
    }

    public void setSolution(String solution) {
        this.solution = solution;
    }

    public String getDetailAnswer() {
        return detailAnswer;
    }

    public void setDetailAnswer(String detailAnswer) {
        this.detailAnswer = detailAnswer;
    }


    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public Long getExamId() {
        return examId;
    }

    public void setExamId(Long examId) {
        this.examId = examId;
    }

    public String getPreviewImage() {
        return previewImage;
    }

    public void setPreviewImage(String previewImage) {
        this.previewImage = previewImage;
    }

    public Integer getNoOfQuest() {
        if(noOfQuest == null) return noOfQuiz;
        return noOfQuest;
    }

    public void setNoOfQuest(Integer noOfQuest) {
        this.noOfQuest = noOfQuest;
    }

    public String getAnswerA() {
        return answerA;
    }

    public void setAnswerA(String answerA) {
        this.answerA = answerA;
    }

    public String getAnswerB() {
        return answerB;
    }

    public void setAnswerB(String answerB) {
        this.answerB = answerB;
    }

    public String getAnswerC() {
        return answerC;
    }

    public void setAnswerC(String answerC) {
        this.answerC = answerC;
    }

    public String getAnswerD() {
        return answerD;
    }

    public void setAnswerD(String answerD) {
        this.answerD = answerD;
    }

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCorrectAnswer() {
		return correctAnswer;
	}

	public void setCorrectAnswer(String correctAnswer) {
		this.correctAnswer = correctAnswer;
	}


	public Float getPointOfQuiz() {
		return pointOfQuiz;
	}

	public void setPointOfQuiz(Float pointOfQuiz) {
		this.pointOfQuiz = pointOfQuiz;
	}

	public Integer getImageId() {
		return imageId;
	}

	public void setImageId(Integer imageId) {
		this.imageId = imageId;
	}

	public Integer getChapterId() {
		return chapterId;
	}

	public void setChapterId(Integer chapterId) {
		this.chapterId = chapterId;
	}

	public String getChapterNameOption() {
		return chapterNameOption;
	}

	public void setChapterNameOption(String chapterNameOption) {
		this.chapterNameOption = chapterNameOption;
	}

    public Long getUnitId() {
        return unitId;
    }

    public void setUnitId(Long unitId) {
        this.unitId = unitId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getChkAnswer() {
        return chkAnswer;
    }

    public void setChkAnswer(String chkAnswer) {
        this.chkAnswer = chkAnswer;
    }

    public Long getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(Long subjectId) {
        this.subjectId = subjectId;
    }

    public Integer getNoOfQuiz() {
        if(noOfQuiz==null) return noOfQuest;
        return noOfQuiz;
    }

    public void setNoOfQuiz(Integer noOfQuiz) {
        this.noOfQuiz = noOfQuiz;
    }

    public Long getTestId() {
        return testId;
    }

    public void setTestId(Long testId) {
        this.testId = testId;
    }

    public Long getQuizId() {
        return quizId;
    }

    public void setQuizId(Long quizId) {
        this.quizId = quizId;
    }

    public Object getImgQuestFile() {
        return imgQuestFile;
    }

    public void setImgQuestFile(Object imgQuestFile) {
        this.imgQuestFile = imgQuestFile;
    }

    public Object getImgAnswerDetailFile() {
        return imgAnswerDetailFile;
    }

    public void setImgAnswerDetailFile(Object imgAnswerDetailFile) {
        this.imgAnswerDetailFile = imgAnswerDetailFile;
    }

    public String getQuestImg() {
        return questImg;
    }

    public void setQuestImg(String questImg) {
        this.questImg = questImg;
    }

    public Object getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Object updateDate) {
        this.updateDate = updateDate;
    }

    public String getUnitName() {
        return unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getDtailAnsImg() {
        return dtailAnsImg;
    }

    public void setDtailAnsImg(String dtailAnsImg) {
        this.dtailAnsImg = dtailAnsImg;
    }

    public Boolean getPublished() {
        return published;
    }

    public void setPublished(Boolean published) {
        this.published = published;
    }

    public Boolean getIsPract() {
        return isPract;
    }

    public void setIsPract(Boolean pract) {
        isPract = pract;
    }
}
