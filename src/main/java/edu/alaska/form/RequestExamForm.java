package edu.alaska.form;

public class RequestExamForm {

    private Long id;
    private Long examId;
    private String examName;
    private String examCode;
    private Integer timeExpire;
    private String instruction;
    private String description;
    private Integer numOfQuiz;
    private Integer yearExam;
    private Long author;
    private Long subjectId;
    private Long classId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getExamName() {
        return examName;
    }

    public void setExamName(String examName) {
        this.examName = examName;
    }

    public Integer getTimeExpire() {
        return timeExpire;
    }

    public void setTimeExpire(Integer timeExpire) {
        this.timeExpire = timeExpire;
    }

    public Long getAuthor() {
        return author;
    }

    public void setAuthor(Long author) {
        this.author = author;
    }

    public String getInstruction() {
        return instruction;
    }

    public void setInstruction(String instruction) {
        this.instruction = instruction;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getNumOfQuiz() {
        return numOfQuiz;
    }

    public void setNumOfQuiz(Integer numOfQuiz) {
        this.numOfQuiz = numOfQuiz;
    }

    public Integer getYearExam() {
        return yearExam;
    }

    public void setYearExam(Integer yearExam) {
        this.yearExam = yearExam;
    }

    public Long getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(Long subjectId) {
        this.subjectId = subjectId;
    }

    public Long getClassId() {
        return classId;
    }

    public void setClassId(Long classId) {
        this.classId = classId;
    }

    public Long getExamId() {
        return examId;
    }

    public void setExamId(Long examId) {
        this.examId = examId;
    }

    public String getExamCode() {
        return examCode;
    }

    public void setExamCode(String examCode) {
        this.examCode = examCode;
    }
}
