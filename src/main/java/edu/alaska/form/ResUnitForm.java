package edu.alaska.form;

import java.math.BigInteger;

/**
 * Created by sonle on 10/16/17.
 */
public class ResUnitForm {
    private String id;
    private String name;
    private String countQuiz;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCountQuiz() {
        return countQuiz;
    }
    public void setCountQuiz(String countQuiz) {
        this.countQuiz = countQuiz;
    }
}
