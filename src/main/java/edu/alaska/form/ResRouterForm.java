package edu.alaska.form;

import java.util.List;

/**
 * Created by sonle on 11/25/17.
 */
public class ResRouterForm {

    private String groupName;
    private List<StateForm> states;
    private String activeGroup = "";
    private String activeChild = "";

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public List<StateForm> getStates() {
        return states;
    }

    public void setStates(List<StateForm> states) {
        this.states = states;
    }

    public String getActiveGroup() {
        return activeGroup;
    }

    public void setActiveGroup(String activeGroup) {
        this.activeGroup = activeGroup;
    }

    public void setActiveChild(String activeChild) {
        this.activeChild = activeChild;
    }

    public String getActiveChild() {
        return activeChild;
    }
}
