package edu.alaska.form;

/**
 * Created by sonle on 11/27/17.
 */
public class StateForm{
    private String title;
    private String stateName;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getStateName() {
        return stateName;
    }

    public void setStateName(String stateName) {
        this.stateName = stateName;
    }
}
