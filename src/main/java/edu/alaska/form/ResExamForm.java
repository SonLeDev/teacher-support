package edu.alaska.form;

/**
 * Created by sonle on 11/20/17.
 */
public class ResExamForm {

    private Long examId;
    private String author;
    private String examCode;
    private String examName;
    private Integer numOfQuest;
    private Integer duration;
    private Long subjectId;
    private Boolean published;
    private String tagName;
    private Boolean tagPublished;
    private Long numQuestPublished;
    private Long numQuestUnPublished;

    public Long getExamId() {
        return examId;
    }

    public void setExamId(Long examId) {
        this.examId = examId;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getExamCode() {
        return examCode;
    }

    public void setExamCode(String examCode) {
        this.examCode = examCode;
    }

    public String getExamName() {
        return examName;
    }

    public void setExamName(String examName) {
        this.examName = examName;
    }

    public Integer getNumOfQuest() {
        return numOfQuest;
    }

    public void setNumOfQuest(Integer numOfQuest) {
        this.numOfQuest = numOfQuest;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public Long getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(Long subjectId) {
        this.subjectId = subjectId;
    }

    public Boolean getPublished() {
        return published;
    }

    public void setPublished(Boolean published) {
        this.published = published;
    }

    public String getTagName() {
        return tagName;
    }

    public void setTagName(String tagName) {
        this.tagName = tagName;
    }

    public Boolean getTagPublished() {
        return tagPublished;
    }

    public void setTagPublished(Boolean tagPublished) {
        this.tagPublished = tagPublished;
    }

    public Long getNumQuestPublished() {
        return numQuestPublished;
    }

    public void setNumQuestPublished(Long numQuestPublished) {
        this.numQuestPublished = numQuestPublished;
    }

    public Long getNumQuestUnPublished() {
        return numQuestUnPublished;
    }

    public void setNumQuestUnPublished(Long numQuestUnPublished) {
        this.numQuestUnPublished = numQuestUnPublished;
    }
}
