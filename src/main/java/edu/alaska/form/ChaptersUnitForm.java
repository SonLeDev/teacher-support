package edu.alaska.form;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sonle on 7/3/17.
 */
public class ChaptersUnitForm {
//    //        SELECT ec.id chapterId,ec.name chapterName, eu.id unitId, eu.unit_name unitName

    private Long chapterId;
    private String chapterName;
    List<UnitForm> unitList = new ArrayList<>();

    public Long getChapterId() {
        return chapterId;
    }

    public void setChapterId(Long chapterId) {
        this.chapterId = chapterId;
    }

    public String getChapterName() {
        return chapterName;
    }

    public void setChapterName(String chapterName) {
        this.chapterName = chapterName;
    }

    public List<UnitForm> getUnitList() {
        return unitList;
    }

    public void setUnitList(List<UnitForm> unitList) {
        this.unitList = unitList;
    }

    public class UnitForm {
        private Long unitId;
        private String unitName;

        public Long getUnitId() {
            return unitId;
        }

        public void setUnitId(Long unitId) {
            this.unitId = unitId;
        }

        public String getUnitName() {
            return unitName;
        }

        public void setUnitName(String unitName) {
            this.unitName = unitName;
        }
    }
}


