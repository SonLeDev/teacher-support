package edu.alaska.form;

import java.util.Date;

/**
 * Created by sonle on 7/27/17.
 */
public class PaperTestForm {

    private Long id;
    private Long classId;
    private Long subjectId;
    private String name;
    private String code;
    private Long timeExpire;
    private String testType;
    private Long authorId;

    private Long[] quizIds;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getClassId() {
        return classId;
    }

    public void setClassId(Long classId) {
        this.classId = classId;
    }

    public Long getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(Long subjectId) {
        this.subjectId = subjectId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Long getTimeExpire() {
        return timeExpire;
    }

    public void setTimeExpire(Long timeExpire) {
        this.timeExpire = timeExpire;
    }

    public String getTestType() {
        return testType;
    }

    public void setTestType(String testType) {
        this.testType = testType;
    }

    public Long getAuthorId() {
        return authorId;
    }

    public void setAuthorId(Long authorId) {
        this.authorId = authorId;
    }

    public Long[] getQuizIds() {
        return quizIds;
    }

    public void setQuizIds(Long[] quizIds) {
        this.quizIds = quizIds;
    }
}
