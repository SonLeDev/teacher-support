package edu.alaska.form;

import java.util.Date;

/**
 * Created by sonle on 10/13/17.
 */
public class ReqFilterForm {

    private Long subjectId;
    private Long examId;
    private Long testId;
    private Long userId;
    private Long unitId;
    private Date updateDate;
    private Boolean published;
    private Boolean reportQuizError;
    private Long[] selectedUnitIds;
    private Long[] selectedQuizIds;
    private Boolean randomQuiz = Boolean.FALSE;
    private Integer limitedRow = 1000;

    public Long getExamId() {
        return examId;
    }

    public void setExamId(Long examId) {
        this.examId = examId;
    }

    public Long getTestId() {
        return testId;
    }

    public void setTestId(Long testId) {
        this.testId = testId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getUnitId() {
        return unitId;
    }

    public void setUnitId(Long unitId) {
        this.unitId = unitId;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public Boolean getPublished() {
        return published;
    }

    public void setPublished(Boolean published) {
        this.published = published;
    }

    public Long[] getSelectedUnitIds() {
        return selectedUnitIds;
    }

    public void setSelectedUnitIds(Long[] selectedUnitIds) {
        this.selectedUnitIds = selectedUnitIds;
    }

    public Long[] getSelectedQuizIds() {
        return selectedQuizIds;
    }

    public void setSelectedQuizIds(Long[] selectedQuizIds) {
        this.selectedQuizIds = selectedQuizIds;
    }

    public Boolean getReportQuizError() {
        return reportQuizError;
    }

    public void setReportQuizError(Boolean reportQuizError) {
        this.reportQuizError = reportQuizError;
    }

    public Long getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(Long subjectId) {
        this.subjectId = subjectId;
    }

    public Boolean getRandomQuiz() {
        return randomQuiz;
    }

    public void setRandomQuiz(Boolean randomQuiz) {
        this.randomQuiz = randomQuiz;
    }

    public Integer getLimitedRow() {
        return limitedRow;
    }

    public void setLimitedRow(Integer limitedRow) {
        this.limitedRow = limitedRow;
    }
}
