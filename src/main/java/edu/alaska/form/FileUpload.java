package edu.alaska.form;

import org.springframework.web.multipart.MultipartFile;

/**
 * Created by SonLee on 5/29/2017.
 */
public class FileUpload {
    MultipartFile file;

    public MultipartFile getFile() {
        return file;
    }

    public void setFile(MultipartFile file) {
        this.file = file;
    }
}
