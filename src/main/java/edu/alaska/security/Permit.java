package edu.alaska.security;

/**
 * Created by sonle on 7/16/17.
 */
public class Permit {

    private Boolean math = false;
    private Boolean physic = false;
    private Boolean chemistry = false;
    private Boolean english = false;
    private Boolean all = false;

    public Boolean getMath() {
        return math;
    }

    public void setMath(Boolean math) {
        this.math = math;
    }

    public Boolean getPhysic() {
        return physic;
    }

    public void setPhysic(Boolean physic) {
        this.physic = physic;
    }

    public Boolean getAll() {
        return all;
    }

    public void setAll(Boolean all) {
        this.all = all;
    }


    public void setChemistry(boolean chemistry) {
        this.chemistry = chemistry;
    }

    public void setEnglish(boolean english) {
        this.english = english;
    }

    public Boolean getChemistry() {
        return chemistry;
    }

    public Boolean getEnglish() {
        return english;
    }
}
