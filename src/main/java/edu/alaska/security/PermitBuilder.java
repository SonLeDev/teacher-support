package edu.alaska.security;

/**
 * Created by sonle on 7/16/17.
 */
public class PermitBuilder {
    private Permit permit;

    private static  PermitBuilder instance = new PermitBuilder();

    public static PermitBuilder getInstance() {
        return instance;
    }

    public Permit permitMath(){
        Permit permitMath = new Permit();
        permitMath.setMath(true);
        return permitMath;
    }

    public Permit permitPhysic(){
        Permit permitPhysic = new Permit();
        permitPhysic.setPhysic(true);
        return permitPhysic;
    }

    public Permit permitChemistry(){
        Permit permitChemistry = new Permit();
        permitChemistry.setChemistry(true);
        return permitChemistry;
    }

    public Permit permitEnglish(){
        Permit permitEnglish = new Permit();
        permitEnglish.setEnglish(true);
        return permitEnglish;
    }


    public Permit permitAll(){
        Permit permitMath = new Permit();
        permitMath.setPhysic(true);
        permitMath.setMath(true);
        permitMath.setChemistry(true);
        permitMath.setEnglish(true);
        permitMath.setAll(true);
        return permitMath;
    }

}
