package edu.alaska.security;

/**
 * Created by sonle on 7/16/17.
 */
public enum  Permission {
    MATH("math"),PHYSIC("physic")
    ,CHEMISTRY("chemistry"),ENGLISH("english")
    ,ALL("all");

    private String name;
    Permission(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

}
