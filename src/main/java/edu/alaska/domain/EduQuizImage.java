package edu.alaska.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Date;

/**
 * Created by sonle on 8/30/17.
 */
@Entity
public class EduQuizImage {
    @Id
    @GeneratedValue
    private Long id;

    private Long quizId;
    private String questImg;
    private String dtailAnsImg;
    private Date mdfDate;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getQuizId() {
        return quizId;
    }

    public void setQuizId(Long quizId) {
        this.quizId = quizId;
    }

    public String getQuestImg() {
        return questImg;
    }

    public void setQuestImg(String questImg) {
        this.questImg = questImg;
    }

    public String getDtailAnsImg() {
        return dtailAnsImg;
    }

    public void setDtailAnsImg(String dtailAnsImg) {
        this.dtailAnsImg = dtailAnsImg;
    }

    public Date getMdfDate() {
        return mdfDate;
    }

    public void setMdfDate(Date mdfDate) {
        this.mdfDate = mdfDate;
    }
}
