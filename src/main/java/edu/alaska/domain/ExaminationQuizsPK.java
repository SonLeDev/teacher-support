package edu.alaska.domain;

import java.io.Serializable;

public class ExaminationQuizsPK implements Serializable{
    private Long examId;
    private Long quizId;
}
