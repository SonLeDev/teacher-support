package edu.alaska.domain;

import edu.alaska.common.Utils;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by sstvn on 5/2/17.
 */
@Entity
public class EduQuiz {
    @Id
    @GeneratedValue
    private Long id;

    private int noOfQuest;

    private String question;

    private String answerA;
    private String answerB;
    private String answerC;
    private String answerD;

    private String correctAnswer;
    private int level;
    private String solution;
    private String detailAnswer;
    private Float pointOfQuiz;

    private Long imageId;

    private Long chapterId;
    private String chapterName;
    private Long userId;
    private Long subjectId;

    private Boolean published = Boolean.FALSE;
    private Boolean deleted = Boolean.FALSE;
    private Boolean isPract = Boolean.FALSE;

    private Date updateDate = Utils.getDateTimeZonVN();

    @OneToOne
    @JoinColumn(name = "unit_id")
    private EduUnit eduUnit;

    @Transient
    private Long unitId;
    @Transient
    private String unitName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getAnswerA() {
        return answerA;
    }

    public void setAnswerA(String answerA) {
        this.answerA = answerA;
    }

    public String getAnswerB() {
        return answerB;
    }

    public void setAnswerB(String answerB) {
        this.answerB = answerB;
    }

    public String getAnswerC() {
        return answerC;
    }

    public void setAnswerC(String answerC) {
        this.answerC = answerC;
    }

    public String getAnswerD() {
        return answerD;
    }

    public void setAnswerD(String answerD) {
        this.answerD = answerD;
    }

    public String getCorrectAnswer() {
        return correctAnswer;
    }

    public void setCorrectAnswer(String correctAnswer) {
        this.correctAnswer = correctAnswer;
    }

    public String getSolution() {
        return solution;
    }

    public void setSolution(String solution) {
        this.solution = solution;
    }

    public String getDetailAnswer() {
        return detailAnswer;
    }

    public void setDetailAnswer(String detailAnswer) {
        this.detailAnswer = detailAnswer;
    }


    public int getNoOfQuest() {
        return noOfQuest;
    }

    public void setNoOfQuest(int noOfQuest) {
        this.noOfQuest = noOfQuest;
    }

    public Float getPointOfQuiz() {
        return pointOfQuiz;
    }

    public void setPointOfQuiz(Float pointOfQuiz) {
        this.pointOfQuiz = pointOfQuiz;
    }

    public Long getImageId() {
        return imageId;
    }

    public void setImageId(Long imageId) {
        this.imageId = imageId;
    }

    public Long getChapterId() {
        return chapterId;
    }

    public void setChapterId(Long chapterId) {
        this.chapterId = chapterId;
    }

    public String getChapterName() {
        return chapterName;
    }

    public void setChapterName(String chapterName) {
        this.chapterName = chapterName;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(Long subjectId) {
        this.subjectId = subjectId;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public EduUnit getEduUnit() {
        return eduUnit;
    }

    public void setEduUnit(EduUnit eduUnit) {
        this.eduUnit = eduUnit;
    }

    public Long getUnitId() {
        if(eduUnit!=null){
            return eduUnit.getId();
        }
        return unitId;
    }

    public String getUnitName() {
        if(eduUnit!=null){
            return eduUnit.getUnitName();
        }
        return unitName;
    }

    public Boolean getPublished() {
        return published;
    }

    public void setPublished(Boolean published) {
        this.published = published;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public Boolean getIsPract() {
        return isPract;
    }

    public void setIsPract(Boolean pract) {
        isPract = pract;
    }
}
