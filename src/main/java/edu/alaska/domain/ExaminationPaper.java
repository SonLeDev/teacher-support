package edu.alaska.domain;

import edu.alaska.common.Utils;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;

/**
 * Created by sstvn on 5/1/17.
 */
@Entity
public class ExaminationPaper {

    @Id
    @GeneratedValue
    private Long id;
    private String examName;    // option
    private String examCode;
    private Long author;      // who compose, ignore manage teacher
    private Integer yearExam;
    private Integer timeExpire;
    private Integer numOfQuiz;
    private String description;
    private String instruction;
    private Long classId;     // exam for who belong to classId
    private Long subjectId;     // exam for specific subjectId
    private Date mdfDate = Utils.getDateTimeZonVN();

    @ManyToMany(cascade = {
                        CascadeType.DETACH,
                        CascadeType.MERGE,
                        CascadeType.REFRESH,
                        CascadeType.PERSIST
                })
    @JoinTable(name = "examination_quizs",
            joinColumns = { @JoinColumn(name = "exam_id") },
            inverseJoinColumns = { @JoinColumn(name = "quiz_id") })
    @OrderBy("noOfQuest")
    private Set<EduQuiz> eduQuiz ;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getExamName() {
        return examName;
    }

    public void setExamName(String examName) {
        this.examName = examName;
    }

    public Long getAuthor() {
        return author;
    }

    public void setAuthor(Long author) {
        this.author = author;
    }


    public Long getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(Long subjectId) {
        this.subjectId = subjectId;
    }

    public Long getClassId() {
        return classId;
    }

    public void setClassId(Long classId) {
        this.classId = classId;
    }


    public Integer getNumOfQuiz() {
        return numOfQuiz;
    }

    public void setNumOfQuiz(Integer numOfQuiz) {
        this.numOfQuiz = numOfQuiz;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getYearExam() {
        return yearExam;
    }

    public void setYearExam(Integer yearExam) {
        this.yearExam = yearExam;
    }

    public Integer getTimeExpire() {
        return timeExpire;
    }

    public void setTimeExpire(Integer timeExpire) {
        this.timeExpire = timeExpire;
    }

    public Set<EduQuiz> getEduQuiz() {
        return eduQuiz;
    }

    public void setEduQuiz(Set<EduQuiz> eduQuiz) {
        this.eduQuiz = eduQuiz;
    }

    public String getInstruction() {
        return instruction;
    }

    public void setInstruction(String instruction) {
        this.instruction = instruction;
    }

    public Date getMdfDate() {
        return mdfDate;
    }

    public void setMdfDate(Date mdfDate) {
        this.mdfDate = mdfDate;
    }

    public String getExamCode() {
        return examCode;
    }

    public void setExamCode(String examCode) {
        this.examCode = examCode;
    }
}
