package edu.alaska.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class EduChapters {
	@Id
	@GeneratedValue
	private Long id;
	private String chpName;
	private String chpCode;
	private String chpIcon;
	private Long classId;
	private Integer chpOrder;
	private Long subjectId;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getChpName() {
		return chpName;
	}

	public void setChpName(String chpName) {
		this.chpName = chpName;
	}

	public String getChpCode() {
		return chpCode;
	}

	public void setChpCode(String chpCode) {
		this.chpCode = chpCode;
	}

	public String getChpIcon() {
		return chpIcon;
	}

	public void setChpIcon(String chpIcon) {
		this.chpIcon = chpIcon;
	}

	public Long getClassId() {
		return classId;
	}

	public void setClassId(Long classId) {
		this.classId = classId;
	}

	public Integer getChpOrder() {
		return chpOrder;
	}

	public void setChpOrder(Integer chpOrder) {
		this.chpOrder = chpOrder;
	}

	public Long getSubjectId() {
		return subjectId;
	}

	public void setSubjectId(Long subjectId) {
		this.subjectId = subjectId;
	}
}
