package edu.alaska.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Date;

/**
 * Created by sonle on 11/28/17.
 */
@Entity
public class EduQuizReports {

    @Id
    @GeneratedValue
    private Long id;

    private Long quizId;
    private String typeReport;
    private Date dateReport;
    private Long userIdReport;

    private Boolean statusReport = Boolean.TRUE;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getQuizId() {
        return quizId;
    }

    public void setQuizId(Long quizId) {
        this.quizId = quizId;
    }

    public String getTypeReport() {
        return typeReport;
    }

    public void setTypeReport(String typeReport) {
        this.typeReport = typeReport;
    }

    public Date getDateReport() {
        return dateReport;
    }

    public void setDateReport(Date dateReport) {
        this.dateReport = dateReport;
    }

    public Long getUserIdReport() {
        return userIdReport;
    }

    public void setUserIdReport(Long userIdReport) {
        this.userIdReport = userIdReport;
    }

    public Boolean getStatusReport() {
        return statusReport;
    }

    public void setStatusReport(Boolean statusReport) {
        this.statusReport = statusReport;
    }
}
