package edu.alaska.domain;

import edu.alaska.common.Utils;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import java.util.Date;

@Entity @IdClass(TestQuizsPK.class)
public class TestQuizs {
    @Id
    @Column(name = "test_id")
    private Long testId;
    @Id
    @Column(name = "quiz_id")
    private Long quizId;

    private Integer orders=0;
    private Boolean publish = Boolean.FALSE;
    private Boolean published = Boolean.FALSE;
    private Date mdfDate = Utils.getDateTimeZonVN();
    private Boolean deleted = Boolean.FALSE;

    public Long getQuizId() {
        return quizId;
    }

    public void setQuizId(Long quizId) {
        this.quizId = quizId;
    }

    public Integer getOrders() {
        return orders;
    }

    public void setOrders(Integer orders) {
        this.orders = orders;
    }

    public Boolean getPublish() {
        return publish;
    }

    public void setPublish(Boolean publish) {
        this.publish = publish;
    }

    public Date getMdfDate() {
        return mdfDate;
    }

    public void setMdfDate(Date mdfDate) {
        this.mdfDate = mdfDate;
    }

    public Long getTestId() {
        return testId;
    }

    public void setTestId(Long testId) {
        this.testId = testId;
    }

    public Boolean getPublished() {
        return published;
    }

    public void setPublished(Boolean published) {
        this.published = published;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }
}
